﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class TFElement
    {
        public int id { get; set; }
        [Display(Name = "N° de TF:")]
        [Required(ErrorMessage = "Numéro TF est obligatoires !")]
        public int numerotf { get; set; }
        [Display(Name = "Immatriculé le:")]
        [DataType(DataType.Date)]
        public string datetf { get; set; }
        public string Valeur { get; set; }
        public string Superficie { get; set; }
        public string Lieux { get; set; }
        public string AllProps{ get; set; }
        public string numerolot { get; set; }
        public virtual List<ProprietaireElement> proprietaires { get; set; }
    }
}