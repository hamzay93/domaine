﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class OperationForReports
    {
        public int ID { get; set; }
        public string Nature { get; set; }
        public string Rangs { get; set; }
        public string NomBeneficiaire { get; set; }
    }
}