﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class ProprietaresViewModel
    {
        public int id { get; set; }


        [Display(Name = "Prénom :")]
        //[Required(ErrorMessage = "Numéro TF est obligatoires !")]
        public string prenom { get; set; }
        [Display(Name = "Nom 2:")]

        public string nome2 { get; set; }
        [Display(Name = "Nom 3:")]

        public string nom3 { get; set; }


        [Display(Name = "Prénom de la mère:")]

        public string prenommere { get; set; }
        [Display(Name = "Nom 2 mère:")]

        public string nommere2 { get; set; }
        [Display(Name = "Nom 3 mère:")]

        public string nommere3 { get; set; }
        [Display(Name = "N° de TF:")]

        public string profession { get; set; }
        [Display(Name = "Nationalité:")]

        public Nullable<int> nationalite { get; set; }
        [Display(Name = "Date de naissance:")]

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> dateNaissace { get; set; }
        [Display(Name = "Lieu de naissance:")]

        public string lieuNaisance { get; set; }
        [Display(Name = "Numéro de pièce:")]


        public string numeropiece { get; set; }
                [Display(Name = "Date d'émission de Pièce")]

        [DataType(DataType.Date)]

        public Nullable<System.DateTime> piecedateemission { get; set; }
        [Display(Name = "Type de pièce:")]

        public Nullable<int> typepiece { get; set; }

        [Display(Name = "Nom de la société")]

        public string nomestablishment { get; set; }
        [Display(Name = "Prénom du responsable:")]

        public string nomeresponsable1 { get; set; }


        [Display(Name = "Nom 2 du responsable:")]

        public string nomeresponsable2 { get; set; }
        [Display(Name = "Nom 3 du responsable:")]

        public string nomeresponsable3 { get; set; }



        public string typestablishment { get; set; }
        [Display(Name = "Secteur d'activité:")]

        public string secturactivite { get; set; }
        [Display(Name = "N° de TF:")]

        public Nullable<int> typeproprietaire { get; set; }
        [Display(Name = "N° de TF:")]
        
        public string telephone { get; set; }
        [Display(Name = "N° de TF:")]

        public string adresse { get; set; }
        public string rue { get; set; }
        public string telfix { get; set; }
        public string fax { get; set; }
        public string pays { get; set; }

        //ADD BY SAAD
        public int idOperation { get; set; }


    }
}