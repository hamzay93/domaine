﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class HistoriquesViewModels
    {
        public HistoriquesViewModels()
        {
            this.HistoriquesOperations = new HashSet<HistoriquesOperations>();
        }

        public int ID { get; set; }
        public string ActionName { get; set; }
        public string Operations { get; set; }
        public string TypesOperations { get; set; }
        public string Descriptions { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> DateActions { get; set; }
        public Nullable<int> UserActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HistoriquesOperations> HistoriquesOperations { get; set; }
    }
}
