﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class ElementHypotheque
    {
        public int Rang { get; set; }
        public string Nature { get; set; }
        public string Beneficiaire { get; set; }
        public string type { get; set; }
    }
}