﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionTitreFoncierApp.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class GTFDB_PROD_PRESEntities : DbContext
    {
        public GTFDB_PROD_PRESEntities()
            : base("name=GTFDB_PROD_PRESEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Actes> Actes { get; set; }
        public virtual DbSet<Acteurs> Acteurs { get; set; }
        public virtual DbSet<Arrondissement> Arrondissement { get; set; }
        public virtual DbSet<Commune> Commune { get; set; }
        public virtual DbSet<Consultation> Consultation { get; set; }
        public virtual DbSet<datetimetable> datetimetable { get; set; }
        public virtual DbSet<DriversMapped> DriversMapped { get; set; }
        public virtual DbSet<Drives> Drives { get; set; }
        public virtual DbSet<DroitAcces> DroitAcces { get; set; }
        public virtual DbSet<Factures_Acteurs> Factures_Acteurs { get; set; }
        public virtual DbSet<Factures_Status> Factures_Status { get; set; }
        public virtual DbSet<Historiques> Historiques { get; set; }
        public virtual DbSet<HistoriquesOperations> HistoriquesOperations { get; set; }
        public virtual DbSet<Nationalite> Nationalite { get; set; }
        public virtual DbSet<NatureOperation> NatureOperation { get; set; }
        public virtual DbSet<Operations> Operations { get; set; }
        public virtual DbSet<Operations_Status> Operations_Status { get; set; }
        public virtual DbSet<Paiments_Factures> Paiments_Factures { get; set; }
        public virtual DbSet<piecejustificative> piecejustificative { get; set; }
        public virtual DbSet<proprietaire> proprietaire { get; set; }
        public virtual DbSet<quartier> quartier { get; set; }
        public virtual DbSet<region> region { get; set; }
        public virtual DbSet<saisie> saisie { get; set; }
        public virtual DbSet<tf> tf { get; set; }
        public virtual DbSet<TF_Cooproprietes> TF_Cooproprietes { get; set; }
        public virtual DbSet<TFFusions_Principals> TFFusions_Principals { get; set; }
        public virtual DbSet<Tfs_Distraction> Tfs_Distraction { get; set; }
        public virtual DbSet<Tfs_Distraction_Proprios> Tfs_Distraction_Proprios { get; set; }
        public virtual DbSet<TypeActes> TypeActes { get; set; }
        public virtual DbSet<TypeActeur> TypeActeur { get; set; }
        public virtual DbSet<TypeEntreprise> TypeEntreprise { get; set; }
        public virtual DbSet<typelieu> typelieu { get; set; }
        public virtual DbSet<typeoperation> typeoperation { get; set; }
        public virtual DbSet<typepiece> typepiece { get; set; }
        public virtual DbSet<typeproprietaire> typeproprietaire { get; set; }
        public virtual DbSet<typesaisie> typesaisie { get; set; }
        public virtual DbSet<TypesOperation> TypesOperation { get; set; }
        public virtual DbSet<typetf> typetf { get; set; }
        public virtual DbSet<typeutilisatur> typeutilisatur { get; set; }
        public virtual DbSet<Unites_Surface> Unites_Surface { get; set; }
        public virtual DbSet<utilisateur> utilisateur { get; set; }
        public virtual DbSet<Vendors> Vendors { get; set; }
        public virtual DbSet<Ville> Ville { get; set; }
        public virtual DbSet<saisie11> saisie11 { get; set; }
    }
}
