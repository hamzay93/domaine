﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace GestionTitreFoncierApp.Models
{
    public class AppUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}