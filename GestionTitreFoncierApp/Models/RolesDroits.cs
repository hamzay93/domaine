﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class RolesDroits
    {
        public typeutilisatur typeutilisateur { get; set; }
        public virtual List<DroitsAcces> Droits { get; set; }
    }
}