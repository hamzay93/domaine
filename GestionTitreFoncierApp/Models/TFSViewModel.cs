﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class TFSViewModel
    {
        public int id { get; set; }
        [Display(Name = "N° de TF:")]
        [Required(ErrorMessage = "Numéro TF est obligatoires !")]
        public int numerotf { get; set; }
        [Display(Name = "Immatriculé le:")]
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> datetf { get; set; }
        //[DataType(DataType.Date)]

       
        [Display(Name = "N° ACP:")]

        public string numacp { get; set; }
        [Display(Name = "Date d'opératio:")]
        [DataType(DataType.Date)]

        public Nullable<System.DateTime> DateOperation { get; set; }

        [Display(Name = "Valeur du terrain:")]

        public string valeurterrain { get; set; }

        //public Nullable<bool> estbati { get; set; }
        [Display(Name = "Terrain bâti:")]

        public bool estbati { get; set; }

        //[Display(Name = "N° de TF:")]

        public string cheminimage { get; set; }
        [Display(Name = "N° de piéce:")]

        public string numeropiece { get; set; }
        [Display(Name = "N° du Lot:")]

        public string numerolot { get; set; }
        [Display(Name = "nom d'ilot:")]

        public string nomillot { get; set; }
        [Display(Name = "Est un ilot")]

        public bool estillot { get; set; }
        [Display(Name = "Lieu:")]

        public Nullable<int> idquartier { get; set; }
        [Display(Name = "Type du Lieu:")]

        public Nullable<int> typelieu { get; set; }
        [Display(Name = "Type du TF:")]

        public Nullable<int> typetf { get; set; }
        [Display(Name = "N° de TF:")]

        public Nullable<int> typeoperation { get; set; }
        [Display(Name = "Piéce:")]

        public Nullable<int> PIECEJUSTIFICATIVE_id { get; set; }

        

        public Nullable<int> usercreated { get; set; }

        public Nullable<System.DateTime> dateCreated { get; set; }

        public Nullable<int> usermodified { get; set; }

        public Nullable<System.DateTime> dateModified { get; set; }
        [Display(Name = "Superficie:")]

        public string superficie { get; set; }
        public Nullable<int> ID_Unites { get; set; }

        [DataType(DataType.Upload)]
       public HttpPostedFileBase ImageUpload { get; set; }
        [Display(Name = "N° de TF:")]
        public virtual Unites_Surface Unites_Surface { get; set; }

        public virtual piecejustificative piecejustificative { get; set; }
        [Display(Name = "N° de TF:")]

        public virtual quartier quartier { get; set; }
        [Display(Name = "N° de TF:")]

        public virtual typelieu typelieu1 { get; set; }
        [Display(Name = "N° de TF:")]

        public virtual typetf typetf1 { get; set; }
        [Display(Name = "N° de TF:")]
        public Nullable<bool> Actif { get; set; }

        public bool Coopropriete { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<proprietaire> proprietaires { get; set; }
        [Required]
        public virtual List<ProprietaresViewModel> proprietaires { get; set; }
        // public virtual ICollection<ProprietaresViewModel> proprietaires { get; set; }
        //public virtual List<Hypotheque> Hypotheques { get; set; }
        //public virtual List<Mutation> Mutations { get; set; }


    }
}