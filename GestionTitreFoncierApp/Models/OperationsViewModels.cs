﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class OperationsViewModels
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OperationsViewModels()
        {
            this.Actes = new HashSet<Actes>();
            this.HistoriquesOperations = new HashSet<HistoriquesOperations>();
            this.proprietaires = new HashSet<proprietaire>();
        }

        public int id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> datecreation { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> datemodification { get; set; }
        [Display(Name = "Nature Operation")]
        public Nullable<int> idNatureOperation { get; set; }
        [Display(Name = "L'id TF")]
        public Nullable<int> idTF { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> dateOperation { get; set; }
        [Display(Name = "Montant")]
        public Nullable<decimal> montantOperation { get; set; }
        public Nullable<int> tempOperation { get; set; }
        public Nullable<int> activeOperation { get; set; }
        public Nullable<int> valideOperation { get; set; }
        public Nullable<int> rang { get; set; }
        public Nullable<int> idTypeOperation { get; set; }
        public Nullable<int> idActeur { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> dateFin { get; set; }
        public Nullable<int> idVendor { get; set; }
        public string Status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Actes> Actes { get; set; }
        public virtual Acteurs Acteur { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HistoriquesOperations> HistoriquesOperations { get; set; }
        public virtual NatureOperation NatureOperation { get; set; }
        public virtual tf tf { get; set; }
        public virtual TypesOperation TypesOperation { get; set; }
        public virtual Vendors Vendor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<proprietaire> proprietaires { get; set; }
    }
}
