//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestionTitreFoncierApp.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class proprietaire
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public proprietaire()
        {
            this.Operations = new HashSet<Operations>();
            this.tf = new HashSet<tf>();
            this.Vendors = new HashSet<Vendors>();
        }
    
        public int id { get; set; }
        public string prenom { get; set; }
        public string nome2 { get; set; }
        public string nom3 { get; set; }
        public string prenommere { get; set; }
        public string nommere2 { get; set; }
        public string nommere3 { get; set; }
        public string profession { get; set; }
        public Nullable<int> nationalite { get; set; }
        public Nullable<System.DateTime> dateNaissace { get; set; }
        public string lieuNaisance { get; set; }
        public string telephone { get; set; }
        public string adresse { get; set; }
        public string rue { get; set; }
        public string telfix { get; set; }
        public string fax { get; set; }
        public string pays { get; set; }
        public string numeropiece { get; set; }
        public Nullable<System.DateTime> piecedateemission { get; set; }
        public string nomestablishment { get; set; }
        public string nomeresponsable1 { get; set; }
        public string nomeresponsable2 { get; set; }
        public string nomeresponsable3 { get; set; }
        public string typestablishment { get; set; }
        public string secturactivite { get; set; }
        public Nullable<int> typeproprietaire { get; set; }
        public Nullable<int> typepiece { get; set; }
    
        public virtual Nationalite Nationalite1 { get; set; }
        public virtual typeproprietaire typeproprietaire1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Operations> Operations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tf> tf { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vendors> Vendors { get; set; }
    }
}
