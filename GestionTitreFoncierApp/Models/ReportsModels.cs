﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class ReportsModels
    {
        //public ReportsModels()
        //{
        //    this.ProprietaireElement = new HashSet<ProprietaireElement>();
        //}
        public int ID { get; set; }
        public string NumerosTF { get; set; }
        public DateTime DateInscription { get; set; }
        //public virtual ICollection<ProprietaireElement> ProprietaireElement { get; set; }
        public  string TypeTF { get; set; }
        public string TypeTerrain { get; set; }
        public string Valeur { get; set; }
        public string Lieu { get; set; }
        public string Superficie { get; set; }
        public int Rang1 { get; set; }
        public string Nature1 { get; set; }
        public string Beneficiaire1 { get; set; }
        public string type1 { get; set; }
        public int Rang2 { get; set; }
        public string Nature2 { get; set; }
        public string Beneficiaire2 { get; set; }
        public string type2 { get; set; }
        public int Rang3 { get; set; }
        public string Nature3 { get; set; }
        public string Beneficiaire3 { get; set; }
        public string type3 { get; set; }
        public string Nomproprietaire { get; set; }
     
    }
   

}