﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class historiquesConsultations
    {
        public int ID { get; set; }

        public string Nom { get; set; }
        public int CountConsul { get; set; }
        public int CountCreation{ get; set; }
        public int CountModification { get; set; }

    }
}