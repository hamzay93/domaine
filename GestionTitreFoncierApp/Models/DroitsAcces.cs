﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.Models
{
    public class DroitsAcces
    {
        public int ID { get; set; }
        public string Libelle { get; set; }
        public bool Checked { get; set; }
    }
}