﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp.ViewModels
{
    public class MutationsViewModel
    {
        

        public int id { get; set; }
        [Display(Name = "N° ACP:")]
        [Required(ErrorMessage = "Numéro ACP est obligatoire !")]
        public string numeroacp { get; set; }
        public Nullable<System.DateTime> dateenregistrement { get; set; }
        public Nullable<System.DateTime> daterequis { get; set; }
        public Nullable<System.DateTime> datecreation { get; set; }
        public Nullable<System.DateTime> datemodification { get; set; }
        public Nullable<System.DateTime> datevente { get; set; }
        public string numerovente { get; set; }
        public Nullable<int> idTF { get; set; }
        public Nullable<int> idTypeMutation { get; set; }
        public Nullable<int> montant { get; set; }
        public Nullable<int> idNotaire { get; set; }

        public virtual tf tf { get; set; }
        //public virtual TypeMutation TypeMutation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<proprietaire> proprietaires { get; set; }
        //public virtual Notaire Notaire { get; set; }
    }
}