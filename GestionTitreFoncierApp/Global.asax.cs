﻿using aejw.Network;
using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace GestionTitreFoncierApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
         //   preparedrivemapping();
        }
        protected void preparedrivemapping()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();


            try
            {

                // DriversMapped d = new DriversMapped();
                var d = db.DriversMapped.Where(x => x.IsActive == true && x.IsValid == true).ToList().FirstOrDefault();

                if (d != null)

                {

                    string drive = "";
                    int driveno = Convert.ToInt32(d.Drive);
                    drive = db.Drives.Where(x => x.id == driveno).FirstOrDefault().name;
                    oNetDrive.Force = true;
                    oNetDrive.Persistent = true;
                    oNetDrive.LocalDrive = drive;
                    oNetDrive.PromptForCredentials = false;
                    oNetDrive.ShareName = d.SharedPath;
                    oNetDrive.SaveCredentials = true;
                    oNetDrive.MapDrive(d.username, d.Password);
                    Log("  " + "Drive map successfully" + "  ");
                }
            }
            catch (Exception err)
            {
                Log("  " + err.Message + "  ");
                //report error
                //zUpdateStatus("Cannot map drive! - " + err.Message);
                //MessageBox.Show(this, "Cannot map drive!\nError: " + err.Message);
            }
            oNetDrive = null;

        }


        public static void Log(string logMessage)
        {
            using (StreamWriter w = File.AppendText(@"D:\log.txt"))
            {


                w.Write("\r\nLog Entry : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                w.WriteLine("  :");
                w.WriteLine("  :{0}", logMessage);
                w.WriteLine("-------------------------------");
            }

        }

        // Unmap network drive

        // Check / Restore a network drive


    }
}
