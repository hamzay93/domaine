﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestionTitreFoncierApp.Startup))]
namespace GestionTitreFoncierApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
