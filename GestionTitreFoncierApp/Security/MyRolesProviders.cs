﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace GestionTitreFoncierApp.Security
{
    public class MyRolesProviders : RoleProvider
    {
        GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            using (var DB = new GTFDB_PROD_PRESEntities())
            {
                var utilisateur = DB.utilisateur.FirstOrDefault(a => a.login == username);
                string[] Droits = utilisateur.typeutilisatur1.DroitAcces.Select(u => u.Libelle).ToArray();
                return Droits;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            //string[] resultS = db.utilisateurs.Where(a => a.RolesUtilisateur.Roles == roleName).Select(i => i.login).ToArray();
           
            //return resultS;
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {

            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}