﻿using aejw.Network;
using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp
{
    public class mapDrive
    {
         public static void preparedrivemapping()
        {
            NetworkDrive oNetDrive = new NetworkDrive();
            GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();


            try
            {

                // DriversMapped d = new DriversMapped();
                var d = db.DriversMapped.Where(x => x.IsActive == true && x.IsValid == true).ToList().FirstOrDefault();

                if (d != null)

                {
                    string drive = "";
                    int driveno = Convert.ToInt32(d.Drive);
                    drive = db.Drives.Where(x => x.id == driveno).FirstOrDefault().name;
                    oNetDrive.Force = false;
                    oNetDrive.Persistent = true;
                    oNetDrive.LocalDrive = drive;
                    oNetDrive.PromptForCredentials = false;
                    oNetDrive.ShareName = d.SharedPath;
                    oNetDrive.SaveCredentials = true;
                    oNetDrive.MapDrive(d.username, d.Password);
                    Log("  " + "Drive map successfully" + "  ");

                }
                //oNetDrive.Force = false;
                //oNetDrive.Persistent = true;
                //oNetDrive.LocalDrive = "M:";
                //oNetDrive.PromptForCredentials = false;
                //oNetDrive.ShareName = @"\\WIN-3KQH9E2B32G\Users\Administrator\Desktop\rt";
                //oNetDrive.SaveCredentials = true;
                //oNetDrive.MapDrive("Administrator", "sdsi*2016");
                //Log("  " + "Drive map successfully" + "  ");





            }
            catch (Exception err)
            {
                Log("  " + err.Message + "  ");
                //report error
                //zUpdateStatus("Cannot map drive! - " + err.Message);
                //MessageBox.Show(this, "Cannot map drive!\nError: " + err.Message);
            }
            oNetDrive = null;

        }


        public static void Log(string logMessage)
        {
            using (StreamWriter w = File.AppendText(@"C:\log.txt"))
            {


                w.Write("\r\nLog Entry : ");
                w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                w.WriteLine("  :");
                w.WriteLine("  :{0}", logMessage);
                w.WriteLine("-------------------------------");
            }

        }
    }
}