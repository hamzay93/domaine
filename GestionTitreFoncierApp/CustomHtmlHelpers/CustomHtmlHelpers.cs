﻿using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.CustomHtmlHelpers
{
    public static class CustomHtmlHelpers
    {
       // public static IHtmlString Image(this HtmlHelper helper, string src, string alt)
                    public static IHtmlString Image(this HtmlHelper helper, string src)

        {
            // Build <img> tag
           // TagBuilder button = new TagBuilder("button");
            TagBuilder tb = new TagBuilder("img");
            // Add "src" attribute
            tb.Attributes.Add("src", VirtualPathUtility.ToAbsolute(src));
            tb.Attributes.Add("src", VirtualPathUtility.ToAbsolute(src));

            // Add "alt" attribute
            //tb.Attributes.Add("alt", alt);
            // return MvcHtmlString. This class implements IHtmlString
            // interface. IHtmlStrings will not be html encoded.
            return new MvcHtmlString(tb.ToString(TagRenderMode.SelfClosing));
        }
    }
}