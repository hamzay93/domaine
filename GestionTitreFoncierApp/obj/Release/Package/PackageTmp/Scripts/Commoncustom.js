﻿
///*Generated List menu*/function GenerateNewList(t) { var e = "/tfs/gettingAllSubDirectories/", a = "#" + t + " option:selected", i = $(a).val(), n = $(a).text(); $.ajax({ type: "POST", url: e, data: { parentpath: i }, success: function (e) { if ($("#labelId").text("k"), e.length > 1) { var a = "#" + t + "1", o = t + "1", r = "<select id='" + o + "' name='" + o + '\' style="width: 120px" onchange="GenerateNewList(\'' + o + "');\">  </select> "; $("#absn").append(r); for (var l = "<option value='0'>... Choisissez Dossier ...</option>", s = 0; s < e.length; s++) l += "<option value=" + e[s].Value + ">" + e[s].Text + "</option>"; $(a).html(l).show() } else { for (var c = "", p = n.length; p > 0 && "-" !== n.charAt(p) ; p--) c += n.charAt(p); $("#numerotf").val(c), $("#numerotf").attr("readonly", !0); var g = "/tfs/imageNavigation"; $("#right_col").html(""), $("#right_col").load(g, { typeNavigation: 0, cheminimage: i }), $("#right_col").append(h) } }, error: function (t) { alert($(this).id) } }) }
//function GenerateNewList(e) { var t = "/tfs/gettingAllSubDirectories/", a = "#" + e + " option:selected", i = $(a).val(), n = $(a).text(); $.ajax({ type: "POST", url: t, data: { parentpath: i }, success: function (t) { if ($("#labelId").text("k"), t.length > 1) { var a = "#" + e + "1", o = e + "1", r = "<select id='" + o + "' name='" + o + '\' style="width: 120px" onchange="GenerateNewList(\'' + o + "');\">  </select> "; $("#absn").append(r); for (var l = "<option value='0'>... Choisissez Dossier ...</option>", s = 0; s < t.length; s++) l += "<option value=" + t[s].Value + ">" + t[s].Text + "</option>"; $(a).html(l).show() } else { for (var c = "", p = n.length; p > 0 && "-" !== n.charAt(p) ; p--) c += n.charAt(p); var g = c.split("").reverse().join(""); $("#numerotf").val(g), $("#numerotf").attr("readonly", !0); $("#cheminimage").val(i); var v = "/tfs/imageNavigation"; $("#right_col").html(""), $("#right_col").load(v, { typeNavigation: 0, cheminimage: i }), $("#right_col").append(h) } }, error: function (e) { alert($(this).id) } }) }
/*Hardisk menu*/$(document).ready(function () { $("#HardDiskList").on("change", function () { $.ajax({ type: "POST", url: "/tfs/gettingAllSubDirectories?parentpath=" + document.getElementById("HardDiskList").value, cache: !1, success: function (t) { if (t.length > 0) { var e = "#" + $(this).attr("id") + "1", i = $(this).attr("id") + "1", n = "<select id='" + i + "' name='" + i + '\' style="width: 90px" onchange="GenerateNewList(\'' + i + "');\">  </select> "; $("#absn").append(n); for (var a = "<option value='0'>... Choisissez Dossier ...</option>", s = 0; s < t.length; s++) a += "<option value=" + t[s].Value + ">" + t[s].Text + "</option>"; $(e).html(a).show() } }, error: function (t) { alert($(this).id) } }) }) });
/*Region SelectChange event*/
$(document).ready(function () {
    $("#Regid").on("change", function () {
        var e = document.getElementById("Regid").value,
           // t = $("input[name=typLieu]").val(),
            n = "/quartiers/getville/";
        $.ajax({
            type: "POST",
            url: n,
            data: { regid: e },
            cache: !1,
            success: function (e) {
                if (e.length > 0) {

                    for (var t = "<option value='0'>... Choisissez Ville ...</option>", n = 0; n < e.length; n++)
                        t += "<option value=" + e[n].Value + ">" + e[n].Text + "</option>";
                    $("#VilleID").html(t).show()

                    var markupp = "<option value='0'> --------- Vide ------- </option>";
                    $("#idquartier").html(markupp).show();
                }
            }, error: function (e)
            { alert($(this).id) }
        })
    })
});
$(document).ready(function ()
{
    $("#VilleID").on("change", function ()
    {
        var e = document.getElementById("VilleID").value,
            t = $("input[name=typLieu]").val(),
            n = "/quartiers/getQuarlst/";
        $.ajax({
            type: "POST",
            url: n,
            data: { typ: t, regid: e },
            cache: !1,
            success: function (e)
            {
                if (e.length > 0)
                {
                    for (var t = "<option value='0'>... Choisissez lieu ...</option>", n = 0; n < e.length; n++)
                        t += "<option value=" + e[n].Value + ">" + e[n].Text + "</option>";
                    $("#idquartier").html(t).show()
                }
            }, error: function (e)
            { alert($(this).id) }
        })
    })
});
/*typelieu RadiobtnChange event*/
$(document).ready(function ()
{
    $("input[name=typLieu]").on("change", function ()
    {
        var e = document.getElementById("VilleID").value,
            t = $(this).val(),
            i = "/quartiers/getQuarlst/";
        $.ajax({
            type: "POST",
            url: i,
            data: { typ: t, regid: e },
            cache: !1, success: function (e)
            {
                if (e.length > 0)
                {
                    for (var t = "<option value='0'>... Choisissez lieu ...</option>", i = 0; i < e.length; i++)
                        t += "<option value=" + e[i].Value + ">" + e[i].Text + "</option>";
                    $("#idquartier").html(t).show()
                }
            }, error: function (e) { alert($(this).id) }
        })
    })
});
