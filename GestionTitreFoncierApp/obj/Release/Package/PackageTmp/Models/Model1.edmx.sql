
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/03/2017 13:47:03
-- Generated from EDMX file: D:\TF DB\Back Up GTFApp\8 Autres Opérations\7 plus droit dacces\GestionTitreFoncierApp\GestionTitreFoncierApp\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TFDB_TEST];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__tfproprie__idpro__793DFFAF]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tfproprietaire] DROP CONSTRAINT [FK__tfproprie__idpro__793DFFAF];
GO
IF OBJECT_ID(N'[dbo].[FK__tfpropriet__idtf__7A3223E8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tfproprietaire] DROP CONSTRAINT [FK__tfpropriet__idtf__7A3223E8];
GO
IF OBJECT_ID(N'[dbo].[FK_Actes_Acteurs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Actes] DROP CONSTRAINT [FK_Actes_Acteurs];
GO
IF OBJECT_ID(N'[dbo].[FK_Actes_Operations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Actes] DROP CONSTRAINT [FK_Actes_Operations];
GO
IF OBJECT_ID(N'[dbo].[FK_Actes_TypeActes1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Actes] DROP CONSTRAINT [FK_Actes_TypeActes1];
GO
IF OBJECT_ID(N'[dbo].[FK_Acteurs_TypeActeur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Acteurs] DROP CONSTRAINT [FK_Acteurs_TypeActeur];
GO
IF OBJECT_ID(N'[dbo].[FK_Acteurs_typeproprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Acteurs] DROP CONSTRAINT [FK_Acteurs_typeproprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_Historiques_utilisateur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Historiques] DROP CONSTRAINT [FK_Historiques_utilisateur];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriquesOperations_Actes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoriquesOperations] DROP CONSTRAINT [FK_HistoriquesOperations_Actes];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriquesOperations_Historiques]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoriquesOperations] DROP CONSTRAINT [FK_HistoriquesOperations_Historiques];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriquesOperations_Operations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoriquesOperations] DROP CONSTRAINT [FK_HistoriquesOperations_Operations];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriquesTFs_Historiques]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoriquesTFs] DROP CONSTRAINT [FK_HistoriquesTFs_Historiques];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoriquesTFs_tf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistoriquesTFs] DROP CONSTRAINT [FK_HistoriquesTFs_tf];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_Acteurs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_Acteurs];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_NatureOperation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_NatureOperation];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_Operations_Status]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_Operations_Status];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_tf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_tf];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_TypesOperation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_TypesOperation];
GO
IF OBJECT_ID(N'[dbo].[FK_Operations_Vendors]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_Operations_Vendors];
GO
IF OBJECT_ID(N'[dbo].[FK_Operationsproprietaire_Operations]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operationsproprietaire] DROP CONSTRAINT [FK_Operationsproprietaire_Operations];
GO
IF OBJECT_ID(N'[dbo].[FK_Operationsproprietaire_proprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operationsproprietaire] DROP CONSTRAINT [FK_Operationsproprietaire_proprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_proprietaire_Nationalite]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[proprietaire] DROP CONSTRAINT [FK_proprietaire_Nationalite];
GO
IF OBJECT_ID(N'[dbo].[FK_proprietaire_typeproprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[proprietaire] DROP CONSTRAINT [FK_proprietaire_typeproprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_quartier_region]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[quartier] DROP CONSTRAINT [FK_quartier_region];
GO
IF OBJECT_ID(N'[dbo].[FK_quartier_utilisateur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[quartier] DROP CONSTRAINT [FK_quartier_utilisateur];
GO
IF OBJECT_ID(N'[dbo].[FK_saisie_typesaisie]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[saisie] DROP CONSTRAINT [FK_saisie_typesaisie];
GO
IF OBJECT_ID(N'[dbo].[FK_saisie_utilisateur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[saisie] DROP CONSTRAINT [FK_saisie_utilisateur];
GO
IF OBJECT_ID(N'[dbo].[FK_tf_piecejustificative]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tf] DROP CONSTRAINT [FK_tf_piecejustificative];
GO
IF OBJECT_ID(N'[dbo].[FK_tf_quartier]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tf] DROP CONSTRAINT [FK_tf_quartier];
GO
IF OBJECT_ID(N'[dbo].[FK_tf_typelieu]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tf] DROP CONSTRAINT [FK_tf_typelieu];
GO
IF OBJECT_ID(N'[dbo].[FK_tf_typetf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tf] DROP CONSTRAINT [FK_tf_typetf];
GO
IF OBJECT_ID(N'[dbo].[FK_tf_utilisateur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tf] DROP CONSTRAINT [FK_tf_utilisateur];
GO
IF OBJECT_ID(N'[dbo].[FK_TypesOperation_NatureOperation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TypesOperation] DROP CONSTRAINT [FK_TypesOperation_NatureOperation];
GO
IF OBJECT_ID(N'[dbo].[FK_TypesOperationTypeActes_TypeActes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TypesOperationTypeActes] DROP CONSTRAINT [FK_TypesOperationTypeActes_TypeActes];
GO
IF OBJECT_ID(N'[dbo].[FK_TypesOperationTypeActes_TypesOperation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TypesOperationTypeActes] DROP CONSTRAINT [FK_TypesOperationTypeActes_TypesOperation];
GO
IF OBJECT_ID(N'[dbo].[FK_utilisateur_typeutilisatur]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[utilisateur] DROP CONSTRAINT [FK_utilisateur_typeutilisatur];
GO
IF OBJECT_ID(N'[dbo].[FK_Vendors_tf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vendors] DROP CONSTRAINT [FK_Vendors_tf];
GO
IF OBJECT_ID(N'[dbo].[FK_Vendors_TypeEntreprise]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vendors] DROP CONSTRAINT [FK_Vendors_TypeEntreprise];
GO
IF OBJECT_ID(N'[dbo].[FK_VendorsActeurs_Acteurs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VendorsActeurs] DROP CONSTRAINT [FK_VendorsActeurs_Acteurs];
GO
IF OBJECT_ID(N'[dbo].[FK_VendorsActeurs_Vendors]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[VendorsActeurs] DROP CONSTRAINT [FK_VendorsActeurs_Vendors];
GO
IF OBJECT_ID(N'[dbo].[FK_Vendorsproprietaire_proprietaire]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vendorsproprietaire] DROP CONSTRAINT [FK_Vendorsproprietaire_proprietaire];
GO
IF OBJECT_ID(N'[dbo].[FK_Vendorsproprietaire_Vendors]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vendorsproprietaire] DROP CONSTRAINT [FK_Vendorsproprietaire_Vendors];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Actes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Actes];
GO
IF OBJECT_ID(N'[dbo].[Acteurs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Acteurs];
GO
IF OBJECT_ID(N'[dbo].[datetimetable]', 'U') IS NOT NULL
    DROP TABLE [dbo].[datetimetable];
GO
IF OBJECT_ID(N'[dbo].[DriversMapped]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriversMapped];
GO
IF OBJECT_ID(N'[dbo].[Drives]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Drives];
GO
IF OBJECT_ID(N'[dbo].[DroitAcces]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DroitAcces];
GO
IF OBJECT_ID(N'[dbo].[DroitsRoles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DroitsRoles];
GO
IF OBJECT_ID(N'[dbo].[Historiques]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Historiques];
GO
IF OBJECT_ID(N'[dbo].[HistoriquesOperations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistoriquesOperations];
GO
IF OBJECT_ID(N'[dbo].[HistoriquesTFs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistoriquesTFs];
GO
IF OBJECT_ID(N'[dbo].[Nationalite]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Nationalite];
GO
IF OBJECT_ID(N'[dbo].[NatureOperation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NatureOperation];
GO
IF OBJECT_ID(N'[dbo].[Operations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Operations];
GO
IF OBJECT_ID(N'[dbo].[Operations_Status]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Operations_Status];
GO
IF OBJECT_ID(N'[dbo].[Operationsproprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Operationsproprietaire];
GO
IF OBJECT_ID(N'[dbo].[piecejustificative]', 'U') IS NOT NULL
    DROP TABLE [dbo].[piecejustificative];
GO
IF OBJECT_ID(N'[dbo].[proprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[proprietaire];
GO
IF OBJECT_ID(N'[dbo].[quartier]', 'U') IS NOT NULL
    DROP TABLE [dbo].[quartier];
GO
IF OBJECT_ID(N'[dbo].[region]', 'U') IS NOT NULL
    DROP TABLE [dbo].[region];
GO
IF OBJECT_ID(N'[dbo].[saisie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[saisie];
GO
IF OBJECT_ID(N'[dbo].[tf]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tf];
GO
IF OBJECT_ID(N'[dbo].[tfproprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tfproprietaire];
GO
IF OBJECT_ID(N'[dbo].[TypeActes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypeActes];
GO
IF OBJECT_ID(N'[dbo].[TypeActeur]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypeActeur];
GO
IF OBJECT_ID(N'[dbo].[TypeEntreprise]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypeEntreprise];
GO
IF OBJECT_ID(N'[dbo].[typelieu]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typelieu];
GO
IF OBJECT_ID(N'[dbo].[typeoperation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typeoperation];
GO
IF OBJECT_ID(N'[dbo].[typepiece]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typepiece];
GO
IF OBJECT_ID(N'[dbo].[typeproprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typeproprietaire];
GO
IF OBJECT_ID(N'[dbo].[typesaisie]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typesaisie];
GO
IF OBJECT_ID(N'[dbo].[TypesOperation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypesOperation];
GO
IF OBJECT_ID(N'[dbo].[TypesOperationTypeActes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TypesOperationTypeActes];
GO
IF OBJECT_ID(N'[dbo].[typetf]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typetf];
GO
IF OBJECT_ID(N'[dbo].[typeutilisatur]', 'U') IS NOT NULL
    DROP TABLE [dbo].[typeutilisatur];
GO
IF OBJECT_ID(N'[dbo].[utilisateur]', 'U') IS NOT NULL
    DROP TABLE [dbo].[utilisateur];
GO
IF OBJECT_ID(N'[dbo].[Vendors]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vendors];
GO
IF OBJECT_ID(N'[dbo].[VendorsActeurs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[VendorsActeurs];
GO
IF OBJECT_ID(N'[dbo].[Vendorsproprietaire]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vendorsproprietaire];
GO
IF OBJECT_ID(N'[DJTFModelStoreContainer].[saisie11]', 'U') IS NOT NULL
    DROP TABLE [DJTFModelStoreContainer].[saisie11];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Actes'
CREATE TABLE [dbo].[Actes] (
    [id] int IDENTITY(1,1) NOT NULL,
    [numacte] nvarchar(50)  NULL,
    [dateacte] datetime  NULL,
    [numacp] nvarchar(50)  NULL,
    [dateacp] datetime  NULL,
    [idTypeActe] int  NULL,
    [datecreation] datetime  NULL,
    [datemodification] datetime  NULL,
    [idOperation] int  NULL,
    [idActeur] int  NULL,
    [Actif] bit  NULL
);
GO

-- Creating table 'Acteurs'
CREATE TABLE [dbo].[Acteurs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nom] nvarchar(50)  NULL,
    [numerocin] nvarchar(50)  NULL,
    [nommere] nvarchar(50)  NULL,
    [telephonePortable] nvarchar(50)  NULL,
    [telephoneFixe] nvarchar(50)  NULL,
    [email] nvarchar(50)  NULL,
    [adresse] nvarchar(50)  NULL,
    [idTypeActeur] int  NULL,
    [IDTypeProprietaire] int  NULL
);
GO

-- Creating table 'datetimetables'
CREATE TABLE [dbo].[datetimetables] (
    [id] int IDENTITY(1,1) NOT NULL,
    [dt] datetime  NULL
);
GO

-- Creating table 'DriversMappeds'
CREATE TABLE [dbo].[DriversMappeds] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Domian] nvarchar(100)  NOT NULL,
    [username] nvarchar(50)  NOT NULL,
    [Password] nvarchar(50)  NOT NULL,
    [DateCreate] datetime  NULL,
    [SharedPath] nvarchar(300)  NOT NULL,
    [IsValid] bit  NOT NULL,
    [IsActive] bit  NOT NULL,
    [Drive] nvarchar(50)  NULL
);
GO

-- Creating table 'Drives'
CREATE TABLE [dbo].[Drives] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(50)  NULL
);
GO

-- Creating table 'DroitAcces'
CREATE TABLE [dbo].[DroitAcces] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Libelle] varchar(50)  NULL
);
GO

-- Creating table 'Historiques'
CREATE TABLE [dbo].[Historiques] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [ActionName] varchar(50)  NULL,
    [Operations] varchar(max)  NULL,
    [TypesOperations] varchar(max)  NULL,
    [Descriptions] varchar(max)  NULL,
    [DateActions] datetime  NULL,
    [UserActions] int  NULL
);
GO

-- Creating table 'Nationalites'
CREATE TABLE [dbo].[Nationalites] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NULL
);
GO

-- Creating table 'NatureOperations'
CREATE TABLE [dbo].[NatureOperations] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nom] varchar(50)  NULL
);
GO

-- Creating table 'Operations_Status'
CREATE TABLE [dbo].[Operations_Status] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Libelle] varchar(50)  NULL
);
GO

-- Creating table 'piecejustificatives'
CREATE TABLE [dbo].[piecejustificatives] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'proprietaires'
CREATE TABLE [dbo].[proprietaires] (
    [id] int IDENTITY(1,1) NOT NULL,
    [prenom] nvarchar(50)  NULL,
    [nome2] nvarchar(50)  NULL,
    [nom3] nvarchar(50)  NULL,
    [prenommere] nvarchar(50)  NULL,
    [nommere2] nvarchar(50)  NULL,
    [nommere3] nvarchar(50)  NULL,
    [profession] nvarchar(50)  NULL,
    [nationalite] int  NULL,
    [dateNaissace] datetime  NULL,
    [lieuNaisance] nvarchar(50)  NULL,
    [telephone] nchar(10)  NULL,
    [adresse] nvarchar(50)  NULL,
    [rue] nvarchar(50)  NULL,
    [telfix] nvarchar(50)  NULL,
    [fax] nvarchar(50)  NULL,
    [pays] nvarchar(50)  NULL,
    [numeropiece] nvarchar(50)  NULL,
    [piecedateemission] datetime  NULL,
    [nomestablishment] nvarchar(180)  NULL,
    [nomeresponsable1] nvarchar(50)  NULL,
    [nomeresponsable2] nvarchar(50)  NULL,
    [nomeresponsable3] nvarchar(50)  NULL,
    [typestablishment] nvarchar(50)  NULL,
    [secturactivite] nvarchar(50)  NULL,
    [typeproprietaire] int  NULL,
    [typepiece] int  NULL
);
GO

-- Creating table 'quartiers'
CREATE TABLE [dbo].[quartiers] (
    [libelle] nvarchar(50)  NULL,
    [estlotissement] bit  NOT NULL,
    [regionid] int  NULL,
    [user_create] int  NULL,
    [id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'regions'
CREATE TABLE [dbo].[regions] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'saisies'
CREATE TABLE [dbo].[saisies] (
    [id] int IDENTITY(1,1) NOT NULL,
    [datesaisie] nvarchar(50)  NULL,
    [utilistaurid] int  NULL,
    [typesaisie] int  NULL,
    [datesaisie1] datetime  NULL
);
GO

-- Creating table 'TypeActes'
CREATE TABLE [dbo].[TypeActes] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nomacte] nvarchar(50)  NULL,
    [estobligatoire] int  NULL
);
GO

-- Creating table 'TypeActeurs'
CREATE TABLE [dbo].[TypeActeurs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] varchar(50)  NULL,
    [type] nvarchar(5)  NULL
);
GO

-- Creating table 'TypeEntreprises'
CREATE TABLE [dbo].[TypeEntreprises] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Libelle] varchar(50)  NULL,
    [DetailLibelle] varchar(max)  NULL
);
GO

-- Creating table 'typelieux'
CREATE TABLE [dbo].[typelieux] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'typeoperations'
CREATE TABLE [dbo].[typeoperations] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'typepieces'
CREATE TABLE [dbo].[typepieces] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'typeproprietaires'
CREATE TABLE [dbo].[typeproprietaires] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NULL
);
GO

-- Creating table 'typesaisies'
CREATE TABLE [dbo].[typesaisies] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'TypesOperations'
CREATE TABLE [dbo].[TypesOperations] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nom] varchar(50)  NULL,
    [idNatureOperation] int  NULL
);
GO

-- Creating table 'typetfs'
CREATE TABLE [dbo].[typetfs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libell] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'typeutilisaturs'
CREATE TABLE [dbo].[typeutilisaturs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [libelle] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'utilisateurs'
CREATE TABLE [dbo].[utilisateurs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [nom] nvarchar(50)  NOT NULL,
    [login] nvarchar(50)  NOT NULL,
    [motdepasse] nvarchar(50)  NOT NULL,
    [etat] bit  NOT NULL,
    [datecreate] datetime  NULL,
    [typeutilisatur] int  NOT NULL,
    [usercreate] int  NULL
);
GO

-- Creating table 'Vendors'
CREATE TABLE [dbo].[Vendors] (
    [id] int IDENTITY(1,1) NOT NULL,
    [numacp] nvarchar(150)  NULL,
    [datesave] datetime  NULL,
    [datevente] datetime  NULL,
    [numvente] nvarchar(50)  NULL,
    [MontantSub] decimal(19,4)  NULL,
    [NomDenom] varchar(50)  NULL,
    [daterequisition] datetime  NULL,
    [idTF] int  NOT NULL,
    [ID_Type_entreprise] int  NULL
);
GO

-- Creating table 'saisie11'
CREATE TABLE [dbo].[saisie11] (
    [id] int  NOT NULL,
    [datesaisie] nvarchar(50)  NULL,
    [utilistaurid] int  NULL,
    [typesaisie] int  NULL,
    [datesaisie1] datetime  NULL
);
GO

-- Creating table 'HistoriquesOperations'
CREATE TABLE [dbo].[HistoriquesOperations] (
    [IDHistoriques] int  NOT NULL,
    [IDOperations] int  NOT NULL,
    [IDActes] int  NULL
);
GO

-- Creating table 'Operations'
CREATE TABLE [dbo].[Operations] (
    [id] int IDENTITY(1,1) NOT NULL,
    [datecreation] datetime  NULL,
    [datemodification] datetime  NULL,
    [idNatureOperation] int  NULL,
    [idTF] int  NULL,
    [dateOperation] datetime  NULL,
    [montantOperation] decimal(19,4)  NULL,
    [tempOperation] int  NULL,
    [activeOperation] int  NULL,
    [valideOperation] int  NULL,
    [rang] int  NULL,
    [idTypeOperation] int  NULL,
    [idActeur] int  NULL,
    [dateFin] datetime  NULL,
    [idVendor] int  NULL,
    [IDStatus] int  NULL,
    [IDFusion] int  NULL
);
GO

-- Creating table 'tfs'
CREATE TABLE [dbo].[tfs] (
    [id] int IDENTITY(1,1) NOT NULL,
    [numerotf] nvarchar(50)  NOT NULL,
    [datetf] datetime  NULL,
    [nomrepertoire] nvarchar(50)  NULL,
    [numacp] nvarchar(50)  NULL,
    [DateOperation] datetime  NULL,
    [valeurterrain] varchar(50)  NULL,
    [valeurterrainconstruit] int  NULL,
    [estbati] bit  NOT NULL,
    [cheminimage] nvarchar(400)  NULL,
    [numeropiece] nvarchar(50)  NULL,
    [numerolot] nvarchar(50)  NULL,
    [nomillot] nvarchar(50)  NULL,
    [estillot] bit  NOT NULL,
    [idquartier] int  NULL,
    [typelieu] int  NULL,
    [typetf] int  NULL,
    [typeoperation] int  NULL,
    [PIECEJUSTIFICATIVE_id] int  NULL,
    [typeliaison] int  NULL,
    [usercreated] int  NULL,
    [dateCreated] datetime  NULL,
    [usermodified] int  NULL,
    [dateModified] datetime  NULL,
    [superficie] nvarchar(50)  NULL,
    [Actif] bit  NULL
);
GO

-- Creating table 'Tfs_Fusions'
CREATE TABLE [dbo].[Tfs_Fusions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IDTF_Principal] nvarchar(max)  NOT NULL,
    [List] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'DroitsRoles'
CREATE TABLE [dbo].[DroitsRoles] (
    [DroitAcces_ID] int  NOT NULL,
    [typeutilisaturs_id] int  NOT NULL
);
GO

-- Creating table 'TypesOperationTypeActes'
CREATE TABLE [dbo].[TypesOperationTypeActes] (
    [TypeActes_id] int  NOT NULL,
    [TypesOperations_id] int  NOT NULL
);
GO

-- Creating table 'VendorsActeurs'
CREATE TABLE [dbo].[VendorsActeurs] (
    [Acteurs_id] int  NOT NULL,
    [Vendors_id] int  NOT NULL
);
GO

-- Creating table 'Vendorsproprietaire'
CREATE TABLE [dbo].[Vendorsproprietaire] (
    [proprietaires_id] int  NOT NULL,
    [Vendors_id] int  NOT NULL
);
GO

-- Creating table 'Operationsproprietaire'
CREATE TABLE [dbo].[Operationsproprietaire] (
    [Operations_id] int  NOT NULL,
    [proprietaires_id] int  NOT NULL
);
GO

-- Creating table 'HistoriquesTFs'
CREATE TABLE [dbo].[HistoriquesTFs] (
    [Historiques_ID] int  NOT NULL,
    [tfs_id] int  NOT NULL
);
GO

-- Creating table 'tfproprietaire'
CREATE TABLE [dbo].[tfproprietaire] (
    [proprietaires_id] int  NOT NULL,
    [tfs_id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'Actes'
ALTER TABLE [dbo].[Actes]
ADD CONSTRAINT [PK_Actes]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Acteurs'
ALTER TABLE [dbo].[Acteurs]
ADD CONSTRAINT [PK_Acteurs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'datetimetables'
ALTER TABLE [dbo].[datetimetables]
ADD CONSTRAINT [PK_datetimetables]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'DriversMappeds'
ALTER TABLE [dbo].[DriversMappeds]
ADD CONSTRAINT [PK_DriversMappeds]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Drives'
ALTER TABLE [dbo].[Drives]
ADD CONSTRAINT [PK_Drives]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [ID] in table 'DroitAcces'
ALTER TABLE [dbo].[DroitAcces]
ADD CONSTRAINT [PK_DroitAcces]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Historiques'
ALTER TABLE [dbo].[Historiques]
ADD CONSTRAINT [PK_Historiques]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [id] in table 'Nationalites'
ALTER TABLE [dbo].[Nationalites]
ADD CONSTRAINT [PK_Nationalites]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'NatureOperations'
ALTER TABLE [dbo].[NatureOperations]
ADD CONSTRAINT [PK_NatureOperations]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [ID] in table 'Operations_Status'
ALTER TABLE [dbo].[Operations_Status]
ADD CONSTRAINT [PK_Operations_Status]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [id] in table 'piecejustificatives'
ALTER TABLE [dbo].[piecejustificatives]
ADD CONSTRAINT [PK_piecejustificatives]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'proprietaires'
ALTER TABLE [dbo].[proprietaires]
ADD CONSTRAINT [PK_proprietaires]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'quartiers'
ALTER TABLE [dbo].[quartiers]
ADD CONSTRAINT [PK_quartiers]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'regions'
ALTER TABLE [dbo].[regions]
ADD CONSTRAINT [PK_regions]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'saisies'
ALTER TABLE [dbo].[saisies]
ADD CONSTRAINT [PK_saisies]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'TypeActes'
ALTER TABLE [dbo].[TypeActes]
ADD CONSTRAINT [PK_TypeActes]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'TypeActeurs'
ALTER TABLE [dbo].[TypeActeurs]
ADD CONSTRAINT [PK_TypeActeurs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [ID] in table 'TypeEntreprises'
ALTER TABLE [dbo].[TypeEntreprises]
ADD CONSTRAINT [PK_TypeEntreprises]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [id] in table 'typelieux'
ALTER TABLE [dbo].[typelieux]
ADD CONSTRAINT [PK_typelieux]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typeoperations'
ALTER TABLE [dbo].[typeoperations]
ADD CONSTRAINT [PK_typeoperations]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typepieces'
ALTER TABLE [dbo].[typepieces]
ADD CONSTRAINT [PK_typepieces]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typeproprietaires'
ALTER TABLE [dbo].[typeproprietaires]
ADD CONSTRAINT [PK_typeproprietaires]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typesaisies'
ALTER TABLE [dbo].[typesaisies]
ADD CONSTRAINT [PK_typesaisies]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'TypesOperations'
ALTER TABLE [dbo].[TypesOperations]
ADD CONSTRAINT [PK_TypesOperations]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typetfs'
ALTER TABLE [dbo].[typetfs]
ADD CONSTRAINT [PK_typetfs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'typeutilisaturs'
ALTER TABLE [dbo].[typeutilisaturs]
ADD CONSTRAINT [PK_typeutilisaturs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'utilisateurs'
ALTER TABLE [dbo].[utilisateurs]
ADD CONSTRAINT [PK_utilisateurs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Vendors'
ALTER TABLE [dbo].[Vendors]
ADD CONSTRAINT [PK_Vendors]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'saisie11'
ALTER TABLE [dbo].[saisie11]
ADD CONSTRAINT [PK_saisie11]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [IDHistoriques], [IDOperations] in table 'HistoriquesOperations'
ALTER TABLE [dbo].[HistoriquesOperations]
ADD CONSTRAINT [PK_HistoriquesOperations]
    PRIMARY KEY CLUSTERED ([IDHistoriques], [IDOperations] ASC);
GO

-- Creating primary key on [id] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [PK_Operations]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [PK_tfs]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [Id] in table 'Tfs_Fusions'
ALTER TABLE [dbo].[Tfs_Fusions]
ADD CONSTRAINT [PK_Tfs_Fusions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [DroitAcces_ID], [typeutilisaturs_id] in table 'DroitsRoles'
ALTER TABLE [dbo].[DroitsRoles]
ADD CONSTRAINT [PK_DroitsRoles]
    PRIMARY KEY CLUSTERED ([DroitAcces_ID], [typeutilisaturs_id] ASC);
GO

-- Creating primary key on [TypeActes_id], [TypesOperations_id] in table 'TypesOperationTypeActes'
ALTER TABLE [dbo].[TypesOperationTypeActes]
ADD CONSTRAINT [PK_TypesOperationTypeActes]
    PRIMARY KEY CLUSTERED ([TypeActes_id], [TypesOperations_id] ASC);
GO

-- Creating primary key on [Acteurs_id], [Vendors_id] in table 'VendorsActeurs'
ALTER TABLE [dbo].[VendorsActeurs]
ADD CONSTRAINT [PK_VendorsActeurs]
    PRIMARY KEY CLUSTERED ([Acteurs_id], [Vendors_id] ASC);
GO

-- Creating primary key on [proprietaires_id], [Vendors_id] in table 'Vendorsproprietaire'
ALTER TABLE [dbo].[Vendorsproprietaire]
ADD CONSTRAINT [PK_Vendorsproprietaire]
    PRIMARY KEY CLUSTERED ([proprietaires_id], [Vendors_id] ASC);
GO

-- Creating primary key on [Operations_id], [proprietaires_id] in table 'Operationsproprietaire'
ALTER TABLE [dbo].[Operationsproprietaire]
ADD CONSTRAINT [PK_Operationsproprietaire]
    PRIMARY KEY CLUSTERED ([Operations_id], [proprietaires_id] ASC);
GO

-- Creating primary key on [Historiques_ID], [tfs_id] in table 'HistoriquesTFs'
ALTER TABLE [dbo].[HistoriquesTFs]
ADD CONSTRAINT [PK_HistoriquesTFs]
    PRIMARY KEY CLUSTERED ([Historiques_ID], [tfs_id] ASC);
GO

-- Creating primary key on [proprietaires_id], [tfs_id] in table 'tfproprietaire'
ALTER TABLE [dbo].[tfproprietaire]
ADD CONSTRAINT [PK_tfproprietaire]
    PRIMARY KEY CLUSTERED ([proprietaires_id], [tfs_id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [idActeur] in table 'Actes'
ALTER TABLE [dbo].[Actes]
ADD CONSTRAINT [FK_Actes_Acteurs]
    FOREIGN KEY ([idActeur])
    REFERENCES [dbo].[Acteurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Actes_Acteurs'
CREATE INDEX [IX_FK_Actes_Acteurs]
ON [dbo].[Actes]
    ([idActeur]);
GO

-- Creating foreign key on [idTypeActe] in table 'Actes'
ALTER TABLE [dbo].[Actes]
ADD CONSTRAINT [FK_Actes_TypeActes1]
    FOREIGN KEY ([idTypeActe])
    REFERENCES [dbo].[TypeActes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Actes_TypeActes1'
CREATE INDEX [IX_FK_Actes_TypeActes1]
ON [dbo].[Actes]
    ([idTypeActe]);
GO

-- Creating foreign key on [idTypeActeur] in table 'Acteurs'
ALTER TABLE [dbo].[Acteurs]
ADD CONSTRAINT [FK_Acteurs_TypeActeur]
    FOREIGN KEY ([idTypeActeur])
    REFERENCES [dbo].[TypeActeurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acteurs_TypeActeur'
CREATE INDEX [IX_FK_Acteurs_TypeActeur]
ON [dbo].[Acteurs]
    ([idTypeActeur]);
GO

-- Creating foreign key on [IDTypeProprietaire] in table 'Acteurs'
ALTER TABLE [dbo].[Acteurs]
ADD CONSTRAINT [FK_Acteurs_typeproprietaire]
    FOREIGN KEY ([IDTypeProprietaire])
    REFERENCES [dbo].[typeproprietaires]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Acteurs_typeproprietaire'
CREATE INDEX [IX_FK_Acteurs_typeproprietaire]
ON [dbo].[Acteurs]
    ([IDTypeProprietaire]);
GO

-- Creating foreign key on [UserActions] in table 'Historiques'
ALTER TABLE [dbo].[Historiques]
ADD CONSTRAINT [FK_Historiques_utilisateur]
    FOREIGN KEY ([UserActions])
    REFERENCES [dbo].[utilisateurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Historiques_utilisateur'
CREATE INDEX [IX_FK_Historiques_utilisateur]
ON [dbo].[Historiques]
    ([UserActions]);
GO

-- Creating foreign key on [nationalite] in table 'proprietaires'
ALTER TABLE [dbo].[proprietaires]
ADD CONSTRAINT [FK_proprietaire_Nationalite]
    FOREIGN KEY ([nationalite])
    REFERENCES [dbo].[Nationalites]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_proprietaire_Nationalite'
CREATE INDEX [IX_FK_proprietaire_Nationalite]
ON [dbo].[proprietaires]
    ([nationalite]);
GO

-- Creating foreign key on [idNatureOperation] in table 'TypesOperations'
ALTER TABLE [dbo].[TypesOperations]
ADD CONSTRAINT [FK_TypesOperation_NatureOperation]
    FOREIGN KEY ([idNatureOperation])
    REFERENCES [dbo].[NatureOperations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TypesOperation_NatureOperation'
CREATE INDEX [IX_FK_TypesOperation_NatureOperation]
ON [dbo].[TypesOperations]
    ([idNatureOperation]);
GO

-- Creating foreign key on [typeproprietaire] in table 'proprietaires'
ALTER TABLE [dbo].[proprietaires]
ADD CONSTRAINT [FK_proprietaire_typeproprietaire]
    FOREIGN KEY ([typeproprietaire])
    REFERENCES [dbo].[typeproprietaires]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_proprietaire_typeproprietaire'
CREATE INDEX [IX_FK_proprietaire_typeproprietaire]
ON [dbo].[proprietaires]
    ([typeproprietaire]);
GO

-- Creating foreign key on [regionid] in table 'quartiers'
ALTER TABLE [dbo].[quartiers]
ADD CONSTRAINT [FK_quartier_region]
    FOREIGN KEY ([regionid])
    REFERENCES [dbo].[regions]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_quartier_region'
CREATE INDEX [IX_FK_quartier_region]
ON [dbo].[quartiers]
    ([regionid]);
GO

-- Creating foreign key on [user_create] in table 'quartiers'
ALTER TABLE [dbo].[quartiers]
ADD CONSTRAINT [FK_quartier_utilisateur]
    FOREIGN KEY ([user_create])
    REFERENCES [dbo].[utilisateurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_quartier_utilisateur'
CREATE INDEX [IX_FK_quartier_utilisateur]
ON [dbo].[quartiers]
    ([user_create]);
GO

-- Creating foreign key on [typesaisie] in table 'saisies'
ALTER TABLE [dbo].[saisies]
ADD CONSTRAINT [FK_saisie_typesaisie]
    FOREIGN KEY ([typesaisie])
    REFERENCES [dbo].[typesaisies]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_saisie_typesaisie'
CREATE INDEX [IX_FK_saisie_typesaisie]
ON [dbo].[saisies]
    ([typesaisie]);
GO

-- Creating foreign key on [utilistaurid] in table 'saisies'
ALTER TABLE [dbo].[saisies]
ADD CONSTRAINT [FK_saisie_utilisateur]
    FOREIGN KEY ([utilistaurid])
    REFERENCES [dbo].[utilisateurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_saisie_utilisateur'
CREATE INDEX [IX_FK_saisie_utilisateur]
ON [dbo].[saisies]
    ([utilistaurid]);
GO

-- Creating foreign key on [ID_Type_entreprise] in table 'Vendors'
ALTER TABLE [dbo].[Vendors]
ADD CONSTRAINT [FK_Vendors_TypeEntreprise]
    FOREIGN KEY ([ID_Type_entreprise])
    REFERENCES [dbo].[TypeEntreprises]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Vendors_TypeEntreprise'
CREATE INDEX [IX_FK_Vendors_TypeEntreprise]
ON [dbo].[Vendors]
    ([ID_Type_entreprise]);
GO

-- Creating foreign key on [typeutilisatur] in table 'utilisateurs'
ALTER TABLE [dbo].[utilisateurs]
ADD CONSTRAINT [FK_utilisateur_typeutilisatur]
    FOREIGN KEY ([typeutilisatur])
    REFERENCES [dbo].[typeutilisaturs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_utilisateur_typeutilisatur'
CREATE INDEX [IX_FK_utilisateur_typeutilisatur]
ON [dbo].[utilisateurs]
    ([typeutilisatur]);
GO

-- Creating foreign key on [DroitAcces_ID] in table 'DroitsRoles'
ALTER TABLE [dbo].[DroitsRoles]
ADD CONSTRAINT [FK_DroitsRoles_DroitAcces]
    FOREIGN KEY ([DroitAcces_ID])
    REFERENCES [dbo].[DroitAcces]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [typeutilisaturs_id] in table 'DroitsRoles'
ALTER TABLE [dbo].[DroitsRoles]
ADD CONSTRAINT [FK_DroitsRoles_typeutilisatur]
    FOREIGN KEY ([typeutilisaturs_id])
    REFERENCES [dbo].[typeutilisaturs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DroitsRoles_typeutilisatur'
CREATE INDEX [IX_FK_DroitsRoles_typeutilisatur]
ON [dbo].[DroitsRoles]
    ([typeutilisaturs_id]);
GO

-- Creating foreign key on [TypeActes_id] in table 'TypesOperationTypeActes'
ALTER TABLE [dbo].[TypesOperationTypeActes]
ADD CONSTRAINT [FK_TypesOperationTypeActes_TypeActes]
    FOREIGN KEY ([TypeActes_id])
    REFERENCES [dbo].[TypeActes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TypesOperations_id] in table 'TypesOperationTypeActes'
ALTER TABLE [dbo].[TypesOperationTypeActes]
ADD CONSTRAINT [FK_TypesOperationTypeActes_TypesOperation]
    FOREIGN KEY ([TypesOperations_id])
    REFERENCES [dbo].[TypesOperations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TypesOperationTypeActes_TypesOperation'
CREATE INDEX [IX_FK_TypesOperationTypeActes_TypesOperation]
ON [dbo].[TypesOperationTypeActes]
    ([TypesOperations_id]);
GO

-- Creating foreign key on [Acteurs_id] in table 'VendorsActeurs'
ALTER TABLE [dbo].[VendorsActeurs]
ADD CONSTRAINT [FK_VendorsActeurs_Acteurs]
    FOREIGN KEY ([Acteurs_id])
    REFERENCES [dbo].[Acteurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Vendors_id] in table 'VendorsActeurs'
ALTER TABLE [dbo].[VendorsActeurs]
ADD CONSTRAINT [FK_VendorsActeurs_Vendors]
    FOREIGN KEY ([Vendors_id])
    REFERENCES [dbo].[Vendors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_VendorsActeurs_Vendors'
CREATE INDEX [IX_FK_VendorsActeurs_Vendors]
ON [dbo].[VendorsActeurs]
    ([Vendors_id]);
GO

-- Creating foreign key on [proprietaires_id] in table 'Vendorsproprietaire'
ALTER TABLE [dbo].[Vendorsproprietaire]
ADD CONSTRAINT [FK_Vendorsproprietaire_proprietaire]
    FOREIGN KEY ([proprietaires_id])
    REFERENCES [dbo].[proprietaires]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Vendors_id] in table 'Vendorsproprietaire'
ALTER TABLE [dbo].[Vendorsproprietaire]
ADD CONSTRAINT [FK_Vendorsproprietaire_Vendors]
    FOREIGN KEY ([Vendors_id])
    REFERENCES [dbo].[Vendors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Vendorsproprietaire_Vendors'
CREATE INDEX [IX_FK_Vendorsproprietaire_Vendors]
ON [dbo].[Vendorsproprietaire]
    ([Vendors_id]);
GO

-- Creating foreign key on [IDActes] in table 'HistoriquesOperations'
ALTER TABLE [dbo].[HistoriquesOperations]
ADD CONSTRAINT [FK_HistoriquesOperations_Actes]
    FOREIGN KEY ([IDActes])
    REFERENCES [dbo].[Actes]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoriquesOperations_Actes'
CREATE INDEX [IX_FK_HistoriquesOperations_Actes]
ON [dbo].[HistoriquesOperations]
    ([IDActes]);
GO

-- Creating foreign key on [IDHistoriques] in table 'HistoriquesOperations'
ALTER TABLE [dbo].[HistoriquesOperations]
ADD CONSTRAINT [FK_HistoriquesOperations_Historiques]
    FOREIGN KEY ([IDHistoriques])
    REFERENCES [dbo].[Historiques]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [idOperation] in table 'Actes'
ALTER TABLE [dbo].[Actes]
ADD CONSTRAINT [FK_Actes_Operations]
    FOREIGN KEY ([idOperation])
    REFERENCES [dbo].[Operations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Actes_Operations'
CREATE INDEX [IX_FK_Actes_Operations]
ON [dbo].[Actes]
    ([idOperation]);
GO

-- Creating foreign key on [idActeur] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_Acteurs]
    FOREIGN KEY ([idActeur])
    REFERENCES [dbo].[Acteurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_Acteurs'
CREATE INDEX [IX_FK_Operations_Acteurs]
ON [dbo].[Operations]
    ([idActeur]);
GO

-- Creating foreign key on [IDOperations] in table 'HistoriquesOperations'
ALTER TABLE [dbo].[HistoriquesOperations]
ADD CONSTRAINT [FK_HistoriquesOperations_Operations]
    FOREIGN KEY ([IDOperations])
    REFERENCES [dbo].[Operations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoriquesOperations_Operations'
CREATE INDEX [IX_FK_HistoriquesOperations_Operations]
ON [dbo].[HistoriquesOperations]
    ([IDOperations]);
GO

-- Creating foreign key on [idNatureOperation] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_NatureOperation]
    FOREIGN KEY ([idNatureOperation])
    REFERENCES [dbo].[NatureOperations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_NatureOperation'
CREATE INDEX [IX_FK_Operations_NatureOperation]
ON [dbo].[Operations]
    ([idNatureOperation]);
GO

-- Creating foreign key on [IDStatus] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_Operations_Status]
    FOREIGN KEY ([IDStatus])
    REFERENCES [dbo].[Operations_Status]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_Operations_Status'
CREATE INDEX [IX_FK_Operations_Operations_Status]
ON [dbo].[Operations]
    ([IDStatus]);
GO

-- Creating foreign key on [idTypeOperation] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_TypesOperation]
    FOREIGN KEY ([idTypeOperation])
    REFERENCES [dbo].[TypesOperations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_TypesOperation'
CREATE INDEX [IX_FK_Operations_TypesOperation]
ON [dbo].[Operations]
    ([idTypeOperation]);
GO

-- Creating foreign key on [idVendor] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_Vendors]
    FOREIGN KEY ([idVendor])
    REFERENCES [dbo].[Vendors]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_Vendors'
CREATE INDEX [IX_FK_Operations_Vendors]
ON [dbo].[Operations]
    ([idVendor]);
GO

-- Creating foreign key on [Operations_id] in table 'Operationsproprietaire'
ALTER TABLE [dbo].[Operationsproprietaire]
ADD CONSTRAINT [FK_Operationsproprietaire_Operation]
    FOREIGN KEY ([Operations_id])
    REFERENCES [dbo].[Operations]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [proprietaires_id] in table 'Operationsproprietaire'
ALTER TABLE [dbo].[Operationsproprietaire]
ADD CONSTRAINT [FK_Operationsproprietaire_proprietaire]
    FOREIGN KEY ([proprietaires_id])
    REFERENCES [dbo].[proprietaires]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operationsproprietaire_proprietaire'
CREATE INDEX [IX_FK_Operationsproprietaire_proprietaire]
ON [dbo].[Operationsproprietaire]
    ([proprietaires_id]);
GO

-- Creating foreign key on [idTF] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_Operations_tf]
    FOREIGN KEY ([idTF])
    REFERENCES [dbo].[tfs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Operations_tf'
CREATE INDEX [IX_FK_Operations_tf]
ON [dbo].[Operations]
    ([idTF]);
GO

-- Creating foreign key on [PIECEJUSTIFICATIVE_id] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [FK_tf_piecejustificative]
    FOREIGN KEY ([PIECEJUSTIFICATIVE_id])
    REFERENCES [dbo].[piecejustificatives]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tf_piecejustificative'
CREATE INDEX [IX_FK_tf_piecejustificative]
ON [dbo].[tfs]
    ([PIECEJUSTIFICATIVE_id]);
GO

-- Creating foreign key on [idquartier] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [FK_tf_quartier]
    FOREIGN KEY ([idquartier])
    REFERENCES [dbo].[quartiers]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tf_quartier'
CREATE INDEX [IX_FK_tf_quartier]
ON [dbo].[tfs]
    ([idquartier]);
GO

-- Creating foreign key on [typelieu] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [FK_tf_typelieu]
    FOREIGN KEY ([typelieu])
    REFERENCES [dbo].[typelieux]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tf_typelieu'
CREATE INDEX [IX_FK_tf_typelieu]
ON [dbo].[tfs]
    ([typelieu]);
GO

-- Creating foreign key on [typetf] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [FK_tf_typetf]
    FOREIGN KEY ([typetf])
    REFERENCES [dbo].[typetfs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tf_typetf'
CREATE INDEX [IX_FK_tf_typetf]
ON [dbo].[tfs]
    ([typetf]);
GO

-- Creating foreign key on [usercreated] in table 'tfs'
ALTER TABLE [dbo].[tfs]
ADD CONSTRAINT [FK_tf_utilisateur]
    FOREIGN KEY ([usercreated])
    REFERENCES [dbo].[utilisateurs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tf_utilisateur'
CREATE INDEX [IX_FK_tf_utilisateur]
ON [dbo].[tfs]
    ([usercreated]);
GO

-- Creating foreign key on [idTF] in table 'Vendors'
ALTER TABLE [dbo].[Vendors]
ADD CONSTRAINT [FK_Vendors_tf]
    FOREIGN KEY ([idTF])
    REFERENCES [dbo].[tfs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Vendors_tf'
CREATE INDEX [IX_FK_Vendors_tf]
ON [dbo].[Vendors]
    ([idTF]);
GO

-- Creating foreign key on [Historiques_ID] in table 'HistoriquesTFs'
ALTER TABLE [dbo].[HistoriquesTFs]
ADD CONSTRAINT [FK_HistoriquesTFs_Historique]
    FOREIGN KEY ([Historiques_ID])
    REFERENCES [dbo].[Historiques]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [tfs_id] in table 'HistoriquesTFs'
ALTER TABLE [dbo].[HistoriquesTFs]
ADD CONSTRAINT [FK_HistoriquesTFs_tf]
    FOREIGN KEY ([tfs_id])
    REFERENCES [dbo].[tfs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoriquesTFs_tf'
CREATE INDEX [IX_FK_HistoriquesTFs_tf]
ON [dbo].[HistoriquesTFs]
    ([tfs_id]);
GO

-- Creating foreign key on [proprietaires_id] in table 'tfproprietaire'
ALTER TABLE [dbo].[tfproprietaire]
ADD CONSTRAINT [FK_tfproprietaire_proprietaire]
    FOREIGN KEY ([proprietaires_id])
    REFERENCES [dbo].[proprietaires]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [tfs_id] in table 'tfproprietaire'
ALTER TABLE [dbo].[tfproprietaire]
ADD CONSTRAINT [FK_tfproprietaire_tf]
    FOREIGN KEY ([tfs_id])
    REFERENCES [dbo].[tfs]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_tfproprietaire_tf'
CREATE INDEX [IX_FK_tfproprietaire_tf]
ON [dbo].[tfproprietaire]
    ([tfs_id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------