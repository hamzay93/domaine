﻿using aejw.Network;
using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GestionTitreFoncierApp
{
    public class mapDrivepathfind
    {


        public static string finddestinationimage(string numerotf,string sharedpath)
        {

            //string pathDestination = sharedpath;
            // string pathDestination = @"\\SRVECVM1\Domain";
            string pathDestination = sharedpath;
            //string pathDestination = @"\\SERVER2\Users\Administrator\Desktop\Domain";
            string targetdestination = "";
            try
            {

                targetdestination = getrealphysicpath(pathDestination, numerotf, 2);
                return targetdestination;

            }
            catch (Exception err)
            {
                return "";
            }

        }

        public static void TrnsmitImages()
        {
            string pathsource = @"\\SERVER2\Users\Administrator\Desktop\Source1";
            //string pathsource = @"\\SERVER2\Users\Administrator\Desktop\Source";

            string pathDestination = @"\\SERVER2\Users\Administrator\Desktop\Domain";
            //string pathDestination = @"\\SERVER2\Users\Administrator\Desktop\Domain";

            for (int i = 14119; i < 14158; i++)
            {
                string targetnumerotf = i.ToString();

                string SourceImagesFolder = getrealphysicpath(pathsource, targetnumerotf, 1);
                //string SourceImagesFolder = getrealphysicpath(pathsource, targetnumerotf, 1);
                if (SourceImagesFolder != "" && SourceImagesFolder != null && SourceImagesFolder.IndexOf('D') != -1 && SourceImagesFolder.IndexOf('J') != -1 && SourceImagesFolder.IndexOf('I') != -1 && SourceImagesFolder.IndexOf('B') != -1)
                {
                    string DestinationImagesFolder = getrealphysicpath(pathDestination, targetnumerotf, 2);
                    //   SourceImagesFolder= getparentpath(SourceImagesFolder);

                    CoptyFolders(SourceImagesFolder, DestinationImagesFolder);
                }
            }
        }

        public static void CoptyFolders(string SourcP, string DestinP)
        {
            foreach (string newPath in Directory.GetFiles(SourcP, "*.*",
              SearchOption.AllDirectories))
            {
                string parentonDistnation = getEqual(SourcP, 1).ToString();
                if (!Directory.Exists(DestinP + "\\" + parentonDistnation))
                {
                    Directory.CreateDirectory(DestinP + "\\DJIB-TF-" + parentonDistnation);

                }
                File.Copy(newPath, newPath.Replace(SourcP, DestinP + "\\DJIB-TF-" + parentonDistnation), true);
            }
        }
        public static string getrealphysicpath(string imp, string tfnumerof, int pathtype)
        {
            string Numerotf = tfnumerof;
            int targetnumerotf = Convert.ToInt32(tfnumerof);
            string completepathtoReturn = "";
            bool endllop;
            int ptyp = pathtype;
            ///
            if (pathtype == 1)
            { checkSubFolderst(imp); }
            ///
            //   if (endllop) { return null; }

            // string sharedpath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
            //   string NumeroTFFoldePath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
            if (Directory.GetDirectories(imp).Length > 0)
            // if ((Directory.GetDirectories(imp).Length > 0 && ptyp == 1) || (Directory.GetDirectories(imp).Length > 0 && ptyp == 2 && imp.IndexOf('J') == -1 && imp.IndexOf('I') == -1 && imp.IndexOf('B') == -1))
            {
                string minnumber;
                string maxnumber;
                string equalNumber;
                int min;
                int max;
                int equal;
                for (int i = 0; i < Directory.GetDirectories(imp).Length; i++)
                {
                    string FolderName = Directory.GetDirectories(imp)[i];
                    string currentnumerotf = getnumerotffromparent(FolderName);
                    if (currentnumerotf != "")
                    {
                        if (pathtype == 2)
                        {
                            completepathtoReturn = getparentpath(FolderName);
                            break;
                        }


                    }
                    //if ((Directory.GetDirectories(FolderName).Length > 0 && ptyp == 1) || (Directory.GetDirectories(FolderName).Length > 0 && ptyp == 2 && FolderName.IndexOf('B') == -1 && currentnumerotf == ""))
                    else if (Directory.GetDirectories(FolderName).Length > 0)

                    {

                        minnumber = getMinLimit(FolderName);
                        maxnumber = getMaxLimit(FolderName);
                        min = Convert.ToInt32(minnumber);
                        max = Convert.ToInt32(maxnumber);

                        if (targetnumerotf >= min && targetnumerotf <= max)
                        {
                            return getrealphysicpath(FolderName, Numerotf, ptyp);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else if ((Directory.GetDirectories(FolderName).Length == 0 && pathtype == 1) || pathtype == 2)
                    {

                        equalNumber = getEqual(FolderName, ptyp);
                        //if (ptyp == 1 && equalNumber.Contains("DJIB-TF"))
                        //rawstring.IndexOf('à');
                        if (FolderName.IndexOf('D') != -1 && FolderName.IndexOf('J') != -1 && FolderName.IndexOf('I') != -1 && FolderName.IndexOf('B') != -1)

                        {
                            equal = Convert.ToInt32(equalNumber);
                            if (equal == targetnumerotf)
                            {
                                completepathtoReturn = FolderName;
                                break;
                                // return (FolderName);
                            }
                        }
                        else
                        {
                            minnumber = getMinLimit(FolderName);
                            maxnumber = getMaxLimit(FolderName);
                            min = Convert.ToInt32(minnumber);
                            max = Convert.ToInt32(maxnumber);
                            if (targetnumerotf >= min && targetnumerotf <= max)
                            {
                                //return getrealphysicpath(FolderName, Numerotf, ptyp);
                                completepathtoReturn = FolderName;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }

                    else // Exception Some Thing Wrong
                    {
                        return "0";
                        //completepathtoReturn = "0";
                        //break;
                    }
                }
            }
            else
            {  // Exception Some Thing Wrong
                return "0";
                // completepathtoReturn = "0";
            }

            return completepathtoReturn;
        }
        //public static string getrealphysicpath(string imp, string tfnumerof, int pathtype)
        //{
        //    string Numerotf = tfnumerof;
        //    int targetnumerotf = Convert.ToInt32(tfnumerof);
        //    string completepathtoReturn = "";
        //    bool endllop;
        //    int ptyp = pathtype;
        //    ///
        //    if (pathtype == 1)
        //    { checkSubFolderst(imp); }
        //    ///
        //    //   if (endllop) { return null; }

        //    // string sharedpath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
        //    //   string NumeroTFFoldePath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
        //    if (Directory.GetDirectories(imp).Length > 0)
        //    // if ((Directory.GetDirectories(imp).Length > 0 && ptyp == 1) || (Directory.GetDirectories(imp).Length > 0 && ptyp == 2 && imp.IndexOf('J') == -1 && imp.IndexOf('I') == -1 && imp.IndexOf('B') == -1))
        //    {
        //        string minnumber;
        //        string maxnumber;
        //        string equalNumber;
        //        int min;
        //        int max;
        //        int equal;
        //        for (int i = 0; i < Directory.GetDirectories(imp).Length; i++)
        //        {
        //            string FolderName = Directory.GetDirectories(imp)[i];
        //            string currentnumerotf = getnumerotffromparent(FolderName);
        //            if (currentnumerotf != "")
        //            {
        //                if (pathtype == 2)
        //                {
        //                    completepathtoReturn = getparentpath(FolderName);
        //                    break;
        //                }


        //            }
        //            //if ((Directory.GetDirectories(FolderName).Length > 0 && ptyp == 1) || (Directory.GetDirectories(FolderName).Length > 0 && ptyp == 2 && FolderName.IndexOf('B') == -1 && currentnumerotf == ""))
        //            else if (Directory.GetDirectories(FolderName).Length > 0)

        //            {

        //                minnumber = getMinLimit(FolderName);
        //                maxnumber = getMaxLimit(FolderName);
        //                min = Convert.ToInt32(minnumber);
        //                max = Convert.ToInt32(maxnumber);

        //                if (targetnumerotf >= min && targetnumerotf <= max)
        //                {
        //                    return getrealphysicpath(FolderName, Numerotf, ptyp);
        //                }
        //                else
        //                {
        //                    continue;
        //                }
        //            }
        //            else if ((Directory.GetDirectories(FolderName).Length == 0 && pathtype == 1) || pathtype == 2)
        //            {

        //                equalNumber = getEqual(FolderName, ptyp);
        //                //if (ptyp == 1 && equalNumber.Contains("DJIB-TF"))
        //                //rawstring.IndexOf('à');
        //                if (FolderName.IndexOf('D') != -1 && FolderName.IndexOf('J') != -1 && FolderName.IndexOf('I') != -1 && FolderName.IndexOf('B') != -1)

        //                {
        //                    equal = Convert.ToInt32(equalNumber);
        //                    if (equal == targetnumerotf)
        //                    {
        //                        completepathtoReturn = FolderName;
        //                        break;
        //                        // return (FolderName);
        //                    }
        //                }
        //                else
        //                {
        //                    minnumber = getMinLimit(FolderName);
        //                    maxnumber = getMaxLimit(FolderName);
        //                    min = Convert.ToInt32(minnumber);
        //                    max = Convert.ToInt32(maxnumber);
        //                    if (targetnumerotf >= min && targetnumerotf <= max)
        //                    {
        //                        //return getrealphysicpath(FolderName, Numerotf, ptyp);
        //                        completepathtoReturn = FolderName;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        continue;
        //                    }
        //                }
        //            }

        //            else // Exception Some Thing Wrong
        //            {
        //                return "0";
        //                //completepathtoReturn = "0";
        //                //break;
        //            }
        //        }
        //    }
        //    else
        //    {  // Exception Some Thing Wrong
        //        return "0";
        //        // completepathtoReturn = "0";
        //    }

        //    return completepathtoReturn;
        //}


        public static string getMinLimit(string Fname)
        {

            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Fname[i].ToString() == "\\")
                {
                    break;
                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            string rawstring = numtf.Replace(" ", string.Empty).Replace("  ", "").Replace("   ", "").Replace("    ", "").Replace("a", "à").Replace("A", "à").Replace("À", "à");
            var pos = rawstring.IndexOf('à');
            string valueToReturn = rawstring.Substring(0, pos);

            return valueToReturn;
        }
        public static string getMaxLimit(string Fname)
        {
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Fname[i].ToString() == "\\")
                {
                    break;
                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            string rawstring = numtf.Replace(" ", string.Empty).Replace("  ", "").Replace("   ", "").Replace("    ", "").Replace("a", "à").Replace("A", "à").Replace("À", "à");
            var pos = rawstring.IndexOf('à');
            string valueToReturn = rawstring.Substring(pos + 1, ((rawstring.Length) - (pos + 1)));

            return valueToReturn;
        }
        public static string getEqual(string Fname, int Ptyp)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Fname.IndexOf('D') != -1 && Fname.IndexOf('J') != -1 && Fname.IndexOf('I') != -1 && Fname.IndexOf('B') != -1)
                {
                    if (Fname[i].ToString() == "-")
                    {
                        break;
                    }
                }
                else
                {
                    if (Fname[i].ToString() == "\\")
                    {
                        break;
                    }

                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            // intnumerotf = Convert.ToInt32(numtf);
            return numtf;
        }
        public static string getnumerotffromparent(string Fname)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            if (Fname.IndexOf('-') != -1)
            {
                for (int i = Fname.Length - 1; i > 0; i--)
                {

                    if (Fname[i].ToString() == "-")
                    {
                        break;
                    }

                    numerotfToDisplay = numerotfToDisplay + Fname[i];

                }
                char[] newstring = numerotfToDisplay.ToCharArray();
                Array.Reverse(newstring);
                string numtf = new string(newstring);
                // intnumerotf = Convert.ToInt32(numtf);
                return numtf;
            }
            else
            {
                return "";
            }
        }
        public static string getparentpath(string Fname)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            //if (Fname.IndexOf('-') != -1)
            //{
            for (int i = 0; i < Fname.Length; i++)
            {

                if (Fname[i].ToString() == "J")
                {
                    break;
                }

                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            // intnumerotf = Convert.ToInt32(numtf);]
            string ll = numerotfToDisplay;
            string ppath = numerotfToDisplay.Substring(0, numerotfToDisplay.Length - 2);
            return ppath;
            //}
            //else
            //{
            //    return Fname;
            //}    
            //   ziadali length=7   5   2

        }
        public static void checkSubFolderst(string Fname)
        {
            int count = Directory.GetDirectories(Fname).Length;
            //  string[] arr4 = new string[count];
            for (int i = 0; i < count; i++)
            {
                string f = Directory.GetDirectories(Fname)[i];
                if (Directory.GetFiles(f).Length != 0)
                {
                    string folder = f;
                    Trnsmit(f);
                }
            }

        }
        public static void Trnsmit(string Source)
        {
            string pathsource = Source;
            //string pathsource = @"\\SERVER2\Users\Administrator\Desktop\Source";

            //string pathDestination = @"\\SERVER2\Users\Administrator\Desktop\Domain";
            string pathDestination = @"\\TFSERVER\Domain";


            string targetnumerotf = getnumerotffromparent(Source);

            string SourceImagesFolder = Source;
            //string SourceImagesFolder = getrealphysicpath(pathsource, targetnumerotf, 1);
            if (SourceImagesFolder != "" && SourceImagesFolder != null && SourceImagesFolder.IndexOf('D') != -1 && SourceImagesFolder.IndexOf('J') != -1 && SourceImagesFolder.IndexOf('I') != -1 && SourceImagesFolder.IndexOf('B') != -1)
            {
                string DestinationImagesFolder = getrealphysicpath(pathDestination, targetnumerotf, 2);
                //   SourceImagesFolder= getparentpath(SourceImagesFolder);

                CoptyFolders(SourceImagesFolder, DestinationImagesFolder);
            }

        }
    }
}