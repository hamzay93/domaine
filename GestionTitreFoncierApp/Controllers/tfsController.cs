﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;
using Newtonsoft.Json;
using System.IO;
using GT.Models;
using PagedList.Mvc;
using PagedList;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using System.Security;
using System.Web.Security;
using GestionTitreFoncierApp.Security;
using System.Text.RegularExpressions;

namespace GestionTitreFoncierApp.Controllers
{
  [Authorize]
    public class tfsController : Controller
    {
        public GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        //public string localphysicpath = @"\\TFSERVER\Domain"; 
        public string localphysicpath = @"c:\Domain\";
        public string PhysicalPath = "C:/Domain/";
        OperationsController OpController = new OperationsController();
        HistoriquesController historique = new HistoriquesController();
        // public string localphysicpath = @"\\ZIAD0Z\Users\ZiAd\Desktop\Domain";
        public ActionResult Acceuil()
        {
            return View();
        }
        public ActionResult viewcleanTF()
        {
            tf tf1 = db.tf.ToList().Last();

            return View("cleanView", tf1);

        }
        [HttpPost]
        public ActionResult viewcleanTF(int? z)
        {

            tf tf1 = (db.tf.ToList().Last());
           // getrealphysicpath(@"\\Ziad0z\Users\ziad\Desktop\Domain", "14126");
            tf1.proprietaire.Clear();
            db.Entry(tf1).State = EntityState.Modified;
            db.SaveChanges();
            db.tf.Remove(tf1);
            db.SaveChanges();
            // return RedirectToAction("Index");

            //return PartialView("imgViewer");
            return View("cleanView", tf1);
        }

        [FiltresAuthorisation(Roles = "Modifier TF,Special")]
        public ActionResult Edit(int? id)
        {
            try
            {
                    string[] strDrives = Environment.GetLogicalDrives();
                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    tf tf = db.tf.Find(id);
                    if (tf == null)
                    {
                        return HttpNotFound();
                    }

                    var results = tf.proprietaire.
                        Select(x => new
                        {
                            id = x.id,
                            prenom = x.prenom,
                            nome2 = x.nome2,
                            nom3 = x.nom3,
                            prenommere = x.prenommere,
                            nommere2 = x.nommere2,
                            nommere3 = x.nommere3,
                            lieuNaisance = x.lieuNaisance,
                            dateNaissace = x.dateNaissace,
                            numeropiece = x.numeropiece,
                            piecedateemission = x.piecedateemission,
                            typepiece = x.typepiece,
                            //nationalite = x.nationalite,
                            nomestablishment = x.nomestablishment,
                            typestablishment = x.typestablishment,
                            nomeresponsable1 = x.nomeresponsable1,
                            secturactivite = x.secturactivite,
                            typ = x.typeproprietaire
                            // Checked = db.books.Where(f=>f.id== bid && f.authors == author).Any()  db.books.Any(u=>u.authors==author)
                        }).ToList();


                    var MyViewModel = new TFSViewModel();
                    MyViewModel.id = id.Value;
                    MyViewModel.numerotf = tf.numerotf;
                    MyViewModel.datetf = tf.datetf;
                    MyViewModel.numacp = tf.numacp;
                    MyViewModel.valeurterrain = tf.valeurterrain;
                    MyViewModel.superficie = tf.superficie;
                    MyViewModel.DateOperation = tf.DateOperation;
                MyViewModel.Coopropriete = tf.Coopropriete;
                    MyViewModel.typetf1 = (db.typetf.Where(x => x.id == tf.typetf).FirstOrDefault());
                if(MyViewModel.typetf1 == null)
                {
                    MyViewModel.typetf1 = db.typetf.Find(1);
                }
                    MyViewModel.quartier = (db.quartier.Where(x => x.id == tf.idquartier).FirstOrDefault());
                MyViewModel.Unites_Surface = (db.Unites_Surface.Where(x => x.ID == tf.ID_Unites).FirstOrDefault());
                MyViewModel.piecejustificative = (db.piecejustificative.Where(x => x.id == tf.PIECEJUSTIFICATIVE_id).FirstOrDefault());
                if (MyViewModel.piecejustificative == null)
                {
                    MyViewModel.piecejustificative = db.piecejustificative.Find(4);
                }
                // MyViewModel.quartier.region = (db.quartiers.Where(x => x.id == tf.idquartier.).FirstOrDefault());
                // MyViewModel.quartier = (db.quartiers.Where(x => x.id == tf.idquartier).FirstOrDefault());
                MyViewModel.estbati = tf.estbati;
                    MyViewModel.estillot = tf.estillot;
                    MyViewModel.typelieu = tf.typelieu;
                    MyViewModel.numerolot = tf.numerolot;
                    MyViewModel.nomillot = tf.nomillot;
                    MyViewModel.typelieu = tf.typelieu;
                    MyViewModel.cheminimage = tf.cheminimage;

                    ViewBag.PIECEJUSTIFICATIVE_id = new SelectList(db.piecejustificative, "id", "libelle");
                    ViewBag.idquartier = new SelectList(db.quartier, "id", "libelle");
                ViewBag.ID_Unites = new SelectList(db.Unites_Surface, "ID", "Libelle");
                //ViewBag.typelieu = new SelectList(db.typelieux, "id", "libelle");
                ViewBag.typetf = new SelectList(db.typetf, "id", "libell");
                    //ViewBag.typeproprietaire = db.typetfs.ToList();
                    ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                    ViewBag.logicallistDrives = strDrives;
                    ViewBag.typelieu = new SelectList(db.typelieu, "id", "libelle");
                    int regid = Convert.ToInt32(tf.quartier.Ville.region.id);
                    int villeid = Convert.ToInt32(tf.quartier.Ville.id);
                    ViewBag.Regid = new SelectList(db.region, "id", "libelle", regid);
                    ViewBag.VilleID = new SelectList(db.Ville.OrderBy(i=>i.libelle), "id", "libelle", villeid);
                   
                    bool typliu = Convert.ToBoolean(tf.quartier.estlotissement);
                    var idquart = 1;
                  if (tf.idquartier != null) {  idquart = Convert.ToInt32(tf.idquartier); }
                    ViewBag.idquartier = new SelectList((db.quartier.Where(x => x.Ville.id == regid && x.estlotissement == typliu).OrderBy(i=>i.libelle).ToList()), "id", "libelle",idquart);
                // prepare for the image 
                ViewBag.ID_Unites = new SelectList(db.Unites_Surface, "ID", "Libelle",tf.ID_Unites);

                string[] imagestf;
                var NTF = Convert.ToString(tf.numerotf);
                var p = getdestpath(NTF);
                string ErrorPath = Server.MapPath(Url.Content("~/Images/ERRORS/"));
                string chemincomplet = "C:/Domain/" + p.Replace(@"\", "/");
                var folder = new DirectoryInfo(chemincomplet);
                if (folder.Exists)
                {
                    if (Directory.GetFiles(chemincomplet).Length == 0 && Directory.GetDirectories(chemincomplet).Length == 0)
                    {
                        imagestf = Directory.GetFiles(ErrorPath);
                    }
                    else
                    { imagestf = Directory.GetFiles(chemincomplet); }
                }
                else
                {
                    imagestf = Directory.GetFiles(ErrorPath);
                }

                byte[] imageByteData2 = System.IO.File.ReadAllBytes(imagestf[0].Replace(@"\", "/"));
                string imageBase64Data2 = Convert.ToBase64String(imageByteData2);
                string imageDataURL2 = string.Format("data:image/jpg;base64,{0}", imageBase64Data2);

                ViewBag.ImageData = imageDataURL2;
                ViewBag.indeximg = 0;
                ViewBag.nbrimages = imagestf.Length - 1;
                TempData["chemins"] = imagestf;

                //string pathofimg1 = returnimagepath(tf.numerotf, "2");
                //var server = db.DriversMappeds.Where(x => x.IsActive == true).FirstOrDefault();
                //string ipadress = server.Domian;
                //string username = server.username;
                //string password = server.Password;
                //string sharedpath = server.SharedPath;
                //int substringsharedpathindex = localphysicpath.Length;
                ////pathofimg1 = @"000 000 à 099 999\010 000 à 019 999\014 000 à 014 999\014 100 à 014 199\DJIB - TF - 14126";
                //string fttppaFTPPATH = pathofimg1.Substring(substringsharedpathindex, pathofimg1.Length - substringsharedpathindex);
                //// fttppaFTPPATH = @"000 000 à 099 999\010 000 à 019 999\014 000 à 014 999\014 100 à 014 199\DJIB-TF-14120";
                //fttppaFTPPATH = getdestpath(tf.numerotf);
                //ftp f = new ftp();
                ////string[] simpleDirectoryListing = f.GetFileList("ftp://" + ipadress + "/" + fttppaFTPPATH, username, password);
                //int countfile = 10;// simpleDirectoryListing.Length;
                //ViewBag.filescount = countfile;

                //ViewBag.pathofimg1 = "ftp://" + username + ":" + password + "@" + ipadress + "/" + fttppaFTPPATH.Replace(@"\", "/").Replace("à", "%E0").Replace(" ", "%20") + "/DJIB-TF-" + tf.numerotf + "%2000" + "1" + ".jpg";                    //ViewBag.imgnvaindex = imgnvaindex;
                ////var countfiles = Directory.GetFiles("ftp://ziad:heimp2856@192.168.1.41/" + pathofimg1);
                //ViewBag.typview = "1";
                //ViewBag.realphysicpath = pathofimg1;
                //ViewBag.numerotf = tf.numerotf; 

                //ViewBag.idquartier = new SelectList(db.quartiers, "id", "libelle");
                var propertairesviewmodel = new List<ProprietaresViewModel>();
                    foreach (var item in results)
                    {
                        propertairesviewmodel.Add(new ProprietaresViewModel
                        {
                            id = item.id,
                            prenom = item.prenom,
                            nome2 = item.nome2,
                            nom3 = item.nom3,
                            prenommere = item.prenommere,
                            nommere2 = item.nommere2,
                            nommere3 = item.nommere3,
                            lieuNaisance = item.lieuNaisance,
                            // dateNaissace = item.dateNaissace,
                            numeropiece = item.numeropiece,
                            // piecedateemission = item.piecedateemission,
                            typepiece = item.typepiece,
                            // nationalite = item.nationalite,
                            nomestablishment = item.nomestablishment,
                            typestablishment = item.typestablishment,
                            nomeresponsable1 = item.nomeresponsable1,
                            secturactivite = item.secturactivite,
                            typeproprietaire = item.typ
                        });
                    }
                    MyViewModel.proprietaires = propertairesviewmodel;
                    //return View(MyViewModel);
                    //  return PartialView("Edit", MyViewModel);
                    return View("EditTF", MyViewModel);

                
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }
        public ActionResult Index(string where, string searchtype, string searchBy, string searchinputn, string searchinputp, string numerotf, string pnom, string datetf, string typetf, string valeurterrain, string superficie, string estbati, string RegId, string typLieu, string idquartier, string estilot, string nomillot, string numerolot,string VilleId, string ArrondId,string CommuneId,string NumeroDrop,string numerotfFin,string DateDrop,string datetfFin)
        { 
            System.Globalization.CultureInfo culInfo = new System.Globalization.CultureInfo("en-US");
            //ViewBag.CurrentSort = sortOrder;
            //ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "tfs_desc" : "tfs";
            var Where = where;
            ViewData["Where"] = Where;

            ViewBag.typetf = new SelectList(db.typetf, "id", "libell");
            ViewBag.idquartier = new SelectList(db.quartier.OrderBy(i => i.libelle), "id", "libelle");
            ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle");
            ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle");
            ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle");
            ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle");

            var users = OpController.CurrentUsers(User.Identity.Name);
            var tfs = db.tf.Include(i => i.proprietaire).Where(i => i.Actif != false );
            if (!User.IsInRole("Blacklist TF"))
            {
                tfs = tfs.Where(i => i.EstPublic == true);
            }

           
            
            if (searchtype == "Basic")
            {
                if (searchBy == "numerotf")
                {
                    string numerostf = searchinputn;
                    int numeroTF = numerostf != "" ? Convert.ToInt32(numerostf) : 0;
                    if (numeroTF != 0)
                    {
                        tfs = tfs.Where(i => i.numerotf==numeroTF);
                        ViewBag.searchinputn = numerostf;
                    }
                    else
                    {
                        tfs = tfs.Where(i => i.datetf == DateTime.Now);
                    }
                }
                else if (searchBy == "proprietaire")
                {
                    string NomCompletProprietaires = searchinputp;
                    var TesteDedebutSpace = searchinputp.Replace(" ", String.Empty);
                 //   if (NomCompletProprietaires != "" && !NomCompletProprietaires.StartsWith(" "))
                    if(TesteDedebutSpace != null && TesteDedebutSpace != "")
                    {
                        ViewBag.searchinputp = searchinputp;
                        string[] ssize = NomCompletProprietaires.Split(null);
                        List<string> Siss = ssize.ToList();
                        Siss.RemoveAll(p => string.IsNullOrEmpty(p));
                        ssize = Siss.ToArray();
                        string firstproprietairename = "";
                        string secondproprietairename = "";
                        string thirdproprietairename = "";
                        if (ssize != null)
                        {
                            //firstproprietairename = EnleveSpace(ssize[0]);
                            if (ssize[0] != null && ssize[0] != "")
                            { firstproprietairename = ssize[0]; }
                            if (ssize.Count() >= 2)
                            { secondproprietairename = ssize[1]; }
                            if (ssize.Count() >= 3)
                            { thirdproprietairename = ssize[2]; }
                        }
                        var ALLName = "";
                        if (firstproprietairename != "" && secondproprietairename == "" && thirdproprietairename == "")
                        {
                            ALLName += firstproprietairename;
                            tfs = tfs.Where(x => x.proprietaire.Any(y => y.prenom.StartsWith(firstproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.Contains(firstproprietairename))).Distinct();
                        }
                        if (secondproprietairename != "" && thirdproprietairename == "")
                        {
                            if (firstproprietairename != "")
                            {
                                ALLName=firstproprietairename +" "  + secondproprietairename;
                                tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename) && y.prenom.StartsWith(firstproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(ALLName))).Distinct();
                            }
                            else
                            {
                                tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(secondproprietairename))).Distinct();
                            }
                        }
                        if (thirdproprietairename != "")
                        {
                            ALLName = firstproprietairename+" "+secondproprietairename+" " + thirdproprietairename;
                            if(ALLName != "")
                            {
                                tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename) &&  y.prenom.StartsWith(firstproprietairename) && y.nom3.StartsWith(thirdproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(ALLName))).Distinct();

                            }
                            else
                            {
                                tfs = tfs.Where(x => x.proprietaire.Any(y => y.nom3.StartsWith(thirdproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(thirdproprietairename))).Distinct();
                            }
                        }
                    }
                    else
                    {
                        tfs = tfs.Where(i => i.datetf == DateTime.Now);
                    }
                }
                else
                {
                    tfs = tfs.Where(i => i.datetf == DateTime.Now);
                }
            }
            else if (searchtype == "Advanced")
            {
                // string hkd = Request["estbati"];
                string NomCompletProprietaires = pnom;
                string[] ssize = null;
                var TesteDedebutSpace = pnom.Replace(" ", String.Empty);
                if (TesteDedebutSpace != "")
                {
                    ssize = NomCompletProprietaires.Split(null);
                    List<string> Siss = ssize.ToList();
                    Siss.RemoveAll(p => string.IsNullOrEmpty(p));
                    ssize = Siss.ToArray();
                }
                string firstproprietairename = "";
                string secondproprietairename = "";
                string thirdproprietairename = "";
                if (ssize != null)
                {
                    if (ssize[0] != null && ssize[0] != "")
                    { firstproprietairename = ssize[0]; }
                    if (ssize.Count() >= 2)
                    { secondproprietairename = ssize[1]; }
                    if (ssize.Count() >= 3)
                    { thirdproprietairename = ssize[2]; }
                }

                int notf = numerotf != "" ? Convert.ToInt32(numerotf) : 0;
                int numtffin = numerotfFin != "" ? Convert.ToInt32(numerotfFin) : 0; 
                int NumDrop = NumeroDrop != "" ? Convert.ToInt32(NumeroDrop) : 0;
                int datDrop = DateDrop != "" ? Convert.ToInt32(DateDrop) : 0;
                int typeoftf = typetf != "" ? Convert.ToInt32(typetf) : 0;
                int regID = RegId != "" ? Convert.ToInt32(RegId) : 0;
                int villeID = VilleId != "" ? Convert.ToInt32(VilleId) : 0;
                int communeID = CommuneId != "" ? Convert.ToInt32(CommuneId) : 0;
                int arrondID = ArrondId != "" ? Convert.ToInt32(ArrondId) : 0;

                int quartierID = idquartier != "" ? Convert.ToInt32(idquartier) : 0;
                string valeur = valeurterrain != null && valeurterrain != "" ? valeurterrain : "";
                string superfici = superficie != null && superficie != "" ? superficie : "";
                DateTime dtimm = DateTime.Today;
                if (datetf != "")
                {
                    dtimm = Convert.ToDateTime(datetf);
                }
                DateTime datefin = DateTime.Today;
                if(datetfFin != "")
                {
                    datefin = Convert.ToDateTime(datetfFin).AddHours(23).AddMinutes(59).AddSeconds(59) ;
                        //"23:59:59";
                }
                
                //  DateTime todaydate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                string nomilot = nomillot != null && nomillot != "" ? nomillot : "";
                string numeroilot = numerolot != null && numerolot != "" ? numerolot : "";


                int typeliu;
                string liutp = typLieu;
                var Quartier = db.quartier.ToList();
                if (liutp == "Quart")
                {
                    typeliu = 1;
                    Quartier = Quartier.Where(k => k.estlotissement == false).ToList();
                }
                else if (liutp == "Lotis")
                {
                    typeliu = 2;
                    Quartier = Quartier.Where(k => k.estlotissement == true).ToList();
                }
                else
                {
                    typeliu = 3;//All Types
                    
                }

                bool? ilot;
                string ilotsting = estilot;
                if (ilotsting == "yes")
                {
                    ilot = true;
                }
                else if (ilotsting == "no")
                {
                    ilot = false;
                }

                else
                {
                    ilot = null;//All Types
                }

                bool? bati;
                string batisting = estbati;
                if (batisting == "yes")
                {
                    bati = true;
                }
                else if (batisting == "no")
                {
                    bati = false;
                }

                else
                {
                    bati = null;//All Types
                }

                // Par numeros tf 
                if (NumDrop == 1) // Egale à
                {
                    if (notf != 0)
                    {
                        tfs = tfs.Where(i => i.numerotf == notf);
                    }
                }
                else if(NumDrop == 2) // Inferieur à
                {
                    if (numtffin != 0)
                    {
                       tfs = tfs.Where(i => i.numerotf <= numtffin);
                    }
                }
                else if (NumDrop == 3) // Superieur à
                {
                    if (notf != 0)
                    {
                        tfs = tfs.Where(i => i.numerotf >= notf);
                    }
                }
                else if (NumDrop == 4) // Entre 
                {
                    if (notf != 0)
                    {
                        tfs = tfs.Where(i => i.numerotf >= notf);
                    }
                    if (numtffin != 0)
                    {
                        tfs = tfs.Where(i => i.numerotf <= numtffin);
                    }
                }
                //if (notf != "")
                //    {
                //        tfs = tfs.Where(i => i.numerotf == notf);
                //    }

                // Par Date Immatriculation
                if (datDrop == 1)  // Egale à
                {
                    if (dtimm != DateTime.Today && datetf != "")
                    {
                        tfs = tfs.Where(i => i.datetf == dtimm);
                    }
                }
                else if (datDrop == 2) // Inferieur à
                {
                    if (datefin != DateTime.Today && datetfFin != "")
                    {
                        tfs = tfs.Where(i => i.datetf <= datefin);
                    }
                }
                else if (datDrop == 3) // Superieur à
                {
                    if (dtimm != DateTime.Today && datetf != "")
                    {
                        tfs = tfs.Where(i => i.datetf >= dtimm);
                    }
                }
                else if (datDrop == 4) // Entre 
                {
                    if (datefin != DateTime.Today && datetfFin != "")
                    {
                        tfs = tfs.Where(i => i.datetf <= datefin);
                    }
                    if (dtimm != DateTime.Today && datetf != "")
                    {
                        tfs = tfs.Where(i => i.datetf >= dtimm);
                    }
                }
                //if (dtimm != DateTime.Today && datetf != "")
                //{
                //    tfs = tfs.Where(i => i.datetf == dtimm);
                //}

                //if (firstproprietairename != "")
                //{
                //    tfs = tfs.Where(x => x.proprietaires.Any(y => y.prenom.StartsWith(firstproprietairename)) || x.proprietaires.Any(y => y.nomestablishment.StartsWith(firstproprietairename))).Distinct();
                //}
                //if (secondproprietairename != "")
                //{
                //    tfs = tfs.Where(x => x.proprietaires.Any(y => y.nome2.StartsWith(secondproprietairename)) || x.proprietaires.Any(y => y.nomestablishment.StartsWith(secondproprietairename))).Distinct();
                //}
                //if (thirdproprietairename != "")
                //{
                //    tfs = tfs.Where(x => x.proprietaires.Any(y => y.nom3.StartsWith(thirdproprietairename)) || x.proprietaires.Any(y => y.nomestablishment.StartsWith(thirdproprietairename))).Distinct();
                //}
                var ALLName = "";
                if (firstproprietairename != "" && secondproprietairename == "" && thirdproprietairename == "")
                {
                    ALLName += firstproprietairename;
                    tfs = tfs.Where(x => x.proprietaire.Any(y => y.prenom.StartsWith(firstproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.Contains(firstproprietairename))).Distinct();
                }
                if (secondproprietairename != "" && thirdproprietairename == "")
                {
                    if (firstproprietairename != "")
                    {
                        ALLName = firstproprietairename + " " + secondproprietairename;
                        tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename) && y.prenom.StartsWith(firstproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(ALLName))).Distinct();
                    }
                    else
                    {
                        tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(secondproprietairename))).Distinct();
                    }
                }
                if (thirdproprietairename != "")
                {
                    ALLName = firstproprietairename + " " + secondproprietairename + " " + thirdproprietairename;
                    if (ALLName != "")
                    {
                        tfs = tfs.Where(x => x.proprietaire.Any(y => y.nome2.StartsWith(secondproprietairename) && y.prenom.StartsWith(firstproprietairename) && y.nom3.StartsWith(thirdproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(ALLName))).Distinct();

                    }
                    else
                    {
                        tfs = tfs.Where(x => x.proprietaire.Any(y => y.nom3.StartsWith(thirdproprietairename)) || x.proprietaire.Any(y => y.nomestablishment.StartsWith(thirdproprietairename))).Distinct();
                    }
                }





                if (typeliu != 3)
                {
                    tfs = tfs.Where(i => i.typelieu == typeliu);
                }
                if (typeoftf != 0)
                {
                    tfs = tfs.Where(i => i.typetf == typeoftf);
                }
                if (regID != 0)
                {
                    tfs = tfs.Where(i => i.quartier.Ville.region_ID == regID);
                    ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle).Where(i => i.region_ID == regID), "id", "libelle");
                }
                if (villeID != 0)
                {
                    tfs = tfs.Where(i => i.quartier.regionid == villeID);
                    ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle).Where(i=>i.idVille == villeID), "id", "libelle");
                }
                if (communeID != 0)
                {
                    tfs = tfs.Where(i => i.quartier.Arrondissement.idCommune == communeID);
                    ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle).Where(i=>i.idCommune == communeID), "id", "libelle");
                }
                if (arrondID != 0)
                {
                    tfs = tfs.Where(i => i.quartier.idArr == arrondID);
                    Quartier = Quartier.OrderBy(i => i.libelle).Where(i => i.idArr == arrondID).ToList();
                }
                if (quartierID != 0)
                {
                    tfs = tfs.Where(i => i.idquartier == quartierID);
                }
                if (valeur != "")
                {
                    tfs = tfs.Where(i => i.valeurterrain.StartsWith(valeur));
                }
                if (superfici != "")
                {
                    tfs = tfs.Where(i => i.superficie.StartsWith(superfici));
                }
                if (nomilot != "")
                {
                    tfs = tfs.Where(i => i.nomillot.StartsWith(nomilot));
                }
                if (numeroilot != "")
                {
                    tfs = tfs.Where(i => i.numerolot.StartsWith(numeroilot));
                }
                if (ilot != null)
                {
                    tfs = tfs.Where(i => i.estillot == ilot);
                }
                if (bati != null)
                {
                    tfs = tfs.Where(i => i.estbati == bati);
                }
                ViewBag.idquartier = new SelectList(Quartier.OrderBy(i => i.libelle), "id", "libelle");
            }
            else
            {
                tfs = tfs.Where(i => i.datetf == DateTime.Now);
            }
           
            ViewBag.NbreTFs = tfs.Count();
            tfs = tfs.OrderBy(s => s.numerotf);
            ViewBag.ListTFS = tfs;
            Session["ListTFS"] = tfs.ToList();
            tfs = tfs.Take(1000).ToList().Select(i => new tf { id = i.id, datetf = i.datetf, numerotf = i.numerotf, quartier = i.quartier, proprietaire = i.proprietaire }).AsQueryable();
            
            return View(tfs);
        }

        public string returnProprietaire(string f, string s, string t)
        {
            return ("");
        }
       
        public bool CheckFolderPathExist(string parentpath)
        {
            System.Web.HttpContext.Current.Session["parentpath"] = "";
            System.Web.HttpContext.Current.Session["parentpath"] = parentpath;
            string numerotf = getnumerotffromparent(parentpath);
            string destinpath = returnimagepath(parentpath,"1");
            //var path = Server.MapPath("~/Images/Domaine/" + parentpath);
            // var path = db.DriversMappeds.Where(x => x.IsValid == true && x.IsActive == true).FirstOrDefault().SharedPath + "\\" + parentpath;
           // var path = "ftp://ziad:heimp2856@192.168.1.36/";
            var path = "ftp://ziad:heimp2856@192.168.1.36/" + destinpath.Substring(35, destinpath.Length - 35).Replace(@"\", "/").Replace("à", "%E0").Replace(" ", "%20") + "/DJIB-TF-" + numerotf;

            //if (path != "" && path != null)
            //{
            //    var fullpath = imagepath + "\\" + filename;
            //    file.SaveAs(fullpath);
            //    message = "Les fichiers envoyé avec succès";
            //}
            //else
            //{
            //    message = "Erreur, les fichiers ne sont pas envoyé";
            //}
            var directory = new DirectoryInfo(path);
            if (directory.Exists == false)
            {
                System.Web.HttpContext.Current.Session["parentpath"] = path;
                directory.Create();
                return true;
            }
            else
            {
                return false;
            }
        //System.Web.HttpContext.Current.Session["parentpath"] = "";
        //System.Web.HttpContext.Current.Session["parentpath"] = parentpath;
        //string numerotffromparent = getnumerotffromparent(parentpath);
        //DriversMapped dm = db.DriversMappeds.Where(x => x.IsActive == true).FirstOrDefault();
        //string Sharedpath = dm.SharedPath;
        //string numerotf = getEqual(parentpath, 1);
        //string res = UploadImg.TrnsmitImages(parentpath);
        //UploadImg up = new UploadImg();
        //return res;

    }
        //public ActionResult UploadImages()
        //{
        //    try
        //    {


        //            try
        //            {
        //                string[] strDrives = Environment.GetLogicalDrives();
        //                ViewBag.logicallistDrives = strDrives;
        //                return View("UploadImages");
        //            }
        //            catch (Exception ex)
        //            {
        //                ViewBag.message = ex.Message;
        //                return View("Error");
        //            }

        //    }
        //    catch (Exception ex)
        //    {
        //        return RedirectToAction("logout", "utilisateurs");

        //    }
        //}
        //[HttpPost]
        //// [ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        //  //public ActionResult UploadImages(IEnumerable<HttpPostedFileBase> file, FormCollection data)
        //public ActionResult UploadImages(HttpPostedFileBase[] fileupload, string ParentFolder)
        //{   
        //    try
        //    {
        //        var server = db.DriversMappeds.Where(x => x.IsActive == true).FirstOrDefault();
        //        string ipadress = server.Domian;
        //        string username=server.username;
        //        string password = server.Password;
        //        string numerotf = Request["numerotf"];
        //        string sharedphysicpath = server.SharedPath;
        //        //  string targetpathiamge = returnimagepath(numerotf, "0");
        //        string targetpathiamge=getdestpath(numerotf);
        //       // string FTPPATH = getFTTpPath(targetpathiamge, sharedphysicpath);
        //        string FTPPATH = targetpathiamge.Replace(@"\","/");


        //            try
        //            {
        //                var fl = ParentFolder;
        //                foreach (HttpPostedFileBase file in fileupload)
        //                {
        //                    bool isfolderexist= CreateFTPDirectory("ftp://" + username+":"+ password+"@"+ ipadress + "/" + FTPPATH, username, password);

        //                    string fileName = System.IO.Path.GetFileName(file.FileName);
        //                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + ipadress + "/" + FTPPATH + "/" + fileName);
        //                    request.Credentials = new NetworkCredential(username, password);
        //                    request.Method = WebRequestMethods.Ftp.UploadFile;

        //                    var sourceStream = file.InputStream;
        //                    Stream requestStream = request.GetRequestStream();
        //                    request.ContentLength = sourceStream.Length;
        //                    int BUFFER_SIZE = 2048;
        //                    byte[] buffer = new byte[BUFFER_SIZE];
        //                    int bytesRead = sourceStream.Read(buffer, 0, BUFFER_SIZE);
        //                    do
        //                    {
        //                        requestStream.Write(buffer, 0, bytesRead);
        //                        bytesRead = sourceStream.Read(buffer, 0, BUFFER_SIZE);
        //                    } while (bytesRead > 0);
        //                    sourceStream.Close();
        //                    requestStream.Close();
        //                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
        //                }
        //                TempData["Message"] = "les fichiers téléchargés avec succès!";
        //                return RedirectToAction("index", "tfs");
        //            }
        //            catch (Exception ex)
        //            {
        //                ViewBag.Operationmessage = ex.Message;
        //                TempData["Message"] = ex.Message;

        //                return RedirectToAction("index", "tfs");

        //            }

        //    }
        //    catch (Exception ex)
        //    {
        //        return RedirectToAction("logout", "utilisateurs");

        //    }
        //}
        [FiltresAuthorisation(Roles = "Importations Images,Special")]

        public ActionResult UploadsImages()
        {
            return View();
        }

        //  [FiltresAuthorisation(Roles = "Importations,Special")]
       // [FiltresAuthorisation(Roles = "Importations Images,Special")]
        [HttpPost]
        

        public ActionResult UploadsImages(HttpPostedFileBase[] Images, FormCollection Form)
        {
             var typeImportation = Form["TypImport"];
            if(Images[0] == null)
            {
                TempData["message"] = "Aucune Image selectionné . Veuillez selectionné au moin une image!";
                TempData["status"] = "Erreur";
                return View();
            }
            //var fileName = Path.GetFileName(Importer.FileName);
            //var PathFile = Path.GetDirectoryName(Importer.FileName);
            var Numeros_Tf = Form["NumeroTf"];
            var DossierName = "DJIB-TF-" + Numeros_Tf;
            var Chemin =PhysicalPath+ getdestpath(Numeros_Tf).Replace(@"\", "/");
            var folder = new DirectoryInfo(Chemin);
            // For Repertoire Importation
            if (Chemin != PhysicalPath)
            {
                if (typeImportation == "Folders")
                {
                    if (folder.Exists)
                    {
                        TempData["message"] = "Ce Dossier Exist déjà !";
                        TempData["status"] = "Erreur";
                        return View();
                    }
                    else
                    {
                        Directory.CreateDirectory(Chemin + "/");
                        int NbrImageImporter = 0;
                        int NbrImageNonImporter = 0;
                        foreach (HttpPostedFileBase Image in Images)
                        {
                            var fileName = Chemin + "/" + Image.FileName;
                            var ImageExtension = Path.GetExtension(Image.FileName);
                            int ImageSize = Image.ContentLength;
                            if ((ImageExtension == ".jpg" || ImageExtension == ".png" || ImageExtension == ".jpeg") && ImageSize <= 200000)
                            {
                                Image.SaveAs(fileName);
                                NbrImageImporter += 1;
                            }
                            else
                            {
                                NbrImageNonImporter += 1;
                            }
                        }
                        if (NbrImageImporter > 0)
                        {
                            TempData["message"] = "Le dossier " + DossierName + " avec " + Images.Length + " Image(s) est importer avec Success!";
                            if (NbrImageNonImporter > 0)
                            {
                                TempData["message"] += " Mais " + NbrImageNonImporter + " element(s) selectionné(s) n'a pas eté importer .";
                            }
                            TempData["status"] = "Success";
                        }
                        else
                        {
                            TempData["message"] = "Aucune Image importer. Veuillez rectifier l'image selectionné!";
                            TempData["status"] = "Erreur";
                            DirectoryInfo Folder = new DirectoryInfo(Chemin + "/");
                            Folder.Delete();
                        }
                        return View();
                    }
                }
                // For Images Importations
                else
                {

                    if (folder.Exists)
                    {
                        var Files = Directory.GetFiles(Chemin);
                        int NbrImageImporter = 0;
                        int NbrImageNonImporter = 0;
                        foreach (HttpPostedFileBase Image in Images)
                        {
                            bool Exist = false;
                            var FilesChemin = Chemin + "\\" + Image.FileName;
                            var ImageExtension = Path.GetExtension(Image.FileName);
                            int ImageSize = Image.ContentLength;
                            if ((ImageExtension == ".jpg" || ImageExtension == ".png" || ImageExtension == ".jpeg") && ImageSize <= 200000)
                            {
                                for (int l = 0; l < Files.Length; l++)
                                {
                                    var filenum = Files[l].ToString();
                                    if (FilesChemin == filenum)
                                    {
                                        Exist = true;
                                    }
                                }
                                if (Exist == false)
                                {
                                    var fileName = Chemin + "/" + Image.FileName;
                                    Image.SaveAs(fileName);
                                    NbrImageImporter += 1;
                                }
                                else
                                {
                                    NbrImageNonImporter += 1;
                                }
                            }
                            else
                            {
                                NbrImageNonImporter += 1;
                            }
                        }
                        if (NbrImageImporter > 0)
                        {
                            TempData["message"] = NbrImageImporter + " Image(s) Importer avec success !";
                            if (NbrImageNonImporter > 0)
                            {
                                TempData["message"] += " Mais " + NbrImageNonImporter + " element(s) selectionné(s) n'a pas eté importer .";
                            }
                            TempData["status"] = "Success";
                        }
                        else
                        {
                            TempData["message"] = " Aucune Image Importer donc veuillez rectifier l'image selectionnée !";
                            TempData["status"] = "Erreur";
                        }
                        return View();
                    }
                    else
                    {
                        Directory.CreateDirectory(Chemin + "/");
                        int NbrImageImporter = 0;
                        int NbrImageNonImporter = 0;
                        foreach (HttpPostedFileBase Image in Images)
                        {
                            var fileName = Chemin + "/" + Image.FileName;
                            var ImageExtension = Path.GetExtension(Image.FileName);
                            int ImageSize = Image.ContentLength;
                            if ((ImageExtension == ".jpg" || ImageExtension == ".png" || ImageExtension == ".jpeg") && ImageSize <= 200000)
                            {
                                Image.SaveAs(fileName);
                                NbrImageImporter += 1;
                            }
                            else
                            {
                                NbrImageNonImporter += 1;
                            }
                        }
                        if (NbrImageImporter > 0)
                        {
                            TempData["message"] = "Le dossier " + DossierName + " avec " + NbrImageImporter + " Image(s) est importer avec Success!";
                            if(NbrImageNonImporter > 0)
                            {
                                TempData["message"] += " Mais "+NbrImageNonImporter+" element(s) selectionné(s) n'a pas eté importer .";
                            }

                            TempData["status"] = "Success";
                        }
                        else
                        {
                            TempData["message"] = "Aucune Image importer. Veuillez rectifier l'image selectionné!";
                            TempData["status"] = "Erreur";
                            DirectoryInfo Folder = new DirectoryInfo(Chemin + "/");
                            Folder.Delete();

                        }
                        return View();
                    }
                }
            }
            TempData["message"] = "Ce Tf n'a pas de chemin ou ce numero n'existe pas!";
            TempData["status"] = "Erreur ";
            return View();
        }

        private bool CreateFTPDirectory(string directory,string user,string password)
        {

            try
            {
                //create the directory
                //  FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(new Uri(directory));
                // FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://192.168.1.40/000 000 à 099 999/010 000 à 019 999/014 000 à 014 999/014 100 à 014 199/DJIB-TF-14129/"));
                FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(new Uri(directory));

                requestDir.Method = WebRequestMethods.Ftp.MakeDirectory;
                requestDir.Credentials = new NetworkCredential(user, password);
                requestDir.UsePassive = true;
                requestDir.UseBinary = true;
                requestDir.KeepAlive = false;
                FtpWebResponse response = (FtpWebResponse)requestDir.GetResponse();
                Stream ftpStream = response.GetResponseStream();

                ftpStream.Close();
                response.Close();

                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    response.Close();
                    return true;
                }
                else
                {
                    response.Close();
                    return false;
                }
            }
        }
        //public PartialViewResult imageviewer(string pathofimg1, int imgnvaindex, int typview, string numerotf)
        //{
        //    ftp f = new ftp();
        //    var server = db.DriversMappeds.Where(x => x.IsActive== true).FirstOrDefault();
        //    string ipadress = server.Domian;
        //    string username = server.username;
        //    string password = server.Password;
        //    string sharedpath = server.SharedPath;
        //    int substringsharedpathindex = localphysicpath.Length;
        //    //string fttppaFTPPATH = pathofimg1.Substring(substringsharedpathindex, pathofimg1.Length - substringsharedpathindex);

        //    string fttppaFTPPATH = getdestpath(numerotf);


        //    string[] simpleDirectoryListing = f.GetFileList("ftp://" + ipadress + "/" + fttppaFTPPATH, username, password);
        //    int countfile = 10;// simpleDirectoryListing.Length; 

        //ViewBag.filescount = countfile;
        //     ViewBag.pathofimg1 = "ftp://" + username + ":" + password + "@" + ipadress + "/" + fttppaFTPPATH.Replace(@"\", "/").Replace("à", "%E0").Replace(" ", "%20") + "/DJIB-TF-" + numerotf + "%2000" + imgnvaindex + ".jpg";


        //                ViewBag.typview = typview;
        //    ViewBag.realphysicpath = pathofimg1;
        //    ViewBag.numerotf = numerotf;

        //    return PartialView("imgViewer", new ViewDataDictionary { { "pathofimg1", "0" } });

        //}

        public string AfficheImage(int indeximg)
        {
            var imagestf = TempData["chemins"] as string[];
            TempData["chemins"] = TempData["chemins"];
            byte[] imageByteData = System.IO.File.ReadAllBytes(imagestf[indeximg].Replace(@"\", "/"));
            string imageBase64Data = Convert.ToBase64String(imageByteData);
            string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);

            return JsonConvert.SerializeObject((Object)imageDataURL);
        }
        [FiltresAuthorisation(Roles = "Consultation Images,Special")]
        public PartialViewResult imageviewer(string pathofimg1, int imgnvaindex, int typview, string numerotf)
        {
            string[] imagestf;
            var p = getdestpath(numerotf);
            string ErrorPath = Server.MapPath(Url.Content("~/Images/ERRORS/"));
            string chemincomplet = /*"C:/Domain/" */PhysicalPath + p.Replace(@"\", "/");
            var folder = new DirectoryInfo(chemincomplet);
            if (folder.Exists)
            {
                if (Directory.GetFiles(chemincomplet).Length == 0 && Directory.GetDirectories(chemincomplet).Length == 0)
                {
                    imagestf = Directory.GetFiles(ErrorPath);
                }
                else
                { imagestf = Directory.GetFiles(chemincomplet); }
            }
            else
            {
                imagestf = Directory.GetFiles(ErrorPath);
            }

                byte[] imageByteData2 = System.IO.File.ReadAllBytes(imagestf[0].Replace(@"\", "/"));
                string imageBase64Data2 = Convert.ToBase64String(imageByteData2);
                string imageDataURL2 = string.Format("data:image/jpg;base64,{0}", imageBase64Data2);

                ViewBag.ImageData = imageDataURL2;
                ViewBag.indeximg = 0;
                ViewBag.nbrimages = imagestf.Length - 1;
                TempData["chemins"] = imagestf;
           
            return PartialView("imgViewer");
        }
        public string returnimagepath(string imagepathcritera, string imageindex)
        {
            string pathtoreturn = "";
            try
            {
                DriversMapped dm = db.DriversMapped.Where(x => x.IsActive == true).FirstOrDefault();
                string sharedpath = this.localphysicpath;
                pathtoreturn = mapDrivepathfind.finddestinationimage(imagepathcritera,sharedpath);
                //pathtoreturn = pathtoreturn + "\\DJIB-TF-"+imagepathcritera+ "\\DJIB-TF-" + imagepathcritera+" 00"+ imageindex+".jpg";
                pathtoreturn = pathtoreturn + "\\DJIB-TF-" + imagepathcritera;

                return pathtoreturn;
            }
            catch (Exception ex)
            {
                return pathtoreturn;

            }
            return "";
        }

        public string  getdestpath(string numerotf)
        {
            //14120
            string fullpath = "";
            int numeortflength = numerotf.Length;
            if (numeortflength < 3)
            {
                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "000 000 à 000 999";//014 000 à 014 999
                string fourththpath = "000 000 à 000 099";//014 100 à 014 199
                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;

            }
            if (numeortflength == 3)
            {
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "000 000 à 000 999";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199
             

                ////Fourth
               var aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, firstleterofnumerof);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, firstleterofnumerof);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;
            }
            else if(numeortflength==4)
            {
                //000 000 à 099 999\000 000 à 009 999
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);
                string fourthletterofnumerotf = numerotf.Substring(3, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199
                ////third name
              var  aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(2, 1);
                aStringBuilder.Insert(2, firstleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(12, 1);
                aStringBuilder.Insert(12, firstleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                ////Fourth
                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, Secondleterofnumerof);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, Secondleterofnumerof);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;

            }
            else if (numeortflength == 5)
            {
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);
                string fourthletterofnumerotf = numerotf.Substring(3, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "";//  010 000 à 019 999
                string thirdpathpath = "";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199



                //Second name
                var aStringBuilder = new StringBuilder(firstpublicpath);
                aStringBuilder.Remove(1, 1);
                aStringBuilder.Insert(1, firstleterofnumerof);
                Secondpublicpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(11, 1);
                aStringBuilder.Insert(11, firstleterofnumerof);
                Secondpublicpath = aStringBuilder.ToString();

                ////third name
                aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(2, 1);
                aStringBuilder.Insert(2, Secondleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(12, 1);
                aStringBuilder.Insert(12, Secondleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                ////Fourth
                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, thirdletterofnumerotf);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, thirdletterofnumerotf);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;
            }

            return fullpath;
            //"010 000 à 019 999"
        }

        public ActionResult Details(string returnUrl, int? id)
        {
            if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null)
                returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
            ViewData["RetourneURL"] = returnUrl;

            string[] strDrives = Environment.GetLogicalDrives();
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tf tf = db.tf.Find(id);
                if (tf == null)
                {
                    return HttpNotFound();
                }
                var listhyp = db.Operations.Where(i => i.idTF == tf.id && i.idNatureOperation==2 && i.IDStatus == 3).ToList();
                if (listhyp.Count > 0)
                {
                    ViewBag.Yes = 1;
                }
                else ViewBag.Yes = 0;
                var results = tf.proprietaire.
                    Select(x => new
                    {
                        id = x.id,
                        prenom = x.prenom,
                        nome2 = x.nome2,
                        nom3 = x.nom3,
                        prenommere = x.prenommere,
                        nommere2 = x.nommere2,
                        nommere3 = x.nommere3,
                        lieuNaisance = x.lieuNaisance,
                        dateNaissace = x.dateNaissace,
                        numeropiece = x.numeropiece,
                        piecedateemission = x.piecedateemission,
                        typepiece = x.typepiece,
                        // nationalite = x.nationalite,
                        nomestablishment = x.nomestablishment,
                        typestablishment = x.typestablishment,
                        nomeresponsable1 = x.nomeresponsable1,
                        secturactivite = x.secturactivite,
                        typ = x.typeproprietaire
                        // Checked = db.books.Where(f=>f.id== bid && f.authors == author).Any()  db.books.Any(u=>u.authors==author)
                    }).ToList();
                 var MontantRestantTF = OpController.VerifieMontantRestant(tf.id);
                ViewBag.MontantRestantTF = MontantRestantTF;
                var MyViewModel = new TFSViewModel();
                MyViewModel.id = id.Value;
                MyViewModel.numerotf = tf.numerotf;
                MyViewModel.Actif = tf.Actif;
               
                MyViewModel.datetf = Convert.ToDateTime(tf.datetf);
                MyViewModel.numacp = tf.numacp;
                MyViewModel.valeurterrain = tf.valeurterrain;
                MyViewModel.superficie = tf.superficie;
                MyViewModel.DateOperation = tf.DateOperation;
                MyViewModel.typetf1 = (db.typetf.Where(x => x.id == tf.typetf).FirstOrDefault());
                MyViewModel.quartier = (db.quartier.Where(x => x.id == tf.idquartier).FirstOrDefault());
            MyViewModel.Unites_Surface = (db.Unites_Surface.Where(x => x.ID == tf.ID_Unites).FirstOrDefault());
            MyViewModel.piecejustificative = (db.piecejustificative.Where(x => x.id == tf.PIECEJUSTIFICATIVE_id).FirstOrDefault());
               
                MyViewModel.estbati = tf.estbati;
                MyViewModel.estillot = tf.estillot;
                MyViewModel.typelieu = tf.typelieu;
                MyViewModel.numerolot = tf.numerolot;
                MyViewModel.nomillot = tf.nomillot;
                MyViewModel.typelieu = tf.typelieu;
                MyViewModel.cheminimage = tf.cheminimage;
                MyViewModel.typetf = tf.typetf;


                ViewBag.PIECEJUSTIFICATIVE_id = new SelectList(db.piecejustificative, "id", "libelle");
                ViewBag.typetf = new SelectList(db.typetf, "id", "libell");
                ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                ViewBag.logicallistDrives = strDrives;
                ViewBag.typelieu = new SelectList(db.typelieu, "id", "libelle");
                ViewBag.RegId = new SelectList(db.region, "id", "libelle");
              //  int regid = Convert.ToInt32(tf.quartier.Ville.id);
               // ViewBag.unitees = new SelectList(db.Unites_Surface, "ID", "Libelle");

              //  bool typliu = Convert.ToBoolean(tf.quartier.estlotissement);
                ViewBag.idquartier = new SelectList(db.quartier, "id", "libelle");
                var propertairesviewmodel = new List<ProprietaresViewModel>();
                foreach (var item in results)
                {
                    propertairesviewmodel.Add(new ProprietaresViewModel
                    {
                        id = item.id,
                        prenom = item.prenom,
                        nome2 = item.nome2,
                        nom3 = item.nom3,
                        prenommere = item.prenommere,
                        nommere2 = item.nommere2,
                        nommere3 = item.nommere3,
                        lieuNaisance = item.lieuNaisance,
                        // dateNaissace = item.dateNaissace,
                        numeropiece = item.numeropiece,
                        // piecedateemission = item.piecedateemission,
                        typepiece = item.typepiece,
                        //  nationalite = item.nationalite,
                        nomestablishment = item.nomestablishment,
                        typestablishment = item.typestablishment,
                        nomeresponsable1 = item.nomeresponsable1,
                        secturactivite = item.secturactivite,
                        typeproprietaire = item.typ
                    });
                }
                MyViewModel.proprietaires = propertairesviewmodel;
            
            var ExistPromess = db.Operations.Any(i => i.idTypeOperation == 15 && i.idTF == MyViewModel.id && (i.IDStatus == 2 || i.IDStatus == 3));
            if(ExistPromess == true)
            {
                ViewBag.Promesse = 1;
            }
            else ViewBag.Promesse = 0;

            historique.Tracers("Consultation","Consultation TF","TF",""+MyViewModel.id,OpController.CurrentUsers(User.Identity.Name), null, tf, null);


            return View("DetailsTFS", MyViewModel);
           
        }
        public bool verfyNumeroTf(string recivednumero)
    
        {
            var NTF = Convert.ToInt32(recivednumero);
            bool b = db.tf.Any(x => x.numerotf == NTF && (x.Actif == true || x.Actif==null));
            return b;
        }
        // GET: tfs/Create
        [FiltresAuthorisation(Roles = "Creer TF,Special")]
        public ActionResult Create()
        {
            try
            {

                
                    try
                    {

                        string[] strDrives = Environment.GetLogicalDrives();
                        ViewBag.PIECEJUSTIFICATIVE_id = new SelectList(db.piecejustificative, "id", "libelle");
                        ViewBag.typetf = new SelectList(db.typetf, "id", "libell");
                        ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                        ViewBag.logicallistDrives = strDrives;
                        ViewBag.typelieu = new SelectList(db.typelieu, "id", "libelle");
                        ViewBag.Regid = new SelectList(db.region, "id", "libelle");
                        ViewBag.VilleID = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle");
                        ViewBag.ID_Unites = new SelectList(db.Unites_Surface.OrderBy(i => i.Libelle), "ID", "Libelle");




                    return View("CreateNewTf");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        [FiltresAuthorisation(Roles = "Creer TF,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(TFSViewModel tfcreated)
        {
            try
            {

                var b = tfcreated.datetf;
                

                var userid = db.utilisateur.Where(i => i.login == User.Identity.Name).Select(i => i.id).FirstOrDefault();
                    var username = User.Identity.Name;
                    try
                    {

                        tf tfnew = new tf();
                    //if (ModelState.IsValid)
                    //{

                    tfnew.EstPublic = true;
                    var type = db.typetf.Find(tfnew.typetf);
                    if (db.tf.Any(x => x.numerotf == tfcreated.numerotf))
                        {
                            ModelState.IsValid.Equals(false);
                        }
                        {

                        }
                        tfnew.numerotf = tfcreated.numerotf;
                        // if (tfcreated.datetf!= Convert.ToDateTime("1/1/ 0001 12:00:00 AM"))
                        if (tfcreated.datetf != null)
                        //{ tfnew.datetf = Convert.ToDateTime(tfcreated.datetf); }
                        { tfnew.datetf = tfcreated.datetf; }
                       
                        //immatricule
                        tfnew.typetf = tfcreated.typetf;
                        tfnew.estbati = tfcreated.estbati;
                        tfnew.numacp = tfcreated.numacp;
                        if (tfcreated.superficie != null && tfcreated.superficie != "")
                        {
                            tfnew.superficie = tfcreated.superficie.ToString();
                        }
                        tfnew.ID_Unites = tfcreated.ID_Unites.Value;
                    tfnew.valeurterrain = tfcreated.valeurterrain;
                        tfnew.PIECEJUSTIFICATIVE_id = tfcreated.PIECEJUSTIFICATIVE_id.Value;
                        if (tfcreated.DateOperation != null)
                        { tfnew.DateOperation = Convert.ToDateTime(tfcreated.DateOperation); }
                        //tfnew.cheminimage = tfcreated.cheminimage;
                        string cheminimage= getrealphysicpath(db.DriversMapped.Where(x => x.IsActive == true).FirstOrDefault().SharedPath, Convert.ToString(tfcreated.numerotf), 1);

                        tfnew.cheminimage = getimagepath(cheminimage);

                        //location
                        tfnew.idquartier = tfcreated.idquartier.Value;
                        if (db.quartier.Any(x => x.estlotissement == true && x.id == tfcreated.idquartier))
                        {
                            tfnew.typelieu = 2;
                        }
                        else
                        {
                            tfnew.typelieu = 1;
                        }

                        tfnew.estillot = tfcreated.estillot;
                        tfnew.nomillot = tfcreated.nomillot;
                        tfnew.numerolot = tfcreated.numerolot;
                        tfnew.usercreated = userid;

                        proprietaire p = null;
                        foreach (var item in tfcreated.proprietaires)
                        {
                            p = new proprietaire();
                            p = db.proprietaire.Find(item.id);
                            tfnew.proprietaire.Add(p);
                        }

                    //using (DJTFEntities db = new DJTFEntities())
                    //{
                    //    DbContextTransaction transaction = db.Database.BeginTransaction();

                    //    try
                    //    {
                    tfnew.dateCreated = DateTime.Now;
                    tfnew.Actif = true;
                    tfnew.Coopropriete = false;

                            db.tf.Add(tfnew);
                            db.SaveChanges();

                           
                    Historiques H = new Historiques();
                    H.ActionName = "Creation";
                    H.TypesOperations = "Nouveau TF";
                    H.Operations = "TF";
                    H.DateActions = DateTime.Now;
                    H.Descriptions = "TF N°"+tfnew.numerotf;
                    H.UserActions = userid;
                    H.IDTF = tfnew.id;
                  //  H.utilisateur = OpController.CurrentUsers(User.Identity.Name);
                   // H.tfs.Add(tfnew);
                    db.Historiques.Add(H);
                   // db.SaveChanges();

                    //Saisie
                    saisie sse = new saisie();
                            sse.datesaisie1 = DateTime.Now.Date;
                            //  sse.timesaisie = DateTime.Now.TimeOfDay;
                            sse.utilistaurid = userid;
                            sse.typesaisie = 1;

                            db.saisie.Add(sse);
                            db.SaveChanges();
                     //historique.TracersTF("Creation", "TF",type.libell,tfnew.numacp , OpController.CurrentUsers(User.Identity.Name),tfnew);

                    //    transaction.Commit();

                    //}
                    //catch
                    //{
                    //    transaction.Rollback();
                    //}
                    //}
                    TempData["Message"] = "Enregistré avec succès!";
                    return RedirectToAction("Index");

                }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }


        }

        
        [FiltresAuthorisation(Roles = "Modifier TF,Special")]
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TFSViewModel tfedited)
        {
            
                    try
                    {
                        var TfToEdit = db.tf.Find(tfedited.id);
                //backuptfbeforUpdate(TfToEdit);
                var userid = db.utilisateur.Where(i => i.login == User.Identity.Name).Select(i => i.id).FirstOrDefault();
                var username = User.Identity.Name;

                // TfToEdit.numerotf = tfedited.numerotf;
                TfToEdit.cheminimage = tfedited.cheminimage;
                        string cheminimage = getrealphysicpath(db.DriversMapped.Where(x => x.IsActive == true).FirstOrDefault().SharedPath,Convert.ToString(TfToEdit.numerotf), 1);

                        TfToEdit.cheminimage = getimagepath(cheminimage);

                        if (tfedited.datetf != null)
                        { TfToEdit.datetf = Convert.ToDateTime(tfedited.datetf); }
                        TfToEdit.numacp = tfedited.numacp;
                        TfToEdit.valeurterrain = tfedited.valeurterrain;

                        if (tfedited.superficie != null && tfedited.superficie != "")
                        {
                            TfToEdit.superficie = tfedited.superficie.ToString();
                        }


                        if (tfedited.DateOperation != null)
                        {
                            TfToEdit.DateOperation = Convert.ToDateTime(tfedited.DateOperation);
                            //DateTime dt = DateTime.ParseExact(tfedited.datetf, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                        }

                        TfToEdit.typetf = (tfedited.typetf);
                        TfToEdit.idquartier = (tfedited.idquartier);
                        TfToEdit.ID_Unites = Convert.ToInt32(tfedited.ID_Unites);
                TfToEdit.PIECEJUSTIFICATIVE_id = (tfedited.PIECEJUSTIFICATIVE_id);
                        // MyViewModel.quartier.region = (db.quartiers.Where(x => x.id == tf.idquartier.).FirstOrDefault());
                        // MyViewModel.quartier = (db.quartiers.Where(x => x.id == tf.idquartier).FirstOrDefault());
                        TfToEdit.estbati = tfedited.estbati;
                        TfToEdit.estillot = tfedited.estillot;
                        TfToEdit.typelieu = tfedited.typelieu;
                        TfToEdit.numerolot = tfedited.numerolot;
                        TfToEdit.nomillot = tfedited.nomillot;
                        if (db.quartier.Any(x => x.estlotissement == true && x.id == tfedited.idquartier))
                        {
                            TfToEdit.typelieu = 2;
                        }
                        else
                        {
                            TfToEdit.typelieu = 1;

                        }


                        TfToEdit.proprietaire.Clear();
                        proprietaire p = null;
                        foreach (var item in tfedited.proprietaires)
                        {
                            p = new proprietaire();
                            p = db.proprietaire.Find(item.id);
                            TfToEdit.proprietaire.Add(p);
                        }

                        TfToEdit.usermodified = userid;
                        TfToEdit.dateModified = DateTime.Now;
                        db.Entry(TfToEdit).State = EntityState.Modified;
                db.Entry(TfToEdit).Property(o => o.numerotf).IsModified = false;
                db.Entry(TfToEdit).Property(o => o.Actif).IsModified = false;
                db.Entry(TfToEdit).Property(o => o.Coopropriete).IsModified = false;
                db.Entry(TfToEdit).Property(o => o.usercreated).IsModified = false;
                db.Entry(TfToEdit).Property(o => o.dateCreated).IsModified = false;
                db.SaveChanges();
                Historiques H = new Historiques();
                H.ActionName = "Modification";
                H.TypesOperations = "Modification TF";
                H.Operations = "TF";
                H.DateActions = DateTime.Now;
                H.Descriptions = "TF N°" + TfToEdit.numerotf;
                H.UserActions = userid;
                H.IDTF = TfToEdit.id;
                db.Historiques.Add(H);
                //Saisie
                saisie sse = new saisie();
                        sse.datesaisie1 = DateTime.Now.Date;
                        //    sse.timesaisie = DateTime.Now.TimeOfDay;
                        sse.utilistaurid = userid;
                        sse.typesaisie = 2;
                        db.saisie.Add(sse);
                        db.SaveChanges();
                        TempData["Message"] = "Enregistré avec succès!";
                        return RedirectToAction("Index");

                       
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }

                return View();
            }
        [FiltresAuthorisation(Roles = "Annuler TF,Special")]
        public ActionResult Delete(int? id)
        {
           
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                tf tf = db.tf.Find(id);
                if (tf == null)
                {
                    return HttpNotFound();
                }
                return View(tf);
           
        }
        // POST: tfs/Delete/5
        [FiltresAuthorisation(Roles = "Annuler TF,Special")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           
                tf tf = db.tf.Find(id);
                db.tf.Remove(tf);
                db.SaveChanges();
                return RedirectToAction("Index");
           
        }
        public ActionResult DirectDelete(int id)
        {
           
                tf tf = db.tf.Find(id);
                tf.proprietaire.Clear();
                db.Entry(tf).State = EntityState.Modified;
                db.tf.Remove(tf);
                db.SaveChanges();
                return RedirectToAction("Index", new { searchBy = "", search = "" });
           
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult gettingAllSubDirectories(string parentpath)
        {
            if (parentpath != null)
            {
                if (Directory.GetDirectories(parentpath).Length != 0)
                {
                    string[] lstsubDirectories = Directory.GetDirectories(parentpath);
                    //   if (lstsubDirectories.Length != 0)// if the parent Folder has SubFolders
                    {
                        List<img> lst = new List<img>();
                        img im = null;
                        string DirectoryNameToDisplay = "";
                        for (int j = 0; j < lstsubDirectories.Length; j++)
                        {
                            string DirectoryName = "";
                            im = new img();

                            string nm = lstsubDirectories[j];
                            for (int i = nm.Length - 1; i > 0; i--)
                            {

                                if (nm[i].ToString() == "\\")
                                {
                                    break;
                                }
                                DirectoryName = DirectoryName + nm[i];
                            }
                            char[] newstring = DirectoryName.ToCharArray();
                            Array.Reverse(newstring);
                            DirectoryNameToDisplay = new string(newstring);
                            im.pathofimg = lstsubDirectories[j];
                            im.Filename = DirectoryNameToDisplay;
                            lst.Add(im);
                        }

                        SelectList selectsubDirectories = new SelectList(lst, "pathofimg", "Filename", 0);
                        return Json(selectsubDirectories);
                    }
                }
                else
                {
                    int j = 0;
                    return Json(j);
                }
            }
            else
            {
                int j = 0;
                return Json(j);
            }
        }

        public ActionResult VerifiePathExistOfTF(string numerotf)
        {
            var Chemin = PhysicalPath + getdestpath(numerotf).Replace(@"\", "/");
            if (Chemin != PhysicalPath)
            {
                var Folder = new DirectoryInfo(Chemin);

                if (Folder.Exists)
                {
                    return Json(new { success = true , status = "Exist"}, JsonRequestBehavior.AllowGet);
                }
                else return Json(new { success = false , status = "Inexistant" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, status = "Numeros Inexistant" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult VerifieExtensionSize(HttpPostedFileBase[] Images)
        {
            //var Chemin = PhysicalPath + getdestpath(numerotf).Replace(@"\", "/");
            int NbrImageFauxExtension = 0;
            int NbrImageFauxTaille = 0;
            int NbrImageValider = 0;
            foreach (HttpPostedFileBase Image in Images)
            {
                var fileName =  Image.FileName;
                var ImageExtension = Path.GetExtension(fileName);
                int ImageSize = Image.ContentLength;
                if ((ImageExtension == ".jpg" || ImageExtension == ".png" || ImageExtension == ".jpeg") && ImageSize <= 200000)
                {
                   // Image.SaveAs(fileName);
                    NbrImageValider += 1;
                }
                else if(ImageSize > 200000)
                {
                    NbrImageFauxTaille += 1;
                }
                else if(ImageExtension != ".jpg" && ImageExtension != ".png" && ImageExtension != ".jpeg")
                {
                    NbrImageFauxExtension += 1;
                }
            }
            var status = "";
            if(NbrImageFauxExtension > 0)
            {
                return Json(new { success = true, status = "Extension" }, JsonRequestBehavior.AllowGet);
            }
            else if (NbrImageFauxTaille > 0)
            {
                return Json(new { success = true, status = "Taille" }, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                return Json(new { success = true, status = "Valider" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetListImagesFromTF(string numerotf)
        {
            var numTF = Convert.ToInt32(numerotf);
            var Chemin = PhysicalPath + getdestpath(numerotf).Replace(@"\", "/");
            if (Chemin != PhysicalPath)
            {
                var Folder = new DirectoryInfo(Chemin);

                if (Folder.Exists)
                {
                    var Images = Folder.GetFiles();
                    List<string> Filnames = new List<string>();
                    for(int i=0;i<Images.Length;i++)
                    {
                        var Name = Images[i].Name;
                        Filnames.Add(Name);
                    }
                    return Json(new { success = true, status = Filnames.ToList() }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult Image(string pathofimg1, string imgnvaindex)
        {
            try {
                DriversMapped dm = db.DriversMapped.Where(x=>x.IsActive==true).FirstOrDefault();
            using (new NetworkConnection(dm.SharedPath, new NetworkCredential(dm.username, dm.Password)))
            {
                    try
                    {

                        // File.Copy("", "");
                        int imgnvaindexint = Convert.ToInt32(imgnvaindex);
                        //   string sahredfolder = db.DriversMappeds.Where(x => x.IsValid == true && x.IsActive == true).FirstOrDefault().SharedPath;
                        //int numerotf = Convert.ToInt32(GetnumerotfFromFolderName(pathofimg1,1));
                        string sahredfolder = dm.SharedPath;
                        string realparentpath = getrealphysicpath(sahredfolder, pathofimg1,1);
                        //System.IO.Directory myDir = GetMyDirectoryForTheExample();
                        int count = Directory.GetFiles(realparentpath).Length;
                        string[] arr4 = new string[count];
                        //string upDir = Directory.GetParent(path).ToString();
                        // string[] arr4 = new string[8];
                        arr4 = Directory.GetFiles(realparentpath);
                        ViewBag.filecount = count;
                        System.Web.HttpContext.Current.Session["filecount"] = count;
                        System.Web.HttpContext.Current.Session["ImageToDisplay"] = File(arr4[imgnvaindexint], "image/jpg");
                        //System.Web.HttpContext.Current.Session["ImageToDisplay"] = File(pathofimg1, "image/jpg");
                        return (System.Web.HttpContext.Current.Session["ImageToDisplay"] as FilePathResult);
                    }
                    catch (Exception ex)
                    {
                        System.Web.HttpContext.Current.Session["ImageToDisplay"] = File(@"D:\imagenotAvilable.jpg", "image/jpg");
                        return (System.Web.HttpContext.Current.Session["ImageToDisplay"] as FilePathResult);

                    }
                }
            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Session["ImageToDisplay"] = File(@"D:\imagenotAvilable.jpg", "image/jpg");
                return (System.Web.HttpContext.Current.Session["ImageToDisplay"] as FilePathResult);

            }


        }

        public  string getrealphysicpath(string imp, string tfnumerof, int pathtype)
        {

            DriversMapped dm = db.DriversMapped.Where(x => x.IsActive == true).FirstOrDefault();
            string Sharedpath = dm.SharedPath;
            try
            {
                //using (new NetworkConnection(dm.SharedPath, new NetworkCredential(dm.username, dm.Password)))
                //{

                    string Numerotf = tfnumerof;
                    int targetnumerotf = Convert.ToInt32(tfnumerof);
                    string completepathtoReturn = "";
                    bool endllop;
                    int ptyp = pathtype;
                    //   if (endllop) { return null; }

                    // string sharedpath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
                    //   string NumeroTFFoldePath = db.DriversMappeds.Where(x => x.IsActive == true && x.IsValid == true).FirstOrDefault().SharedPath;
                    if (Directory.GetDirectories(imp).Length > 0)
                    {
                        string minnumber;
                        string maxnumber;
                        string equalNumber;
                        int min;
                        int max;
                        int equal;
                        for (int i = 0; i < Directory.GetDirectories(imp).Length; i++)
                        {
                            string FolderName = Directory.GetDirectories(imp)[i];
                            if (Directory.GetDirectories(FolderName).Length > 0)
                            {
                                minnumber = getMinLimit(FolderName);
                                maxnumber = getMaxLimit(FolderName);
                                min = Convert.ToInt32(minnumber);
                                max = Convert.ToInt32(maxnumber);

                                if (targetnumerotf >= min && targetnumerotf <= max)
                                {
                                    return getrealphysicpath(FolderName, Numerotf, ptyp);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            else if (Directory.GetDirectories(FolderName).Length == 0)
                            {

                                equalNumber = getEqual(FolderName, ptyp);
                                if (ptyp == 1)
                                {
                                    equal = Convert.ToInt32(equalNumber);
                                    if (equal == targetnumerotf)
                                    {
                                        completepathtoReturn = FolderName;
                                        break;
                                        // return (FolderName);
                                    }
                                }
                                else
                                {
                                    minnumber = getMinLimit(FolderName);
                                    maxnumber = getMaxLimit(FolderName);
                                    min = Convert.ToInt32(minnumber);
                                    max = Convert.ToInt32(maxnumber);
                                    if (targetnumerotf >= min && targetnumerotf <= max)
                                    {
                                        //return getrealphysicpath(FolderName, Numerotf, ptyp);
                                        completepathtoReturn = FolderName;
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                            }

                            else // Exception Some Thing Wrong
                            {
                                return "0";
                                //completepathtoReturn = "0";
                                //break;
                            }
                        }
                    }
                    else
                    {  // Exception Some Thing Wrong
                        return "0";
                        // completepathtoReturn = "0";
                    }

                    return completepathtoReturn;
               // }
            }
            catch (Exception ex)
            {
                return "";
            }
        }


        public static string getMinLimit(string Fname)
        {

            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Fname[i].ToString() == "\\")
                {
                    break;
                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            string rawstring = numtf.Replace(" ", string.Empty).Replace("  ", "").Replace("   ", "").Replace("    ", "").Replace("a", "à").Replace("A", "à").Replace("À", "à");
            var pos = rawstring.IndexOf('à');
            string valueToReturn = rawstring.Substring(0, pos);

            return valueToReturn;
        }
        public static string getMaxLimit(string Fname)
        {
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Fname[i].ToString() == "\\")
                {
                    break;
                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            string rawstring = numtf.Replace(" ", string.Empty).Replace("  ", "").Replace("   ", "").Replace("    ", "").Replace("a", "à").Replace("A", "à").Replace("À", "à");
            var pos = rawstring.IndexOf('à');
            string valueToReturn = rawstring.Substring(pos + 1, ((rawstring.Length) - (pos + 1)));

            return valueToReturn;
        }
        public static string getEqual(string Fname, int Ptyp)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {
                if (Ptyp == 1)
                {
                    if (Fname[i].ToString() == "-")
                    {
                        break;
                    }
                }
                else
                {
                    if (Fname[i].ToString() == "\\")
                    {
                        break;
                    }

                }
                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            // intnumerotf = Convert.ToInt32(numtf);
            return numtf;
        }

        public static string getimagepath(string Fname)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {

                if (Fname[i].ToString() == "n")
                {
                    break;
                }

                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            // intnumerotf = Convert.ToInt32(numtf);
            return numtf;
        }
        public static string getnumerotffromparent(string Fname)
        {

            int intnumerotf;
            string numerotfToDisplay = "";
            for (int i = Fname.Length - 1; i > 0; i--)
            {

                if (Fname[i].ToString() == "-")
                {
                    break;
                }

                numerotfToDisplay = numerotfToDisplay + Fname[i];

            }
            char[] newstring = numerotfToDisplay.ToCharArray();
            Array.Reverse(newstring);
            string numtf = new string(newstring);
            // intnumerotf = Convert.ToInt32(numtf);
            return numtf;
        }

        public static string getFTTpPath(string Fname,string sharedpath)
        {

            int substringsharedpathindex = sharedpath.Length +1;
            string fttppaFTPPATH = Fname.Substring(substringsharedpathindex, Fname.Length - substringsharedpathindex);



            //string numerotfToDisplay = "";
            //for (int i = 0;i< Fname.Length - 1; i++)
            //{

            //    if (Fname[i].ToString() == "n")
            //    {
            //        break;
            //    }

            //    numerotfToDisplay = numerotfToDisplay + Fname[i];

            //}
            //char[] newstring = numerotfToDisplay.ToCharArray();
            //Array.Reverse(newstring);
            // string numtf = new string(newstring);
            // intnumerotf = Convert.ToInt32(numtf);

            return fttppaFTPPATH;
        }


        public ActionResult AnnulerTF(int id)
        {
            if(id == null)
            {

            }
            var users =OpController.CurrentUsers(User.Identity.Name);
            var tf = db.tf.Find(id);
            tf.Actif = false;
            tf.dateModified = DateTime.Now;
            tf.usermodified = users.id;
            db.Entry(tf).State = EntityState.Modified;
            db.Entry(tf).Property(o => o.Actif).IsModified = true;
            db.Entry(tf).Property(o => o.dateModified).IsModified = true;
            db.Entry(tf).Property(o => o.usermodified).IsModified = true;
            db.SaveChanges();
            return Json(new { success = true });
           // return RedirectToAction("Details", new { id = id });
        }

        [ChildActionOnly]
        public ActionResult List_Coopropriete(int id)
        {
            ViewBag.ID_TF = id;
            var Cooproprietes = db.TF_Cooproprietes.Where(i => i.ID_TF == id);
           
            return PartialView("List_Coopropriete", Cooproprietes.ToList());
        }

        public ActionResult Add_Coopropriete(int? id)
        {
            TF_Cooproprietes TFCOOP = new TF_Cooproprietes();
            TFCOOP.ID_TF = Convert.ToInt32(id);
            ViewBag.ID_unites= new SelectList(db.Unites_Surface, "ID", "Libelle");
            return PartialView("Add_Coopropriete", TFCOOP);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add_Coopropriete(TF_Cooproprietes TF_COOP, FormCollection collections)
        {
            var TF = db.tf.Find(TF_COOP.ID_TF);

            var Unite =Convert.ToInt32(collections["ID_unites"]);
            TF_COOP.ID_Unites = Unite;
            db.TF_Cooproprietes.Add(TF_COOP);
            TF.Coopropriete = true;
            db.Entry(TF).State = EntityState.Modified;
            db.Entry(TF).Property(x => x.Coopropriete).IsModified = true;
            db.SaveChanges();

            string url = Url.Action("List_Coopropriete", "tfs", new { id = TF_COOP.ID_TF });
            return Json(new { success = true, url = url });
        }
        public static string EnleveSpace(string ElementaCorriger)
        {
            var ElementCorrigee = "";
            if (ElementaCorriger != "" && ElementaCorriger != " " && ElementaCorriger != null && ElementaCorriger != "  " && ElementaCorriger != "   " && ElementaCorriger != "    ")
            {
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options);
                ElementCorrigee = regex.Replace(ElementaCorriger, " ");
            }
            else ElementCorrigee = null;
            return ElementCorrigee;
        }
    }
     
}