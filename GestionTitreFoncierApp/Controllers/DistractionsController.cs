﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
    public class DistractionsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController OperationController = new OperationsController();
       
      
        public ActionResult List(int id , int idd)
        {
            ViewBag.ID_TF = id;
            var ListDistractions = db.Tfs_Distraction.Where(a => a.ID_TF == id && a.Actif == false);
            if (idd != 0)
            {
                ListDistractions = ListDistractions.Where(k => k.ID_Operation == idd);
            }
            else
            {
                ListDistractions = ListDistractions.Where(k => k.ID_Operation == null);
            }
            ViewBag.ListDistractions = ListDistractions.Count();
            return PartialView("_List", ListDistractions.ToList());
        }
        public ActionResult Index(int id, int idd)
        {
            ViewBag.ID_TF = id;
            var ListDistractions = db.Tfs_Distraction.Where(a => a.ID_TF == id && a.ID_Operation == idd);
            ViewBag.ListDistractions = ListDistractions.Count();
            return PartialView("_Index", ListDistractions.ToList());
        }
        public ActionResult Create(int? id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** ID TF :" + id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tf tf = db.tf.Find(id);
            Operations Op = new Operations();
            Op.tf = tf;
            Op.idTF = tf.id;
            Op.idTypeOperation = 13;
            Op.idNatureOperation = 4;
            ViewBag.IDTF = tf.id;
            ViewBag.sup = tf.superficie;
            var val = Convert.ToDouble(tf.valeurterrain);

            ViewBag.valeur = (int)val;
            ViewBag.supertxt = tf.superficie;
            ViewBag.valeurtxt = (int)val;
            return View(Op);
        }
        [HttpPost]
        public ActionResult Create(FormCollection form, Operations operation)
        {
            var natureid = Convert.ToInt32(form["idNatureOperation"]);
            var typeid = Convert.ToInt32(form["idTypeOperation"]);
            var DateOperation = Convert.ToDateTime(form["dateOperation"]);
            var idTF = Convert.ToInt32(form["IDTF"]);
            var ExistDistractionList = db.Tfs_Distraction.Any(i => i.ID_TF == idTF);
            if(ExistDistractionList == false)
            {
                TempData["message"] = "Veuillez saisir le(s) Tf(s) distrait!";
                TempData["status"] = "Erreur";
                tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                operation.tf = tf;
                operation.idTypeOperation = 13;
                operation.idNatureOperation = 4;
                ViewBag.IDTF = operation.idTF;
                ViewBag.superficie = form["superficie"];
                ViewBag.valeur = form["valeur"];

                return View(operation);

            }

            var Chek = OperationController.DateNotInFutur(Convert.ToDateTime(DateOperation));
            if (Chek == true)
            {
                TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                TempData["status"] = "Erreur";
                tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                operation.tf = tf;
                operation.idTypeOperation = 13;
                operation.idNatureOperation = 4;
                ViewBag.IDTF = operation.idTF;
                ViewBag.superficie = form["superficie"];
                ViewBag.valeur = form["valeur"];
                
                return View(operation);
            }
            operation.idTF = Convert.ToInt32(form["IDTF"]);
            var ListeTFDistraction = db.Tfs_Distraction.Where(i => i.ID_TF == operation.idTF && i.ID_Operation == null && i.Actif == false).ToList();

            operation.Tfs_Distraction = ListeTFDistraction;
            
            operation.datecreation = DateTime.Now;
            operation.datemodification = DateTime.Now;
            operation.tempOperation = 1;
            operation.activeOperation = 0;
            operation.valideOperation = 0;
            operation.IDStatus = 1;
            operation.dateOperation = DateOperation;
            db.Operations.Add(operation);
            db.SaveChanges();
            var nature = db.NatureOperation.Find(operation.idNatureOperation);
            var type = db.TypesOperation.Find(operation.idTypeOperation);
            var TF = db.tf.Find(operation.idTF);
            historique.Tracers("Creation", nature.nom, type.nom, "", OperationController.CurrentUsers(User.Identity.Name), operation, TF, null);

            return RedirectToAction("EditCreate", "Operations", new { id = operation.id });
        }


        public ActionResult CreateDistraction(int? id)
        {
            Tfs_Distraction TFD = new Tfs_Distraction();
            TFD.ID_TF = id;
            //var Lastnumtf = db.tfs.Max(i => i.numerotf).FirstOrDefault();
            //ViewBag.NumTFS = Convert.ToInt32(Lastnumtf) + 1;
            return PartialView("_Create", TFD);
        }
        [HttpPost]
        public ActionResult CreateDistraction(Tfs_Distraction TFS,FormCollection form,string CheckEtat)
        {
            TFS.NumeroTF = form["NumTFS"];
            TFS.Superficie = form["SuperficieTFS"];
            TFS.ValeurTerrain = form["ValeurTFS"];
            TFS.Actif = false;
           
            List <Tfs_Distraction_Proprios> ListTFSPRO = new List<Tfs_Distraction_Proprios>();
            var ListNbreTFS = form.AllKeys.Where(k => k.StartsWith("Nom")).ToList();
            var ListNbreDate = form.AllKeys.Where(k => k.StartsWith("DateNaiss")).ToList();
            var ListNbreTFSENTRE = form.AllKeys.Where(k => k.StartsWith("EnName")).ToList();
            var ListNbreTFSRES = form.AllKeys.Where(k => k.StartsWith("ResName")).ToList();
            var ListNbreDateRES = form.AllKeys.Where(k => k.StartsWith("NaissRes")).ToList();
            db.Tfs_Distraction.Add(TFS);
            //db.SaveChanges();
            bool Etat = false;
            if (CheckEtat == "on")
            {
                Etat = true;
            }
            else Etat = false;

            Tfs_Distraction_Proprios TFSPRO = new Tfs_Distraction_Proprios();

            if (Etat != true)
            {
                if (ListNbreTFS.Count != 0)
                {
                    for (int i = 0; i < ListNbreTFS.Count; i++)
                    {
                        string Nom = ListNbreTFS[i];
                        string NumPro = form[Nom];
                        if(NumPro != "")
                        {
                            TFSPRO.NomComplet = NumPro.ToUpper();
                        }
                        string k = ListNbreDate[i];
                        string Date = form[k];

                        if (Date != "")
                        {
                            //var Datet = Convert.ToDateTime(Date);
                            TFSPRO.DateNaiss = Date;
                        }
                        TFSPRO.IDTypeProprios = 1;
                        TFSPRO.IDTypeEntreprise = null;
                        // ListTFSPRO.Add(TFSPRO);
                        // TFS.Tfs_Distraction_Proprios
                        TFSPRO.ID_Distracton = TFS.ID;
                        //ListTFSPRO.Add(TFSPRO);
                        //TFSPRO.Tfs_Distraction = TFS;
                        db.Tfs_Distraction_Proprios.Add(TFSPRO);
                        db.SaveChanges();
                    }
                }
                if(ListNbreTFSENTRE.Count != 0)
                {
                    for (int i = 0; i < ListNbreTFSENTRE.Count; i++)
                    {

                        string NomEn = ListNbreTFSENTRE[i];
                        string NumPro = form[NomEn];
                        TFSPRO.NomEntreprise = NumPro;

                        
                        string NomRes = ListNbreTFSRES[i];
                        string NumRes = form[NomRes];
                        if (NumRes != "")
                        { TFSPRO.NomComplet = NumRes.ToUpper(); }

                        string k = ListNbreDate[i];
                        string Date = form[k];

                        if (Date != "")
                        {
                            //var Datet = Convert.ToDateTime(Date);
                            TFSPRO.DateNaiss = Date;
                        }
                        TFSPRO.IDTypeProprios = 2;
                        TFSPRO.IDTypeEntreprise = 1;
                        // ListTFSPRO.Add(TFSPRO);
                        // TFS.Tfs_Distraction_Proprios
                        TFSPRO.ID_Distracton = TFS.ID;
                        //ListTFSPRO.Add(TFSPRO);
                        //TFSPRO.Tfs_Distraction = TFS;
                        db.Tfs_Distraction_Proprios.Add(TFSPRO);
                        db.SaveChanges();
                    }
                }
                if(ListNbreTFS.Count ==0 && ListNbreTFSENTRE.Count == 0)
                {
                    TempData["message"] = "Veuillez indiqué au moin un proprietaire!";
                    TempData["status"] = "Erreur";
                    ViewBag.ValeurTFS = form["ValeurTFS"];
                    ViewBag.SuperficieTFS = form["SuperficieTFS"];
                    ViewBag.NumTFS = form["NumTFS"];
                    return PartialView("_Create", TFS);
                }
            }
            else
            {
                TFSPRO.NomComplet = "Etat de Djibouti";
                TFSPRO.ID_Distracton = TFS.ID;
                db.Tfs_Distraction_Proprios.Add(TFSPRO);
                db.SaveChanges();
            }
            // TFS.Tfs_Distraction_Proprios = ListTFSPRO;



            string url = Url.Action("List", "Distractions", new { id = TFS.ID_TF,idd=0 });
            return Json(new { success = true, url = url });
        }

        public ActionResult DetailsDistraction(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tfs_Distraction TFD = db.Tfs_Distraction.Find(id);
            TFD.Tfs_Distraction_Proprios = db.Tfs_Distraction_Proprios.Where(i => i.ID_Distracton == TFD.ID).ToList();

            return PartialView("_Details",TFD);
        }
        [HttpGet]
        public ActionResult EditDistraction(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tfs_Distraction TFD = db.Tfs_Distraction.Find(id);
            TFD.Tfs_Distraction_Proprios = db.Tfs_Distraction_Proprios.Where(i => i.ID_Distracton == TFD.ID).ToList();
            return PartialView("_Edit",TFD);
        }
        //[HttpPost]
        //public ActionResult EditDistraction(int id)
        //{
        //    return PartialView("_Edit");
        //}
        public ActionResult DeleteDistraction(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tfs_Distraction TFD = db.Tfs_Distraction.Find(id);
            TFD.Tfs_Distraction_Proprios = db.Tfs_Distraction_Proprios.Where(i => i.ID_Distracton == TFD.ID).ToList();
            
            return PartialView("_Delete", TFD);
        }
        [HttpPost]
        public ActionResult DeleteDistract(Tfs_Distraction TF)
        {
           
            var TFD = db.Tfs_Distraction.Find(TF.ID);
            var IDTF = TFD.ID_TF;
            var TFDP = db.Tfs_Distraction_Proprios.Where(i => i.ID_Distracton == TFD.ID).ToList();
            db.Tfs_Distraction.Attach(TFD);
            db.Tfs_Distraction.Remove(TFD);
            db.SaveChanges();
            string url = Url.Action("List", "Distractions", new { id = IDTF,idd = 0 });
            return Json(new { success = true, url = url });
        }


        public ActionResult VerifieSuper(string id, int? idtf)
        {
            var Sup = Convert.ToDouble(id);
            var tf = db.tf.Find(idtf);
            var listTFD = db.Tfs_Distraction.Where(i => i.ID_TF == idtf && i.Actif == false).ToList();
            var t = Convert.ToDouble(tf.superficie);
            var Superficie = (double)t;
            Double compteur = 0;
            for(int j=0;j<listTFD.Count;j++)
            {
                compteur += Convert.ToDouble(listTFD[j].Superficie);
            }
          
            
            var Total = Superficie - (Sup + compteur);
            if(Total > 0)
            {
                return Json(new { success = true, sup = Total});
            }
           else
                return Json(new { success = false, sup = Total});
        }

        public ActionResult VerifieNumeros(string num)
        {
            var NTF = Convert.ToInt32(num);
            var NumerosTF = db.tf.Any(i => i.numerotf == NTF);
            if (NumerosTF == false)
            {
                return Json(new { success = true});
            }
            else
                return Json(new { success = false});
        }
    }

}