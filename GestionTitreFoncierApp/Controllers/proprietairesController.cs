﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class proprietairesController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController OperationController = new OperationsController();
        // GET: proprietaires
        public ActionResult Index()
        {

            try
            {

                
                    try
                    {
                        var proprietaires = db.proprietaire.Include(p => p.typeproprietaire1);
            return View(proprietaires.ToList());
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // START ADD BY SAAD

        public ActionResult List(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS INDEX PROP :" + id);
            var op = db.Operations.Find(id);
            var prop = op.proprietaire.ToList();
            ViewBag.ID_OP = id;

            if(op.IDStatus == 2 || (op.IDStatus == 1 && op.tempOperation == 1))
            {
                ViewBag.StatusEnAttente = "Oui";
            }
            else ViewBag.StatusEnAttente = "Non";
            //var typeactes 
            TempData["ProCount"] = prop.Count();
            return PartialView("List", prop);
            //return View(actes.ToList());
        }

        public ActionResult ListDetails(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS INDEX PROP :" + id);
            var op = db.Operations.Find(id);
            var prop = op.proprietaire.ToList();
            ViewBag.ID_OP = id;
            //var typeactes 
            TempData["ProCount"] = prop.Count();
            return PartialView("ListDetails", prop);
            //return View(actes.ToList());
        }

        public ActionResult detailsProp(int? id, int idop)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS EDIT PROP  :" + id);

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                proprietaire proprietaire = db.proprietaire.Find(id);
                ProprietaresViewModel pr = new ProprietaresViewModel();
                pr.idOperation = idop;
                pr.prenom = proprietaire.prenom;
                pr.nome2 = proprietaire.nome2;
                pr.nom3 = proprietaire.nom3;

                pr.prenommere = proprietaire.prenommere;
                pr.nommere2 = proprietaire.nommere2;
                pr.nommere3 = proprietaire.nommere3;
                pr.lieuNaisance = proprietaire.lieuNaisance;
                pr.dateNaissace = proprietaire.dateNaissace;
                pr.numeropiece = proprietaire.numeropiece;

                pr.piecedateemission = proprietaire.piecedateemission;
                pr.typepiece = proprietaire.typepiece;
                pr.numeropiece = proprietaire.numeropiece;

                pr.nationalite = proprietaire.nationalite;



                pr.nomestablishment = proprietaire.nomestablishment;
                pr.nomeresponsable1 = proprietaire.nomeresponsable1;
                pr.nomeresponsable2 = proprietaire.nomeresponsable2;
                pr.nomeresponsable3 = proprietaire.nomeresponsable3;
                pr.typestablishment = proprietaire.typestablishment;
                pr.secturactivite = proprietaire.secturactivite;
                pr.typeproprietaire = proprietaire.typeproprietaire;

                
                if (proprietaire == null)
                {
                    return HttpNotFound();
                }

                

                return PartialView( pr);
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View("Error");
            }
        }
        public ActionResult Vendors(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS Vendors PROP :" + id);
            var op = db.Operations.Find(id);
            
            if (op.Vendors != null)
            {

                var prop = op.Vendors.proprietaire.ToList();;
                ViewBag.ID_OP = id;
                return PartialView("Vendors", prop);
            }
            
           
            return PartialView("Vendors", null);
            //return View(actes.ToList());
        }

        //public ActionResult VendorsDetails(int id)
        //{

        //}

        public ActionResult CreateProp(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS CREATE PROP :" + id);
            ProprietaresViewModel prop = new ProprietaresViewModel();
            ViewBag.ID_OP = id;
            prop.idOperation = id;
            try
            {
                ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                ViewBag.Nationalite = new[]
                   {

                            new SelectListItem { Value = "1", Text = "Djiboutienne" },
                            new SelectListItem { Value = "2", Text = "Etranger" },
                        };

                ViewBag.typepiece = new[]
                    {

                    new SelectListItem { Value = "1", Text = "Carte Identité" },
                    new SelectListItem { Value = "2", Text = "Passeport" },
                };

                return PartialView("CreateProp", prop);


            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View("Error");
            }
           
            //return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProp(ProprietaresViewModel proprietaireToCreate)
        {
            if (ModelState.IsValid)
            {
                System.Diagnostics.Debug.WriteLine(" ***************** DANS CREATE PROP 2 :" + proprietaireToCreate.idOperation);

           
                    proprietaire pro = new proprietaire();
                    if (proprietaireToCreate.prenom != null && proprietaireToCreate.prenom != "")
                    {
                    if (proprietaireToCreate.prenom != null)
                    {
                        pro.prenom = proprietaireToCreate.prenom.ToUpper();
                    }
                    if (proprietaireToCreate.nome2 != null)
                    {
                        pro.nome2 = proprietaireToCreate.nome2.ToUpper();

                    }
                    if (proprietaireToCreate.nom3 != null)
                    {
                        pro.nom3 = proprietaireToCreate.nom3.ToUpper();
                    }
                    pro.prenommere = proprietaireToCreate.prenommere;
                        pro.nommere2 = proprietaireToCreate.nommere2;
                        pro.nommere3 = proprietaireToCreate.nommere3;
                        // if(proprietaireToCreate.nationalite!=0)
                        // {
                        //     pro.nationalite = proprietaireToCreate.nationalite;

                        // }
                        //else
                        //{
                        //     pro.nationalite = 3;
                        //}
                        pro.nationalite = proprietaireToCreate.nationalite;

                        pro.typepiece = proprietaireToCreate.typepiece;
                        
                        pro.lieuNaisance = proprietaireToCreate.lieuNaisance;
                        pro.dateNaissace = proprietaireToCreate.dateNaissace;
                        pro.typepiece = proprietaireToCreate.typepiece;
                        pro.numeropiece = proprietaireToCreate.numeropiece;
                        pro.piecedateemission = proprietaireToCreate.piecedateemission;
                        pro.typeproprietaire = 1;
                        db.proprietaire.Add(pro);
                        Operations op = db.Operations.Find(proprietaireToCreate.idOperation);
                        op.proprietaire.Add(pro);
                        db.SaveChanges();
                 


                        historique.Tracers("Creation", "Beneficiaire", "Beneficiaire Physique", pro.prenom, OperationController.CurrentUsers(User.Identity.Name), op, op.tf, null);
                        string url = Url.Action("List", "proprietaires", new { id = proprietaireToCreate.idOperation });
                        return Json(new { success = true, url = url });

                    }
                    else
                    {
                    if (proprietaireToCreate.nomestablishment != null)
                    {
                        pro.nomestablishment = proprietaireToCreate.nomestablishment.ToUpper();
                    }
                    if (proprietaireToCreate.nomeresponsable1 != null)
                    {
                        pro.nomeresponsable1 = proprietaireToCreate.nomeresponsable1.ToUpper();

                    }
                    if (proprietaireToCreate.nomeresponsable2 != null)
                    {
                        pro.nomeresponsable2 = proprietaireToCreate.nomeresponsable2.ToUpper();

                    }
                    if (proprietaireToCreate.nomeresponsable3 != null)
                    {
                        pro.nomeresponsable3 = proprietaireToCreate.nomeresponsable3.ToUpper();

                    }
                   

                        pro.secturactivite = proprietaireToCreate.secturactivite;
                        pro.typestablishment = proprietaireToCreate.typestablishment;
                        pro.typepiece = proprietaireToCreate.typepiece;
                        pro.numeropiece = proprietaireToCreate.numeropiece;
                        pro.piecedateemission = proprietaireToCreate.piecedateemission;
                        pro.typeproprietaire = 2;
                        db.proprietaire.Add(pro);

                        Operations op = db.Operations.Find(proprietaireToCreate.idOperation);
                        op.proprietaire.Add(pro);
                        db.SaveChanges();
                        historique.Tracers("Creation", "Beneficiaire", "Beneficiaire Morale", pro.nomestablishment, OperationController.CurrentUsers(User.Identity.Name), op, op.tf, null);
                        TempData["ProCount"] = 1;
                        string url = Url.Action("List", "proprietaires", new { id = proprietaireToCreate.idOperation });
                        return Json(new { success = true, url = url });
                    }


             
            }


            ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle", proprietaireToCreate.typeproprietaire);
            ViewBag.Nationalite = new[]
               {

                            new SelectListItem { Value = "1", Text = "Djiboutienne" },
                            new SelectListItem { Value = "2", Text = "Etranger" },
                        };

            ViewBag.typepiece = new[]
                {

                    new SelectListItem { Value = "1", Text = "Carte Identité" },
                    new SelectListItem { Value = "2", Text = "Passeport" },
                };

            return PartialView();

            //return PartialView("CreateProp", proprietaireToCreate);

        }

        public ActionResult EditProp(int? id , int idop)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS EDIT PROP  :" + id);

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                proprietaire proprietaire = db.proprietaire.Find(id);
                ProprietaresViewModel pr = new ProprietaresViewModel();
                pr.idOperation = idop;
                pr.prenom = proprietaire.prenom;
                pr.nome2 = proprietaire.nome2;
                pr.nom3 = proprietaire.nom3;

                pr.prenommere = proprietaire.prenommere;
                pr.nommere2 = proprietaire.nommere2;
                pr.nommere3 = proprietaire.nommere3;
                pr.lieuNaisance = proprietaire.lieuNaisance;
                pr.dateNaissace = proprietaire.dateNaissace;
                pr.numeropiece = proprietaire.numeropiece;

                pr.piecedateemission = proprietaire.piecedateemission;
                pr.typepiece = proprietaire.typepiece;
                pr.numeropiece = proprietaire.numeropiece;

                pr.nationalite = proprietaire.nationalite;



                pr.nomestablishment = proprietaire.nomestablishment;
                pr.nomeresponsable1 = proprietaire.nomeresponsable1;
                pr.nomeresponsable2 = proprietaire.nomeresponsable2;
                pr.nomeresponsable3 = proprietaire.nomeresponsable3;
                pr.typestablishment = proprietaire.typestablishment;
                pr.secturactivite = proprietaire.secturactivite;
                pr.typeproprietaire = proprietaire.typeproprietaire;

                ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                ViewBag.typepiece = new SelectList(db.typepiece, "id", "libelle");
                ViewBag.Nationalite = new SelectList(db.Nationalite, "id", "libelle");

                if (proprietaire == null)
                {
                    return HttpNotFound();
                }

                ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle", proprietaire.typeproprietaire);

                ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                ViewBag.Nationalite = new[]
                   {

                            new SelectListItem { Value = "1", Text = "Djiboutienne" },
                            new SelectListItem { Value = "2", Text = "Etranger" },
                        };

                ViewBag.typepiece = new[]
                    {

                    new SelectListItem { Value = "1", Text = "Carte Identité" },
                    new SelectListItem { Value = "2", Text = "Passeport" },
                };

                return PartialView("EditProp", pr);
            }
                    catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProp(ProprietaresViewModel predited)
        {
            try
            {

                    try
                    {
                        if (ModelState.IsValid)
                        {
                            proprietaire proprietaire = db.proprietaire.Find(predited.id);
                            ProprietaresViewModel pr = new ProprietaresViewModel();
                        if (predited.prenom != null)
                        {
                            proprietaire.prenom = predited.prenom.ToUpper();

                        }
                        if (predited.nome2 != null)
                        {
                            proprietaire.nome2 = predited.nome2.ToUpper();

                        }
                        if (predited.nom3 != null)
                        {
                            proprietaire.nom3 = predited.nom3.ToUpper();

                        }
                        proprietaire.prenommere = predited.prenommere;
                            proprietaire.nommere2 = predited.nommere2;
                            proprietaire.nommere3 = predited.nommere3;
                            proprietaire.lieuNaisance = predited.lieuNaisance;
                            proprietaire.dateNaissace = predited.dateNaissace;
                            proprietaire.numeropiece = predited.numeropiece;
                            proprietaire.piecedateemission = predited.piecedateemission;
                        if (predited.nomestablishment != null)
                        {
                            proprietaire.nomestablishment = predited.nomestablishment.ToUpper();
                        }
                        if (predited.nomeresponsable1 != null)
                        {
                            proprietaire.nomeresponsable1 = predited.nomeresponsable1.ToUpper();
                        }
                        if (predited.nomeresponsable2 != null)
                        {
                            proprietaire.nomeresponsable2 = predited.nomeresponsable2.ToUpper();
                        }
                        if (predited.nomeresponsable3 != null)
                        {
                            proprietaire.nomeresponsable3 = predited.nomeresponsable3.ToUpper();
                        }
                        proprietaire.typestablishment = predited.typestablishment;
                            proprietaire.secturactivite = predited.secturactivite;
                            proprietaire.nationalite = predited.nationalite;
                            proprietaire.typepiece = predited.typepiece;


                            if (predited.prenom != null && predited.prenom != "")
                            {
                                proprietaire.typeproprietaire = 1;

                            }
                            else
                            {
                                proprietaire.typeproprietaire = 2;

                            }


                            //if (predited.nationalite != 0)
                            //{
                            //    proprietaire.nationalite = predited.nationalite;

                            //}
                            //else
                            //{
                            //    proprietaire.nationalite = 3;
                            //}

                            db.Entry(proprietaire).State = EntityState.Modified;
                            db.SaveChanges();

                           // historique.Tracers("Creation", "Beneficiaire", "Beneficiaire Morale", pro.NomComplet, OperationController.CurrentUsers(User.Identity.Name), op, null, null);

                            string url = Url.Action("List", "proprietaires", new { id = predited.idOperation });
                            return Json(new { success = true, url = url });
                        }

                        ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle", predited.typeproprietaire);
                        ViewBag.Nationalite = new[]
                           {

                            new SelectListItem { Value = "1", Text = "Djiboutienne" },
                            new SelectListItem { Value = "2", Text = "Etranger" },
                        };

                        ViewBag.typepiece = new[]
                            {

                    new SelectListItem { Value = "1", Text = "Carte Identité" },
                    new SelectListItem { Value = "2", Text = "Passeport" },
                };

                        return PartialView();

                        //ViewBag.typeproprietaire = new SelectList(db.typeproprietaires, "id", "libelle", proprietaire.typeproprietaire);
                        //return PartialView("details", proprietaire);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               

                

            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }

            
            
        }

        // END ADD BY SAAD
        // GET: proprietaires/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            proprietaire proprietaire = db.proprietaire.Find(id);
            if (proprietaire == null)
            {
                return HttpNotFound();
            }
            return PartialView(proprietaire);
        }

        // GET: proprietaires/Create
        public ActionResult Create()
        {
            try
            {

                    try
                    {
                        ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
         // ViewBag.typepiece = new SelectList(db.typepieces, "id", "libelle");
            //ViewBag.Nationalite = new SelectList(db.Nationalites, "id", "libelle");
            ViewBag.Nationalite = new[]
               {
        
        new SelectListItem { Value = "1", Text = "Djiboutian" },
        new SelectListItem { Value = "2", Text = "Etranger" },
    };
            ViewBag.typepiece = new[]
    {

        new SelectListItem { Value = "1", Text = "Carte Identite" },
        new SelectListItem { Value = "2", Text = "Passeport" },
    };

            return PartialView();


        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }

            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: proprietaires/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(proprietaire proprietaireToCreate)
        {
            try
            {

                    try
                    {
                        proprietaire pro = new proprietaire();
            if (proprietaireToCreate.prenom != null && proprietaireToCreate.prenom != "")
            {
                        if (proprietaireToCreate.prenom != null)
                        {
                            pro.prenom = proprietaireToCreate.prenom.ToUpper();
                        }
                        if (proprietaireToCreate.nome2 != null)
                        {
                            pro.nome2 = proprietaireToCreate.nome2.ToUpper();

                        }
                        if (proprietaireToCreate.nom3 != null)
                        {
                            pro.nom3 = proprietaireToCreate.nom3.ToUpper();
                        }

                        pro.prenommere = proprietaireToCreate.prenommere;
                pro.nommere2 = proprietaireToCreate.nommere2;
                pro.nommere3 = proprietaireToCreate.nommere3;
                // if(proprietaireToCreate.nationalite!=0)
                // {
                //     pro.nationalite = proprietaireToCreate.nationalite;

                // }
                //else
                //{
                //     pro.nationalite = 3;
                //}
                   pro.nationalite = proprietaireToCreate.nationalite;

                pro.typepiece = proprietaireToCreate.typepiece;

               
                   
              
                pro.lieuNaisance = proprietaireToCreate.lieuNaisance;
                pro.dateNaissace = proprietaireToCreate.dateNaissace;
                pro.typepiece = proprietaireToCreate.typepiece;
                pro.numeropiece = proprietaireToCreate.numeropiece;
                pro.piecedateemission = proprietaireToCreate.piecedateemission;
                pro.typeproprietaire = 1;
                db.proprietaire.Add(pro);
                db.SaveChanges();

            }
            else
            {
                        if (proprietaireToCreate.nomestablishment != null)
                        {
                            pro.nomestablishment = proprietaireToCreate.nomestablishment.ToUpper();
                        }
                        if(proprietaireToCreate.nomeresponsable1 != null)
                        {
                            pro.nomeresponsable1 = proprietaireToCreate.nomeresponsable1.ToUpper();

                        }
                        if (proprietaireToCreate.nomeresponsable2 != null)
                        {
                            pro.nomeresponsable2 = proprietaireToCreate.nomeresponsable2.ToUpper();

                        }
                        if (proprietaireToCreate.nomeresponsable3 != null)
                        {
                            pro.nomeresponsable3 = proprietaireToCreate.nomeresponsable3.ToUpper();

                        }

                        pro.secturactivite = proprietaireToCreate.secturactivite;
                pro.typestablishment = proprietaireToCreate.typestablishment;
                pro.typepiece = proprietaireToCreate.typepiece;
                pro.numeropiece = proprietaireToCreate.numeropiece;
                pro.piecedateemission = proprietaireToCreate.piecedateemission;
                pro.typeproprietaire = 2;
                db.proprietaire.Add(pro);
                db.SaveChanges();
            }
           

            return PartialView("details",pro);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }

            // return View(proprietaire);
        }

        // GET: proprietaires/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {

               
                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            proprietaire proprietaire = db.proprietaire.Find(id);
            ProprietaresViewModel pr = new ProprietaresViewModel();
            pr.prenom = proprietaire.prenom;
                pr.nome2=proprietaire.nome2;
            pr.nom3 = proprietaire.nom3;

            pr.prenommere= proprietaire.prenommere;
            pr.nommere2= proprietaire.nommere2;
            pr.nommere3= proprietaire.nommere3;
            pr.lieuNaisance= proprietaire.lieuNaisance;
            pr.dateNaissace= proprietaire.dateNaissace;
            pr.numeropiece= proprietaire.numeropiece;

             pr.piecedateemission= proprietaire.piecedateemission;
            pr.typepiece= proprietaire.typepiece;
            pr.numeropiece = proprietaire.numeropiece;

            pr.nationalite = proprietaire.nationalite;



            pr.nomestablishment = proprietaire.nomestablishment;
            pr.nomeresponsable1 = proprietaire.nomeresponsable1;
            pr.nomeresponsable2 = proprietaire.nomeresponsable2;
            pr.nomeresponsable3 = proprietaire.nomeresponsable3;
            pr.typestablishment = proprietaire.typestablishment;
            pr.secturactivite = proprietaire.secturactivite;
            pr.typeproprietaire = proprietaire.typeproprietaire;

            ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
            ViewBag.typepiece = new SelectList(db.typepiece, "id", "libelle");
            ViewBag.Nationalite = new SelectList(db.Nationalite, "id", "libelle");

            if (proprietaire == null)
            {
                return HttpNotFound();
            }

            ViewBag.typeproprietaire = new SelectList(db.typeproprietaire, "id", "libelle", proprietaire.typeproprietaire);
            return PartialView("edit",pr);
        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: proprietaires/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProprietaresViewModel predited)
        {
    try
    {

       
            try
            {
                proprietaire proprietaire = db.proprietaire.Find(predited.id);
            ProprietaresViewModel pr = new ProprietaresViewModel();
                    if(predited.prenom!= null)
                    {
                        proprietaire.prenom = predited.prenom.ToUpper();

                    }
                    if (predited.nome2 != null)
                    {
                        proprietaire.nome2 = predited.nome2.ToUpper();

                    }
                    if (predited.nom3 != null)
                    {
                        proprietaire.nom3 = predited.nom3.ToUpper();

                    }
                    proprietaire.prenommere = predited.prenommere;
            proprietaire.nommere2 = predited.nommere2;
            proprietaire.nommere3 = predited.nommere3;
            proprietaire.lieuNaisance = predited.lieuNaisance;
            proprietaire.dateNaissace = predited.dateNaissace;
            proprietaire.numeropiece = predited.numeropiece;
            proprietaire.piecedateemission = predited.piecedateemission;
                    if (predited.nomestablishment != null)
                    {
                        proprietaire.nomestablishment = predited.nomestablishment.ToUpper();
                    }
                    if (predited.nomeresponsable1 != null)
                    {
                        proprietaire.nomeresponsable1 = predited.nomeresponsable1.ToUpper();
                    }
                    if (predited.nomeresponsable2 != null)
                    {
                        proprietaire.nomeresponsable2 = predited.nomeresponsable2.ToUpper();
                    }
                    if (predited.nomeresponsable3 != null)
                    {
                        proprietaire.nomeresponsable3 = predited.nomeresponsable3.ToUpper();
                    }
                        proprietaire.typestablishment = predited.typestablishment;
                        proprietaire.secturactivite = predited.secturactivite;
                  
           proprietaire.nationalite = predited.nationalite;
            proprietaire.typepiece = predited.typepiece;


            if (predited.prenom != null && predited.prenom != "")
            {
                proprietaire.typeproprietaire =1;

            }
            else
            {
                proprietaire.typeproprietaire = 2;

            }


            //if (predited.nationalite != 0)
            //{
            //    proprietaire.nationalite = predited.nationalite;

            //}
            //else
            //{
            //    proprietaire.nationalite = 3;
            //}

            db.Entry(proprietaire).State = EntityState.Modified;
            db.SaveChanges();

            //ViewBag.typeproprietaire = new SelectList(db.typeproprietaires, "id", "libelle", proprietaire.typeproprietaire);
            return PartialView("details", proprietaire);
}
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: proprietaires/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            proprietaire proprietaire = db.proprietaire.Find(id);
            if (proprietaire == null)
            {
                return HttpNotFound();
            }
            return View(proprietaire);
        }

        // POST: proprietaires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            proprietaire proprietaire = db.proprietaire.Find(id);
            db.proprietaire.Remove(proprietaire);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
