﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;
using System.Security;
using System.Web.Security;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class ActesController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController OperationController = new OperationsController();


        // GET: Actes
        public ActionResult Index(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS INDEX ACTE :" + id);
            var actes = db.Actes.Where(a => a.idOperation == id).ToList();
            ViewBag.ID_OP = id;
            var RadiationExist = actes.Any(i => i.idTypeActe == 1010);
            var opera = db.Operations.Find(id);

            //var typeactes 
            if( opera.IDStatus == 4 || opera.IDStatus == 5 || (opera.IDStatus == 1 && opera.tempOperation == 0) || opera.IDStatus == null)
            {
                ViewBag.RadiationAnnulationValidationExist = "Oui";
            }

            TempData["ActesCount"] = actes.Count();
            return PartialView("_Index", actes.ToList());
            //return View(actes.ToList());
        }

        [ChildActionOnly]
        public ActionResult List(int id)
        {
            ViewBag.ID_OP = id;
            var actes = db.Actes.Where(a => a.idOperation == id);
            ViewBag.ActesCount = actes.Count();
            return PartialView("_List", actes.ToList());
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actes acte = db.Actes.Find(id);
            TypeActes TY = db.TypeActes.Find(acte.idTypeActe);
            acte.TypeActes = TY;
            
            Acteurs AC = db.Acteurs.Find(acte.idActeur);
            TypeActeur TA = db.TypeActeur.Find(AC.idTypeActeur);
            AC.TypeActeur = TA;
            acte.Acteurs = AC;
            if(acte.idTypeActe == 1011)
            {
                var vend = acte.Operations.Vendors.Acteurs.Where(i=>i.TypeActeur.type == "CRE").Select(i=>i.nom).FirstOrDefault();
                var mont = acte.Operations.Vendors.MontantSub;
                ViewBag.BANK = vend;
                ViewBag.BANKMont = mont;
            }
            if (acte == null)
            {
                return HttpNotFound();
            }
            return PartialView(acte);
        }
        
        public ActionResult Create(int? id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** DANS CREATE ACTE :" + id);
            Actes acte = new Actes();
            acte.idOperation = id;
            Operations op = db.Operations.Find(id);
            var actes = db.Actes.Where(i => i.idOperation == id).ToList();
            var typeactes = new List<TypeActes>();
            typeactes = op.TypesOperation.TypeActes.ToList();
            if (op.idNatureOperation == 2)
            {
                if (op.IDStatus != 3)
                {
                    typeactes = typeactes.Where(i => i.id != 1010).ToList();
                }
            }
            if (op != null)
            {
                if (actes.Count == 0)
                {
                    typeactes = typeactes.Where(i => i.estobligatoire == 1).ToList();
                }
                else
                {
                    //typeop = db.TypesOperations.Find(op.idTypeOperation);
                    typeactes = typeactes.ToList();
                    // Pour le cas de l'hypotheque judiciaire ... 
                    

                }
                int idacte = 0;
                if(typeactes.Count()== 1)
                {
                     idacte = typeactes.Select(i => i.id).FirstOrDefault();
                }
                
                ViewBag.idTypeActe = new SelectList(typeactes, "id", "nomacte",idacte);
            }
            else
            {

                ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte");

            }

            var Creancier = db.Acteurs.Where(i => i.IDTypeProprietaire == 2 && i.idTypeActeur == 4 && i.id != op.idActeur).ToList();
            ViewBag.Creancier = new SelectList(Creancier, "id", "nom" );
            var typeacteurs = db.TypeActeur.Where(a => a.type == "ACT").ToList();
            ViewBag.idTypeActeur = new SelectList(typeacteurs, "id", "libelle",1);

            var acteurs = db.Acteurs.Where(a => a.TypeActeur.id == 1).ToList();
            ViewBag.idActeur = new SelectList(acteurs, "id", "nom");
            return PartialView("_Create", acte);
            //return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // public ActionResult Create([Bind(Include = "id,numacte,dateacte,numacp,dateacp,idTypeActe,idActeur,idOperation")] Actes acte, FormCollection collections)
        public ActionResult Create([Bind(Include = "id,numacte,dateacte,numacp,dateacp,idTypeActe,idActeur,idOperation")] Actes acte, FormCollection collections)
        {
                if (ModelState.IsValid)
                {
                    var type = db.TypeActes.Find(acte.idTypeActe);
                    var operation = db.Operations.Find(acte.idOperation);

                /*----------------------------- Pour la notification ----------------------------------------*/
                if (Convert.ToInt32(collections["idTypeActe"]) == 1017)
                {
                    acte.numacte = "Notification/" + operation.tf.numerotf + "/" + operation.id;
                }

                    /* ---------------------- Pour la subrogation ----------------------------------------------------- */

                    if (Convert.ToInt32(collections["idTypeActe"]) == 1011)
                    {
                        var NewMontant = Convert.ToInt32(collections["Montant"]);
                        var NewCreancier = Convert.ToInt32(collections["Creancier"]);

                        var CreancierOperation = operation.idActeur;
                        var MontantOperation = operation.montantOperation;

                        operation.montantOperation = NewMontant;
                        operation.idActeur = NewCreancier;

                        var Acteur = db.Acteurs.Find(CreancierOperation);

                        /*---------------- Si Debiteur Existe -----------------------------------*/

                        if (operation.idVendor != null)
                        {
                            Vendors vend = db.Vendors.Find(operation.idVendor);
                            vend.MontantSub = MontantOperation;
                            vend.Acteurs.Add(Acteur);
                            db.Entry(vend).State = EntityState.Modified;
                            db.Entry(operation).State = EntityState.Modified;
                        }
                        else
                        {
                            Vendors V = new Vendors();
                            V.idTF = Convert.ToInt32(operation.idTF);
                            V.datesave = DateTime.Now;
                            V.MontantSub = MontantOperation;
                            V.Acteurs.Add(Acteur);
                            db.Vendors.Add(V);
                            operation.Vendors = V;
                            db.Entry(operation).State = EntityState.Modified;
                        }
                    }
                    /* ---------------------- Pour l'hypotheque judiciaire definitive ----------------------------------- */

                    if (Convert.ToInt32(collections["idTypeActe"]) == 1014)
                    {
                        operation.idTypeOperation = 11;
                        db.Entry(operation).State = EntityState.Modified;
                    }

                    /*-------------------------- La Radiation --------------------------------------------------------------*/

                    if (Convert.ToInt32(collections["idTypeActe"]) == 1010)
                    {
                        operation.IDStatus = 5;
                        db.Entry(operation).State = EntityState.Modified;
                    }

                    System.Diagnostics.Debug.WriteLine(" ***************** DANS CREATE ACTE 2 :" + acte.idOperation);
                    acte.idActeur = Convert.ToInt32(collections["idActeur"]);
                    var IDACTeurs = Convert.ToInt32(acte.idActeur);
                    acte.datecreation = DateTime.Now;
                    acte.datemodification = DateTime.Now;

                    var ExistActes = db.Actes.Any(i => i.numacte == acte.numacte && i.idActeur == acte.idActeur);
                    if (ExistActes == true)
                    {
                        TempData["message"] = "Ce numeros Acte Existe déjà !";
                        TempData["status"] = "Erreur";
                        var idtype = Convert.ToInt32(collections["idTypeActeur"]);
                        var typeacteurs = db.TypeActeur.Where(a => a.type == "ACT").ToList();
                        ViewBag.idTypeActeur = new SelectList(typeacteurs, "id", "libelle", idtype);
                        ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte", acte.idTypeActe);
                        ViewBag.idActeur = new SelectList(db.Acteurs, "id", "nom", acte.idActeur);
                        var Creancier = db.Acteurs.Where(i => i.IDTypeProprietaire == 2 && i.idTypeActeur == 4 && i.id != operation.idActeur).ToList();
                        ViewBag.Creancier = new SelectList(Creancier, "id", "nom");
                        return PartialView("_Create", acte);
                    }

                   var chekdateActe = OperationController.DateNotInFutur(Convert.ToDateTime(acte.dateacte));
                   var chekdateEnreg = OperationController.DateNotInFutur(Convert.ToDateTime(acte.dateacp));
                   if (chekdateActe == true || chekdateEnreg == true)
                   {
                        
                        TempData["message"] = "La date de l'acte et de l'enregistrement doivent etre anterieurs a la date d'aujourd'hui !";
                        TempData["status"] = "Erreur";
                        var idtype = Convert.ToInt32(collections["idTypeActeur"]);
                        var typeacteurs = db.TypeActeur.Where(a => a.type == "ACT").ToList();
                        ViewBag.idTypeActeur = new SelectList(typeacteurs, "id", "libelle", idtype);
                        ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte", acte.idTypeActe);
                        ViewBag.idActeur = new SelectList(db.Acteurs, "id", "nom", acte.idActeur);
                        var Creancier = db.Acteurs.Where(i => i.IDTypeProprietaire == 2 && i.idTypeActeur == 4 && i.id != operation.idActeur).ToList();
                        ViewBag.Creancier = new SelectList(Creancier, "id", "nom");
                        return PartialView("_Create", acte);
                   }
                    db.Actes.Add(acte);
                    db.SaveChanges();

                    historique.Tracers("Creation", "Acte", type.nomacte, acte.numacte, OperationController.CurrentUsers(User.Identity.Name), operation, operation.tf, acte);
                    TempData["ActesCount"] = 1;
                    string url = Url.Action("Index", "Actes", new { id = acte.idOperation });
                    return Json(new { success = true, url = url });
                }

                ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte", acte.idTypeActe);
                ViewBag.idActeur = new SelectList(db.Acteurs, "id", "nom", acte.idActeur);
                //return View(acte);
                return PartialView("_Create", acte);
           
        }

        // GET: Actes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actes acte = db.Actes.Find(id);
            if (acte == null)
            {
                return HttpNotFound();
            }

            Operations op = acte.Operations;
            if (op != null)
            {
                //typeop = db.TypesOperations.Find(op.idTypeOperation);
                var typeactes = op.TypesOperation.TypeActes.ToList();
                ViewBag.idTypeActe = new SelectList(typeactes, "id", "nomacte", acte.idTypeActe);
                ViewBag.idTypeActeur = new SelectList(db.TypeActeur, "id", "libelle",acte.Acteurs.idTypeActeur);
                ViewBag.idActeur = new SelectList(db.Acteurs, "id", "nom",acte.idActeur);
               
            }
            else
                ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte", acte.idTypeActe);
            acte.dateacp = acte.dateacp.Value.Date;

            //return View(acte);
            if(acte.idTypeActe == 1011)
            {
                var cr = op.Vendors.Acteurs.Where(i => i.idTypeActeur == 4).FirstOrDefault();
                var Creancier = db.Acteurs.Where(i => i.IDTypeProprietaire == 2 && i.idTypeActeur == 4 && i.id != op.idActeur).ToList();
                ViewBag.Creancier = new SelectList(Creancier, "id", "nom",cr.id);
                ViewBag.Montant = Convert.ToInt32(op.Vendors.MontantSub);
            }

            System.Diagnostics.Debug.WriteLine(" ***************** DANS EDIT ACTE  :" + acte.idOperation);

            return PartialView("_Edit", acte);
        }

        // POST: Actes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit([Bind(Include = "id,numacte,dateacte,numacp,dateacp,idTypeActe,idActeur,idOperation")] Actes acte)
        {
            
                var type = db.TypeActes.Find(acte.idTypeActe);
                var operation = db.Operations.Find(acte.idOperation);
                var nature = db.NatureOperation.Find(operation.idNatureOperation);
                if (ModelState.IsValid)
                {
                    System.Diagnostics.Debug.WriteLine(" ***************** DANS EDIT ACTE 2 :" + acte.idOperation);

                    acte.datemodification = DateTime.Now;
                    db.Entry(acte).State = EntityState.Modified;
                    db.Entry(acte).Property(x => x.datecreation).IsModified = false;
                    db.SaveChanges();
                    historique.Tracers("Modification","Acte", type.nomacte, acte.numacp, OperationController.CurrentUsers(User.Identity.Name), operation, operation.tf, acte);
                    //return RedirectToAction("Index");
                    string url = Url.Action("Index", "Actes", new { id = acte.idOperation });
                    return Json(new { success = true, url = url });
                }

                Operations op = acte.Operations;
                if (op != null)
                {
                    //typeop = db.TypesOperations.Find(op.idTypeOperation);
                    var typeactes = op.TypesOperation.TypeActes.ToList();
                    ViewBag.idTypeActe = new SelectList(typeactes, "id", "nomacte", acte.idTypeActe);
                }
                else
                    ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte", acte.idTypeActe);

                return PartialView("_Edit", acte);
           
        }

        // GET: Actes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Actes acte = db.Actes.Find(id);
            if (acte == null)
            {
                return HttpNotFound();
            }

            return PartialView("_Delete", acte);
            
        }

        // POST: Actes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
          
                if (id != null)
                {
                 
                    var acte = db.Actes.Find(id);
                    var idmut = acte.idOperation;
                    var operation = db.Operations.Find(idmut);
                    var type = db.TypeActes.Find(acte.idTypeActe);
                    var ACP = acte.numacp;
                    var nuacte = acte.numacte;
                    db.Actes.Attach(acte);
                    db.Actes.Remove(acte);
                    //db.Entry(acte).State = EntityState.Deleted;
                    db.SaveChanges();
                    historique.Tracers("Annulation", "Acte", type.nomacte, nuacte, OperationController.CurrentUsers(User.Identity.Name), operation, null, null);
                    string url = Url.Action("Index", "Actes", new { id = idmut });
                    return Json(new { success = true, url = url });
                }
                return PartialView("_Delete", id);
                // return RedirectToAction("Index");
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult ChoixTypeActeur(string idtype)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** CHOIX ID TYPE ACTEUR :" + idtype);

            int typeacteur = Convert.ToInt32(idtype);
            ViewBag.idTypeActe = new SelectList(db.TypeActes, "id", "nomacte");

            var typeacteurs = db.TypeActeur.Where(a => a.type == "ACT").ToList();
            ViewBag.idTypeActeur = new SelectList(typeacteurs, "id", "libelle", typeacteur);

            var acteurs = db.Acteurs.Where(a => a.TypeActeur.id == typeacteur).ToList();
            ViewBag.idActeur = new SelectList(acteurs, "id", "nom");


            SelectList Acteurs = new SelectList(acteurs, "id", "nom",0);
            return Json(Acteurs);
        }
    }
}
