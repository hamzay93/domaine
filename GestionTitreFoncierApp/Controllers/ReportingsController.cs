﻿using CrystalDecisions.CrystalReports.Engine;
using GestionTitreFoncierApp.Models;
using GestionTitreFoncierApp.Reports.DataSets;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using NsExcel = Microsoft.Office.Interop.Excel;
using System.Drawing;
using System.Web.UI;

namespace GestionTitreFoncierApp.Controllers
{
    public class ReportingsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        // GET: Reportings
        public ActionResult EtatReports(int id)
        {
            if(id == null)
            {

            }
            tf tf = db.tf.Find(id);
            if(tf == null)
            {

            }

            var Typeterrain = "";
            if(tf.estbati==true)
            {
                Typeterrain = "Bati";
            }
            else if(tf.estbati == false)
            {
                Typeterrain = "Non bati";
            }

            List<ElementHypotheque> ListEH = new List<ElementHypotheque>();
            ReportsModels Eh = new ReportsModels();
                
            var operationsList = db.Operations.Where(i => i.idTF == tf.id && i.idNatureOperation == 2 && i.IDStatus == 3).ToList();
            if(operationsList.Count > 0)
            {
               for(int i=0;i<operationsList.Count;i++)
                {
                    if (Convert.ToInt32(operationsList[i].rang) == 1)
                    {
                        Eh.Rang1 = Convert.ToInt32(operationsList[i].rang);
                        Eh.Nature1 = operationsList[i].NatureOperation.nom;
                        Eh.type1 = operationsList[i].TypesOperation.nom;
                        Eh.Beneficiaire1 = operationsList[i].Acteurs.nom;
                    }

                    else if (Convert.ToInt32(operationsList[i].rang) == 2)
                    {
                        Eh.Rang2 = Convert.ToInt32(operationsList[i].rang);
                        Eh.Nature2 = operationsList[i].NatureOperation.nom;
                        Eh.type2 = operationsList[i].TypesOperation.nom;
                        Eh.Beneficiaire2 = operationsList[i].Acteurs.nom;
                    }
                    else if (Convert.ToInt32(operationsList[i].rang) == 3)
                    {
                        Eh.Rang3 = Convert.ToInt32(operationsList[i].rang);
                        Eh.Nature3 = operationsList[i].NatureOperation.nom;
                        Eh.type3 = operationsList[i].TypesOperation.nom;
                        Eh.Beneficiaire3 = operationsList[i].Acteurs.nom;
                    }
                }
            }
            //List<ProprietaireElement> ListPro = new List<ProprietaireElement>();
            //ProprietaireElement PRO = new ProprietaireElement();
            //var Proprios = tf.proprietaires.ToList();
            //if(Proprios.Count >0)
            //{
            //    for(int j=0; j < Proprios.Count; j++)
            //    {
            //        PRO.id = Proprios[j].id;
            //        PRO.prenom = Proprios[j].prenom;
            //        PRO.nome2 = Proprios[j].nome2;
            //        PRO.nom3 = Proprios[j].nom3;
            //        ListPro.Add(PRO);
            //    }
            //}

            var NomComplet1 = "";
            var NomComplet2 = "";
            if(tf.proprietaire.Count ==2)
            {
                NomComplet1 = tf.proprietaire.ElementAt(1).nome2;
            }
            if(tf.proprietaire.Count > 2)
            {
                NomComplet1 = tf.proprietaire.ElementAt(1).nome2;
                NomComplet2 = tf.proprietaire.ElementAt(2).nom3;
            }

            List<ReportsModels> RM = new List<ReportsModels>
             {
                new ReportsModels(){Nomproprietaire=tf.proprietaire.ElementAt(0).prenom+"    "+NomComplet1+"   "+NomComplet2,
                    ID = tf.id,NumerosTF=Convert.ToString(tf.numerotf),TypeTF=tf.typetf1.libell,TypeTerrain=Typeterrain,Lieu=tf.quartier.Ville.region.libelle+" - "+tf.quartier.libelle,DateInscription=Convert.ToDateTime(tf.datetf),Superficie=tf.superficie,Valeur=tf.valeurterrain,Rang1=Eh.Rang1,Nature1=Eh.Nature1,Beneficiaire1=Eh.Beneficiaire1,type1=Eh.type1,Rang2=Eh.Rang2,Nature2=Eh.Nature2,Beneficiaire2=Eh.Beneficiaire2,type2=Eh.type2,Rang3=Eh.Rang3,Nature3=Eh.Nature3,Beneficiaire3=Eh.Beneficiaire3,type3=Eh.type3}
             };

            RM = RM.ToList();


            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports"), "ReportOfinformationTFs.rpt"));
            rd.SetDataSource(RM);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();
            //try
            //{
            Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            //.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, @"D:\ASD.pdf");
            //
            stream.Seek(0, SeekOrigin.Begin);
            return File(stream, "application/pdf", tf.numerotf + "-" + DateTime.Now + ".pdf");
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.Status = ex.ToString();

            //}
            return RedirectToAction("Details", "tfs", id);


        }

      

        public ActionResult SaveExcel()
        {
            // Step 1 - get the data from database
            var tfs = Session["ListTFS"];

            if (tfs != null)
            {
                var listTf = new List<tf>();
                List<tf> list = tfs as List<tf>;
                int MAX = list.Count;

                List<TFElement> tfelement = new List<TFElement>();
                TFElement TFE = null;

                foreach (var item in list)
                {
                    TFE = new TFElement();
                    TFE.id = item.id;
                    TFE.numerotf = item.numerotf;
                    TFE.Superficie = item.superficie;
                    TFE.Valeur = item.valeurterrain;
                    if (item.datetf != null)
                    {
                        TFE.datetf = item.datetf.Value.ToString("dd-MM-yyyy");
                    }
                    else TFE.datetf = "";
                    if (item.quartier != null && item.idquartier != 23198)
                    {
                        TFE.Lieux = item.quartier.Ville.libelle + " - " + item.quartier.libelle;
                    }
                    else TFE.Lieux = "NO ADRESSE";
                    TFE.numerolot = item.numerolot;
                    TFE.proprietaires = new List<ProprietaireElement>();

                    List<ProprietaireElement> PROE = new List<ProprietaireElement>();
                    ProprietaireElement PRE = null;
                    foreach (var props in item.proprietaire)
                    {
                        PRE = new ProprietaireElement();
                        
                        if (props.typeproprietaire == 1)
                        {
                            PRE.prenom = props.prenom;
                        }
                        else
                        {
                            PRE.prenom = props.nomestablishment;
                        }
                        if (item.proprietaire.Count > 1)
                        {
                            TFE.AllProps += PRE.prenom + " || ";
                        }
                        else
                        {
                            TFE.AllProps += PRE.prenom;
                        }
                        PRE.id = props.id;
                        PROE.Add(PRE);
                        TFE.proprietaires.Add(PRE);
                    }
                    tfelement.Add(TFE);
                }

                
                var data = tfelement.ToList().Select(p => new
                {
                    p.numerotf,
                    p.AllProps,
                    p.datetf,
                    p.Superficie,
                    p.Valeur,
                    p.Lieux,
                    p.numerolot
                });

                // instantiate the GridView control from System.Web.UI.WebControls namespace
                // set the data source
                GridView gridview = new GridView();
                gridview.DataSource = data;
                gridview.DataBind();
                var TodayDate = DateTime.Today.ToString("dd-MM-yyyy");
                var timeNow = DateTime.Now.ToString("HH-mm-ss");
                // Clear all the content from the current response
                Response.ClearContent();
                Response.Buffer = true;
                // set the header
                Response.AddHeader("content-disposition", "attachment;filename =" + MAX + " TFS " + TodayDate + "à" + timeNow + ".xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                gridview.HeaderRow.Cells[0].Text = "Numeros TF";
                gridview.HeaderRow.Cells[0].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[0].Font.Bold = true;
                gridview.HeaderRow.Cells[0].Font.Size = 14;
                gridview.HeaderRow.Cells[1].Text = "Nom Proprietaires";
                gridview.HeaderRow.Cells[1].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[1].Font.Bold = true;
                gridview.HeaderRow.Cells[1].Font.Size = 14;
                gridview.HeaderRow.Cells[2].Text = "Date immatriculation";
                gridview.HeaderRow.Cells[2].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[2].Font.Bold = true;
                gridview.HeaderRow.Cells[2].Font.Size = 14;
                gridview.HeaderRow.Cells[3].Text = "Superficie (m²)";
                gridview.HeaderRow.Cells[3].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[3].Font.Bold = true;
                gridview.HeaderRow.Cells[3].Font.Size = 14;
                gridview.HeaderRow.Cells[4].Text = "Valeur (FDJ)";
                gridview.HeaderRow.Cells[4].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[4].Font.Bold = true;
                gridview.HeaderRow.Cells[4].Font.Size = 14;
                gridview.HeaderRow.Cells[5].Text = "Adresse";
                gridview.HeaderRow.Cells[5].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[5].Font.Bold = true;
                gridview.HeaderRow.Cells[5].Font.Size = 14;
                gridview.HeaderRow.Cells[6].Text = "N°Lot";
                gridview.HeaderRow.Cells[6].BackColor = ColorTranslator.FromHtml("#f8da00");
                gridview.HeaderRow.Cells[6].Font.Bold = true;
                gridview.HeaderRow.Cells[6].Font.Size = 14;
                // create HtmlTextWriter object with StringWriter
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        // render the GridView to the HtmlTextWriter
                        gridview.RenderControl(htw);
                        // Output the GridView content saved into StringWriter
                        Response.Output.Write(sw.ToString());
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            return RedirectToAction("Index", "tfs");
        }
    }
}