﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class FacturationsActeursController : Controller
    {
       // CultureInfo ci = new CultureInfo("fr-FR");
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        // GET: FacturationsActeurs
        public ActionResult Index(string Acteurs,string Mois,string Status,string Annee,string NumFAC)
        {
            var ActeurList = db.Acteurs.Where(i => i.idTypeActeur != 4 && i.idTypeActeur != 5).OrderBy(i=>i.nom).ToList();
            ViewBag.Acteurs = new SelectList(ActeurList, "id", "nom");
            var Stat = db.Factures_Status.ToList();
            ViewBag.Status = new SelectList(Stat, "ID", "Libelle");

            var Facturations = db.Factures_Acteurs.Where(i=>i.ID != 0);

            if(Acteurs != "" && Acteurs != null)
            {
                var IDActeurs = Convert.ToInt32(Acteurs);
                Facturations = Facturations.Where(i => i.ID_Acteurs == IDActeurs);
                ViewBag.Acteurs = new SelectList(ActeurList, "id", "nom",IDActeurs);
            }
            if(Mois != "" && Mois != null)
            {
                var IDMois = Convert.ToInt32(Mois);
                Facturations = Facturations.Where(i => i.Mois == IDMois);
            }
            if (Status != "" && Status != null)
            {
                var IDStatus = Convert.ToInt32(Status);
                Facturations = Facturations.Where(i => i.Status == IDStatus);
            }
            if (Annee != "" && Annee != null)
            {
                var IDAnnee = Convert.ToInt32(Annee);
                Facturations = Facturations.Where(i => i.Annees == IDAnnee);
            }
            if (NumFAC != "" && NumFAC != null)
            {
                Facturations = Facturations.Where(i => i.NumeroFact == NumFAC);
            }
            return View(Facturations.ToList());
        }
        public ActionResult GenerateAllFactures()
        {
            var listActeurs = db.Acteurs.Where(i => i.idTypeActeur != 4 && i.idTypeActeur != 5).ToList();
            var DernierDate = new DateTime(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month, DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.AddMonths(-1).Month), 23, 59, 59);
            var PremierDate = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            for (int i=0;i<listActeurs.Count;i++)
            {
                FacturationsParActeur(listActeurs[i].id, listActeurs[i].IDCompte,PremierDate,DernierDate);
            }
            return RedirectToAction("Index", "FacturationsActeurs");
        }
        public void FacturationsParActeur(int? IDActeur,int? IDCompteActeur,DateTime PremierDates ,DateTime DernierDates)
        {
            CultureInfo ci = new CultureInfo("fr-FR");
            var UserID = db.utilisateur.Where(i => i.login == User.Identity.Name).Select(k => k.id).FirstOrDefault();
            var NbrConsultationParActeurForMount = db.Consultation.Where(i => i.idUser == IDCompteActeur && (i.dateconsultation <= DernierDates && i.dateconsultation >= PremierDates)).ToList();
            decimal MontantFixe = 2500;
            if (NbrConsultationParActeurForMount.Count > 0)
            {
               
                Factures_Acteurs FAs = new Factures_Acteurs();
                FAs.Annees = DateTime.Today.Year;
                FAs.Nom_Mois = PremierDates.ToString("MMMM",ci);
                FAs.Mois = PremierDates.Month;
                FAs.ID_Acteurs = IDActeur;
                FAs.ID_Paiment = null;
                FAs.NumeroFact = FAs.Mois + "" + IDActeur + "/" + FAs.Annees;
                FAs.DateFacture = DateTime.Now;
                FAs.Status = 1;
                FAs.User_Created = UserID;
                FAs.NbrConsultation = NbrConsultationParActeurForMount.Count;
                FAs.Montant = FAs.NbrConsultation * MontantFixe;
                var ExistFac = db.Factures_Acteurs.Any(i => i.ID_Acteurs == FAs.ID_Acteurs && i.Mois == FAs.Mois);
                if (ExistFac != true)
                {
                    db.Factures_Acteurs.Add(FAs);
                    db.SaveChanges();
                }
               
            }
        }

        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factures_Acteurs FA = db.Factures_Acteurs.Find(id);
            if (FA == null)
            {
                return HttpNotFound();
            }

            return View(FA);
        }

        public ActionResult ReglementFacture(int? id)
        {
            if (id == null) { id = 0; }

            return PartialView(id);
        }
        public ActionResult ReglerFacture(string NumQui,string Montant,int? id)
        {
            var UserID = db.utilisateur.Where(i => i.login == User.Identity.Name).Select(k => k.id).FirstOrDefault();
            Paiments_Factures pf = new Paiments_Factures();
            pf.NumerosQuittance = NumQui;
            pf.Montant = Convert.ToDecimal(Montant);
            pf.User_Created = UserID;
            pf.DatePaiement = DateTime.Now;
            db.Paiments_Factures.Add(pf);

            var FA = db.Factures_Acteurs.Find(id);
            FA.ID_Paiment = pf.ID;
            FA.Status = 2;
            db.Entry(FA).Property(x => x.ID_Paiment).IsModified = true;
            db.SaveChanges();


            return Json(new
            {
                success = true
            });
                //return RedirectToAction("Details", "FacturationsActeurs");
            }

        }
}