﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
    public class TFDefinitifsController : Controller
    {

        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController OperationController = new OperationsController();
        // GET: TFDefinitifs
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tf tf = db.tf.Find(id);
            Operations Op = new Operations();
            Op.tf = tf;
            Op.idTF = tf.id;
            Op.idTypeOperation = 14;
            Op.idNatureOperation = 5;
            ViewBag.IDTF = tf.id;
            return View(Op);
        }
        [HttpPost]
        public ActionResult Create(Operations operations,FormCollection form)
        {
            var natureid = Convert.ToInt32(form["idNatureOperation"]);
            var typeid = Convert.ToInt32(form["idTypeOperation"]);
            var DateOperation = Convert.ToDateTime(form["dateOperation"]);
            var Chek = OperationController.DateNotInFutur(Convert.ToDateTime(DateOperation));
            if (Chek == true)
            {
                TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                TempData["status"] = "Erreur";
                tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                operations.tf = tf;
                operations.idTypeOperation = 14;
                operations.idNatureOperation = 5;
                ViewBag.IDTF = operations.idTF;
               
                return View(operations);
            }
            operations.idTF = Convert.ToInt32(form["IDTF"]);
            operations.datecreation = DateTime.Now;
            operations.datemodification = DateTime.Now;
            operations.tempOperation = 1;
            operations.activeOperation = 0;
            operations.valideOperation = 0;
            operations.IDStatus = 1;
            operations.dateOperation = DateOperation;
            db.Operations.Add(operations);
            db.SaveChanges();
            var nature = db.NatureOperation.Find(operations.idNatureOperation);
            var type = db.TypesOperation.Find(operations.idTypeOperation);
            var TF = db.tf.Find(operations.idTF);
            historique.Tracers("Creation", nature.nom, type.nom, "", OperationController.CurrentUsers(User.Identity.Name), operations, TF, null);
            return RedirectToAction("EditCreate", "Operations", new { id = operations.id });

        }
        }
}