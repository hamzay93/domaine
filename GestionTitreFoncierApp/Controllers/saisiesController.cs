﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class saisiesController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        public ActionResult Index()
        {
            try
            {

               
                    try
                    {
                        ViewBag.user= ViewBag.typelieu = new SelectList(db.utilisateur, "id", "nom"); 
            return View();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
             
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }
        public ActionResult saisiesview(DateTime? datedu, DateTime? dateau, string user)

        {
            try
            {

              
                    try
                    {
                        List<saisie> lst = new List<saisie>();
            lst = null;
            //saisielst = db.saisies.ToList();
            //ar results = lst;
            if (user != "")//Search saisie For all users
            {
                int cuserid = Convert.ToInt32(user);
                if (datedu != null && dateau != null)
                {
                                        var results = db.saisie.Where(x => x.datesaisie1 >= datedu && x.datesaisie1 <= dateau && x.utilistaurid == cuserid)
                       .Select(y => new
                       {

                           usernom = y.utilisateur.nom,
                           totalSaisieSave = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 1).Count(),
                           totalSaisieEdit = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 2).Count(),
                           totalSaisie = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id).Count()
                       }).ToList();
                    return Json(results.Distinct());


                }
                else
                {

                    var results = db.saisie.Where(x => x.utilistaurid == cuserid)
                        .Select(y => new
                        {

                            usernom = y.utilisateur.nom,
                            totalSaisieSave = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 1).Count(),
                            totalSaisieEdit = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 2).Count(),
                            totalSaisie = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id).Count()
                        }).ToList();
                    return Json(results.Distinct());


                  
                }
            }
            else
            {
                if (datedu != null && dateau != null)
                {
                    var results = db.saisie.Where(x => x.datesaisie1 >= datedu && x.datesaisie1 <= dateau)
                        .Select(y => new
                        {

                            usernom = y.utilisateur.nom,
                            totalSaisieSave = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 1).Count(),
                            totalSaisieEdit = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 2).Count(),
                            totalSaisie = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id).Count()
                        }).ToList();
                    return Json(results.Distinct());


                    
                }
                else
                {
                    int userid;

                    
                var results = db.saisie
                         .Select(y => new
                         {

                             usernom = y.utilisateur.nom,
                             totalSaisieSave = db.saisie.Where(u=>u.utilistaurid==y.utilisateur.id && u.typesaisie==1).Count(),
                             totalSaisieEdit = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id && u.typesaisie == 2).Count(),
                             totalSaisie = db.saisie.Where(u => u.utilistaurid == y.utilisateur.id).Count()
                         }).ToList();
                    return Json(results.Distinct());
                }
            }
        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }

            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }
        // GET: saisies
        //public ActionResult Index(DateTime ? datedu, DateTime ? dateau, string  user)
        //{
        //    List<utilisateur> saisielst = new List<utilisateur>();
        //    saisielst = db.utilisateurs.ToList();
        //    //if (user!="")//Search saisie For all users
        //    //{
        //    //    if (datedu != null && dateau != null)
        //    //    {    
        //    //        utilisateur ut = new utilisateur();
        //    //        //saisielst = db.saisies.Where(x => x.datesaisie1 > datedu && x.datesaisie1 < dateau).ToList();

        //    //       // saisielst = db.utilisateurs.Where(x => x.id==user).ToList();

        //    //    }
        //    //    else
        //    //    {
        //    //        saisielst = db.utilisateurs.ToList();
        //    //       
        //    //        //IList<SelectListItem> enrgsitmentCount = new List<SelectListItem>
        //    //        // {
        //    //        // new SelectListItem{Text = "California", Value = "B"},
        //    //        // new SelectListItem{Text = "Alaska", Value = "B"},
        //    //        // new SelectListItem{Text = "Illinois", Value = "B"},
        //    //        // new SelectListItem{Text = "Texas", Value = "B"},
        //    //        // new SelectListItem{Text = "Washington", Value = "B"}

        //    //        // };
        //    //    }

        //    //}
        //    //else
        //    //{
        //    //    int userid = Convert.ToInt32(user);
        //    //    if (datedu != null && dateau != null)
        //    //    {

        //    //        saisielst = db.utilisateurs.Where(x => x.id == userid).ToList();
        //    //        ViewBag.countEnregistment = db.saisies.Where(x => x.datesaisie1 > datedu && x.datesaisie1 < dateau && x.typesaisie == 1 && x.utilistaurid == userid).ToList();
        //    //        ViewBag.countmiseajour = db.saisies.Where(x => x.datesaisie1 > datedu && x.datesaisie1 < dateau && x.typesaisie == 2 && x.utilistaurid == userid).ToList();

        //    //    }
        //    //    else
        //    //    {
        //    //        saisielst = db.utilisateurs.Where(x =>x.id == userid).ToList();
        //    //        ViewBag.countEnregistment = db.saisies.Where(x =>x.typesaisie==1 && x.utilistaurid== userid).ToList();
        //    //        ViewBag.countmiseajour = db.saisies.Where(x =>x.typesaisie == 2 && x.utilistaurid == userid).ToList();
        //    //    }
        //    //}

        //    ViewBag.user = ViewBag.typelieu = new SelectList(db.utilisateurs, "id", "nom");
        //    return View(saisielst);
        //}

        // GET: saisies/Details/5
        public ActionResult Details(int? id)
        {
            try
            {

                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            saisie saisie = db.saisie.Find(id);
            if (saisie == null)
            {
                return HttpNotFound();
            }
            return View(saisie);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: saisies/Create
        public ActionResult Create()
        {
            try
            {

                    try
                    {
                        ViewBag.utilistaurid = new SelectList(db.utilisateur, "id", "nom");
            ViewBag.typesaisie = new SelectList(db.typesaisie, "id", "libelle");
            return View();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }

        }

        // POST: saisies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,datesaisie,utilistaurid,typesaisie,timesaisie,datesaisie1")] saisie saisie)
        {
            try
            {

                    try
                    {
                        if (ModelState.IsValid)
            {
                db.saisie.Add(saisie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.utilistaurid = new SelectList(db.utilisateur, "id", "nom", saisie.utilistaurid);
            ViewBag.typesaisie = new SelectList(db.typesaisie, "id", "libelle", saisie.typesaisie);
            return View(saisie);
        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: saisies/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {

                
                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            saisie saisie = db.saisie.Find(id);
            if (saisie == null)
            {
                return HttpNotFound();
            }
            ViewBag.utilistaurid = new SelectList(db.utilisateur, "id", "nom", saisie.utilistaurid);
            ViewBag.typesaisie = new SelectList(db.typesaisie, "id", "libelle", saisie.typesaisie);
            return View(saisie);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }

            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: saisies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,datesaisie,utilistaurid,typesaisie,timesaisie,datesaisie1")] saisie saisie)
        {

            try
            {

              
                    try
                    {
                        if (ModelState.IsValid)
            {
                db.Entry(saisie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.utilistaurid = new SelectList(db.utilisateur, "id", "nom", saisie.utilistaurid);
            ViewBag.typesaisie = new SelectList(db.typesaisie, "id", "libelle", saisie.typesaisie);
            return View(saisie);
        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }

            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: saisies/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {

                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            saisie saisie = db.saisie.Find(id);
            if (saisie == null)
            {
                return HttpNotFound();
            }
            return View(saisie);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
             
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: saisies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                    try
                    {
                        saisie saisie = db.saisie.Find(id);
            db.saisie.Remove(saisie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
    }
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
