﻿using GestionTitreFoncierApp.Models;
using GestionTitreFoncierApp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
[Authorize]
    public class AutresOperationsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController Opérations = new OperationsController();
        // GET: AutresOperations


        [FiltresAuthorisation(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        public ActionResult Create(int? id, int? idop,int? idTypeop)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** ID TF :" + id);
            System.Diagnostics.Debug.WriteLine(" ***************** ID Nature :" + idop);
            System.Diagnostics.Debug.WriteLine(" ***************** ID Type Operation :" + idTypeop);

            if (id == null || idop == null || idTypeop == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Operations OP = new Operations();


            if (idop == 6) // Si IdNatureOperation est Autres Operations
            {
                OP.idNatureOperation = 6;
                if(idTypeop == 18) // Réevaluation de valeur
                {
                    OP.idTypeOperation = 18;
                    ViewBag.TitresOpérations = "Enregistrement d'une nouvelle réevaluation de valeur";
                }
                if(idTypeop == 19) // Rectification de Superficie
                {
                    OP.idTypeOperation = 19;
                    ViewBag.TitresOpérations = "Enregistrement d'une nouvelle rectification de superficie";

                }
                if (idTypeop == 16) // Incorporation de Superficie
                {
                    OP.idTypeOperation = 16;
                    ViewBag.TitresOpérations = "Enregistrement d'une incorporation de superficie";

                }

            }

            tf tf = db.tf.Find(id);
            ViewBag.IDTF = id;
            tf.proprietaire = tf.proprietaire;
            OP.tf = tf;
            OP.idTF = tf.id;

            return View(OP);
        }


        [FiltresAuthorisation(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Operations operation)
        {
            
            if (operation != null)
            {
                var Chek =Opérations.DateNotInFutur(Convert.ToDateTime(operation.dateOperation));

                if (Chek == true)
                {
                    TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                    TempData["status"] = "Erreur";
                    if (operation.idTypeOperation == 18) // Réevaluation de valeur
                    {
                        ViewBag.TitresOpérations = "Enregistrement d'une nouvelle réevaluation de valeur";
                    }
                    if (operation.idTypeOperation == 19) // Rectification de Superficie
                    {
                        ViewBag.TitresOpérations = "Enregistrement d'une nouvelle rectification de superficie";
                    }
                    if (operation.idTypeOperation == 16) // Incorporation de Superficie
                    {
                        ViewBag.TitresOpérations = "Enregistrement d'une incorporation de superficie";
                    }
                    tf tf = db.tf.Find(operation.idTF);
                    operation.tf = tf;
                    
                    return View(operation);
                }
               
              //  operation.idTF = Convert.ToInt32(collections["IDTF"]);
                operation.datecreation = DateTime.Now;
                operation.datemodification = DateTime.Now;
                operation.tempOperation = 1;
                operation.activeOperation = 0;
                operation.valideOperation = 0;
                operation.IDStatus = 1;
                db.Operations.Add(operation);
                await db.SaveChangesAsync();
                var nature = db.NatureOperation.Find(operation.idNatureOperation);
                var type = db.TypesOperation.Find(operation.idTypeOperation);
                var TF = db.tf.Find(operation.idTF);
                historique.Tracers("Creation", nature.nom, type.nom, "", Opérations.CurrentUsers(User.Identity.Name), operation,TF , null);
                var id = operation.id;
                return RedirectToAction("EditCreate", "Operations", new { id = id });
            }
            TempData["message"] = "L'opération n'a pas pris en compte donc veuillez actualisé cette page";
            TempData["status"] = "Erreur";
            return View(operation);


        }



    }
}