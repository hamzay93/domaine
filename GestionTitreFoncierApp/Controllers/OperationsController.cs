﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;
using PagedList;
using GestionTitreFoncierApp.Security;
using System.Text;
using System.Web.UI;
using System.Globalization;
using System.Data.Entity.Validation;

namespace GestionTitreFoncierApp.Controllers
{

    [Authorize]
   
    public class OperationsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        public string localphysicpath = @"c:\Domain\";
        // GET: Operations
        //[OutputCache(NoStore = true, Location = OutputCacheLocation.Client, Duration = 3)]
        public ActionResult ReturneURL(string returnUrl)
        {
            string decodedUrl = "";
            if (!string.IsNullOrEmpty(returnUrl))
            {
                decodedUrl = Server.UrlDecode(returnUrl);
                return Redirect(decodedUrl);
            }
            return Redirect(returnUrl);
        }
        public ActionResult Index(string IDOperations, string IDTypeOperations, string IDStatus, string NumTF)
        {
            var NatureOperations = db.NatureOperation.ToList();
            ViewBag.IDOperations = new SelectList(NatureOperations, "id", "nom");
            var TypeOperation = db.TypesOperation.ToList();
            ViewBag.IDTypeOperations = new SelectList(TypeOperation, "id", "nom");
            var ListStatus = db.Operations_Status.Where(i => i.ID != 1).ToList();
            ViewBag.IDStatus = new SelectList(ListStatus, "ID", "Libelle");

            System.Globalization.CultureInfo culInfo = new System.Globalization.CultureInfo("en-US");

            var operations = db.Operations.Include(o => o.NatureOperation).Include(o => o.tf).Include(o => o.Operations_Status).Where(i => i.idTF != null && i.IDStatus != 1);

            int IDType = 0;
            int intStatus = 0;
            if (!String.IsNullOrEmpty(IDTypeOperations) && IDTypeOperations != "" && IDTypeOperations != "0")
            {

                IDType = Convert.ToInt32(IDTypeOperations);
                operations = operations.Where(i => i.idTypeOperation == IDType);
                ViewBag.IDTypeOperations = new SelectList(TypeOperation, "id", "nom", IDType);
            }
            if (!String.IsNullOrEmpty(IDOperations) && IDOperations != "" && IDOperations != "0")
            {

                int IDoperations = Convert.ToInt32(IDOperations);
                operations = operations.Where(i => i.idNatureOperation == IDoperations);
                ViewBag.IDOperations = new SelectList(NatureOperations, "id", "nom", IDOperations);
                TypeOperation = TypeOperation.Where(i => i.idNatureOperation == IDoperations).ToList();
                ViewBag.IDTypeOperations = new SelectList(TypeOperation, "id", "nom", IDType);

            }
            if (!String.IsNullOrEmpty(NumTF))
            {
                var NTF = Convert.ToInt32(NumTF);
                operations = operations.Where(i => i.tf.numerotf== NTF);
            }
            if (!String.IsNullOrEmpty(IDStatus) && IDStatus != "" && IDStatus != "0")
            {

                intStatus = Convert.ToInt32(IDStatus);
                operations = operations.Where(i => i.Operations_Status.ID == intStatus);
                ViewBag.IDStatus = new SelectList(ListStatus, "ID", "Libelle", intStatus);
            }

            operations = operations.OrderByDescending(s => s.datemodification);
            ViewBag.NbreOperations = operations.Count();

            return View(operations.Take(1000).ToList());
        }

        [ChildActionOnly]
        public ActionResult List(int id)
        {
            ViewBag.ID_TF = id;
            var operations = db.Operations.Where(a => a.idTF == id && a.IDStatus != 1).OrderByDescending(i => i.datemodification);

            return PartialView("List", operations.ToList());
        }

        // GET: Operations/Details/5
        //[Authorize(Roles = "Agent,Agent Admin")]

        public async Task<ActionResult> Details(string returnUrl, int? id)
        {
            if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null)
                returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
            ViewData["RetourneURL"] = returnUrl;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operations operation = await db.Operations.FindAsync(id);
            operation.tf = db.tf.Find(operation.idTF);
            operation.Operations_Status = db.Operations_Status.Find(operation.IDStatus);
            operation.Vendors = db.Vendors.Find(operation.idVendor);
            if(operation.idNatureOperation == 2 && operation.Vendors != null)
            {
                var Vend = operation.Vendors.Acteurs.Where(i => i.idTypeActeur == 5).Select(o => o.nom).FirstOrDefault();
                if (Vend != null)
                {
                    ViewBag.Debiteurs = Vend;
                }
                else ViewBag.Debiteurs = null;
            }
            //var typProprios =operation.tf.proprietaires.Select(i => i.typeproprietaire).FirstOrDefault();
            //if (Convert.ToInt32(typProprios) == 1)
            //{
            //    ViewBag.Proprietaire = operation.tf.proprietaires.Select(i => i.NomComplet);
            //}
            //else
            //{
            //    ViewBag.Proprietaire = operation.tf.proprietaires.Select(i => i.nomestablishment);
            //}
            if (operation == null)
            {
                return HttpNotFound();
            }
            return View(operation);
        }
        [ChildActionOnly]
       
        public PartialViewResult DetailsTF(int? id)
        {
          if(id==null)
            {

            }
            tf titreF = db.tf.Find(id);

            //ViewBag.Proprietaire = Model.tf.proprietaires.Select(i => i.NomComplet).FirstOrDefault(); 
            return PartialView(titreF);

        }
        // GET: Operations/Create
        //[Authorize(Roles = "Sous-Agent,Agent,Agent Admin")]
        [FiltresAuthorisation(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        public ActionResult Create(int? id,int? idop)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** ID TF :" + id);
            System.Diagnostics.Debug.WriteLine(" ***************** ID Nature :" + idop);
            
            if( id == null || idop == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            tf tf = db.tf.Find(id);



            // modif par ikram var typeoperations = db.TypesOperation.Where(a => a.idNatureOperation == idop && a.id != 11);
               var typeoperations = db.TypesOperation.Where(a => a.idNatureOperation == idop && a.id != 9);
            if (tf.typetf != 2 && tf.typetf != 3)
            {
                typeoperations = typeoperations.Where(i=>i.id != 5);
            }
                ViewBag.idTypeOperation = new SelectList(typeoperations.ToList(), "id", "nom");
            ViewBag.idNatureOperation = idop;

           

            Operations OP = new Operations();
            
            
            ViewBag.IDTF = id;
            tf.proprietaire = tf.proprietaire;
            OP.tf = tf;
            OP.idTF = tf.id;


            if (idop == 1) // /*-------------- Mutations -------------------*/
            {
                var Typeentreprise = db.TypeEntreprise.ToList();
                ViewBag.Typeentreprise = new SelectList(Typeentreprise, "ID", "Libelle");
                ViewBag.idType = 3;
                ViewBag.style = "hide()";

            }
            if( idop == 2) // /*------------- Hypotheque ------------------*/
            {
                var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                ViewBag.IDDebiteur = new SelectList(IDDebiteur.OrderBy(i => i.nom), "id", "nom");

                var acteurs = db.Acteurs.Where(a => a.TypeActeur.type == "CRE").ToList();
                ViewBag.idActeur = new SelectList(acteurs.OrderBy(i => i.nom), "id", "nom");
                
                 ViewBag.MaxMontant = VerifieMontantRestant(Convert.ToInt32(id));
                ViewBag.MontantInsufisant = "La valeur du terrain est déjà hypothequé en totalité ";

                var Rangs = VerifieRangTF(Convert.ToInt32(id));
                OP.rang = Rangs;

            }


            /*----------------------------Details Proprietaire TF ------------------------------ */

            //var typProprios = tf.proprietaires.Select(i => i.typeproprietaire).FirstOrDefault();
            //if (Convert.ToInt32(typProprios) == 1)
            //{
            //    ViewBag.Proprietaire = tf.proprietaires.Select(i => i.NomComplet).FirstOrDefault();
            //}
            //else
            //{
            //    ViewBag.Proprietaire = tf.proprietaires.Select(i => i.nomestablishment).FirstOrDefault();
            //}

            return View(OP);
        }

        
        [FiltresAuthorisation(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Operations operation, FormCollection collections)
        {
            
            var natureid = Convert.ToInt32(collections["idNatureOperation"]);
            var typeid = Convert.ToInt32(collections["idTypeOperation"]);
            var Debiteur = Convert.ToInt32(collections["Debiteur"]);

            if (operation != null)
            {
                var Chek = DateNotInFutur(Convert.ToDateTime(operation.dateOperation));




                /* ---------------------------------- Operation Mutation -------------------------------------------------------------*/

                if (natureid == 1)
                {
                    operation.rang = null;
                    if (Chek == true)
                    {
                        TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                        TempData["status"] = "Erreur";
                        var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == natureid).ToList();
                        ViewBag.idTypeOperation = new SelectList(typeoperation, "id", "nom", typeid);
                        ViewBag.Montant = collections["Montant"];
                        ViewBag.idNatureOperation = Convert.ToInt32(collections["idNatureOperation"]);
                        operation.idTypeOperation = typeid;
                      
                        var Typeentreprise = db.TypeEntreprise.ToList();
                        ViewBag.Typeentreprise = new SelectList(Typeentreprise, "ID", "Libelle", collections["Typeentreprise"]);
                        ViewBag.Denomination = collections["Denomination"];


                        ViewBag.IDTF = Convert.ToInt32(collections["IDTF"]);
                        operation.tf = db.tf.Find(Convert.ToInt32(collections["IDTF"]));
                        tf tf = operation.tf;
                        ViewBag.Proprietaire = tf.proprietaire.Select(i => i.prenom).FirstOrDefault(); ;

                        return View(operation);
                    }
                   
                    operation.idNatureOperation = natureid;
                    operation.idTypeOperation = typeid;
                    if (typeid == 3)
                    {
                        Vendors vo = new Vendors();
                        vo.datesave = DateTime.Now;
                        vo.NomDenom = collections["Denomination"];
                        if(vo.NomDenom != "")
                        {
                            vo.NomDenom = vo.NomDenom.ToUpper();
                        }
                        vo.idTF = Convert.ToInt32(collections["IDTF"]);
                        vo.ID_Type_entreprise = Convert.ToInt32(collections["Typeentreprise"]);
                        db.Vendors.Add(vo);
                        operation.Vendors = vo;
                    }
                    if (typeid != 1 && typeid !=15 )
                    {
                        operation.montantOperation = 0;
                    }
                    if(typeid == 16)
                    {
                        operation.montantOperation = Convert.ToDecimal(collections["Superficie"]);
                    }

                }

                /*--------------------------------- Operation Hypotheque -------------------------------------------------------------*/

                if (natureid == 2)
                {
                   var IDACteur = Convert.ToInt32(collections["idActeur"]);
                    decimal MontantRestant = VerifieMontantRestant(Convert.ToInt32(operation.idTF));
                    var IDTypePro = Convert.ToInt32(collections["Debiteur"]);
                    var IDDebit = 0;
                    if (IDTypePro == 1)
                    {
                        IDDebit = 0;
                    }
                    else IDDebit=Convert.ToInt32(collections["IDDebiteur"]);
                  

                    if (Debiteur == 2)
                    {

                        var IDDebiteur = Convert.ToInt32(collections["IDDebiteur"]);
                        Acteurs AC = db.Acteurs.Find(IDDebiteur);
                        Vendors V = new Vendors();
                        V.datesave = DateTime.Now;
                        V.idTF = Convert.ToInt32(operation.idTF);
                        V.Acteurs.Add(AC);
                        db.Vendors.Add(V);
                        operation.Vendors = V;

                    }
                    
                    if (operation.montantOperation <= 0)
                    {
                        TempData["message"] = "Un Montant doit etre saisie Obligatoirement !";
                        TempData["status"] = "Erreur";
                        var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                        ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom", IDDebit);

                        var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == natureid && a.id != 11).ToList();
                        ViewBag.idTypeOperation = new SelectList(typeoperation, "id", "nom");
                        ViewBag.Montant = Convert.ToDecimal(collections["Montant"]);
                        
                        ViewBag.idNatureOperation = Convert.ToInt32(collections["idNatureOperation"]);

                        var acteurs = db.Acteurs.Where(i => i.idTypeActeur == 4).ToList();

                        if (operation.idTypeOperation == 8)
                        {
                            acteurs = acteurs.Where(i => i.IDTypeProprietaire == 2).ToList();
                        }
                        else if (operation.idTypeOperation == 9)
                        {
                            acteurs = acteurs.Where(i => i.IDTypeProprietaire == 1).ToList();
                        }
                        else acteurs = acteurs.ToList();
                        ViewBag.idActeur = new SelectList(acteurs, "id", "nom", operation.idActeur);
                        ViewBag.MaxMontant = MontantRestant;
                        ViewBag.IDTF = Convert.ToInt32(collections["IDTF"]);
                        operation.tf = db.tf.Find(Convert.ToInt32(collections["IDTF"]));

                        tf tf = operation.tf;
                        ViewBag.Proprietaire = tf.proprietaire.Select(i => i.prenom).FirstOrDefault(); ;
                        operation.idTypeOperation = null;
                        return View(operation);

                    }
                    if (IDACteur == 0)
                    {
                        TempData["message"] = "Veuillez selectionné un créancier !";
                        TempData["status"] = "Erreur";
                        var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                        ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom", IDDebit);

                        var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == natureid && a.id != 11).ToList();
                        ViewBag.idTypeOperation = new SelectList(typeoperation, "id", "nom");
                        ViewBag.Montant = Convert.ToDecimal(collections["Montant"]);
                        ViewBag.idNatureOperation = Convert.ToInt32(collections["idNatureOperation"]);
                        var acteurs = db.Acteurs.Where(i=>i.idTypeActeur == 4).ToList();
                        if (operation.idTypeOperation == 8)
                        {
                            acteurs = acteurs.Where(i => i.IDTypeProprietaire == 2).ToList();
                        }
                        else if (operation.idTypeOperation == 9)
                        {
                            acteurs = acteurs.Where(i => i.IDTypeProprietaire == 1).ToList();
                        }
                        else acteurs = acteurs.ToList();
                        ViewBag.idActeur = new SelectList(acteurs, "id", "nom", operation.idActeur);
                        ViewBag.MaxMontant = MontantRestant;
                        ViewBag.IDTF = Convert.ToInt32(collections["IDTF"]);
                        operation.tf = db.tf.Find(Convert.ToInt32(collections["IDTF"]));

                        tf tf = operation.tf;
                        ViewBag.Proprietaire = tf.proprietaire.Select(i => i.prenom).FirstOrDefault(); 
                        operation.montantOperation = Convert.ToDecimal(collections["Montant"]);
                        operation.idTypeOperation = null;
                        return View(operation);

                    }
                   
                    if (Chek == true)
                    {
                        TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                        TempData["status"] = "Erreur";
                        var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                        ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom", IDDebit);
                        var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == natureid && a.id != 11).ToList();
                        ViewBag.idTypeOperation = new SelectList(typeoperation, "id", "nom");
                        ViewBag.Montant = Convert.ToDecimal(collections["Montant"]);
                        ViewBag.idNatureOperation = Convert.ToInt32(collections["idNatureOperation"]);
                        ViewBag.idActeur = new SelectList(db.Acteurs, "id", "nom", operation.idActeur);
                        ViewBag.MaxMontant = MontantRestant;
                        ViewBag.IDTF = Convert.ToInt32(collections["IDTF"]);
                        operation.tf = db.tf.Find(Convert.ToInt32(collections["IDTF"]));
                        tf tf = operation.tf;
                        ViewBag.Proprietaire = tf.proprietaire.Select(i => i.prenom).FirstOrDefault(); ;
                        operation.idTypeOperation = null;
                        return View(operation);
                    }
                }
                operation.idTF = Convert.ToInt32(collections["IDTF"]);
                operation.datecreation = DateTime.Now;
                operation.datemodification = DateTime.Now;
                operation.tempOperation = 1;
                operation.activeOperation = 0;
                operation.valideOperation = 0;
                operation.IDStatus = 1;
                db.Operations.Add(operation);
                await db.SaveChangesAsync();
                var nature = db.NatureOperation.Find(operation.idNatureOperation);
                var type = db.TypesOperation.Find(operation.idTypeOperation);
                historique.Tracers("Creation", nature.nom, type.nom, "", CurrentUsers(User.Identity.Name), operation,db.tf.Find(operation.idTF), null);
                //var typeoperations = db.TypesOperations.Where(a => a.idNatureOperation == operation.idNatureOperation);
                //ViewBag.idTypeOperation = new SelectList(typeoperations.ToList(), "id", "nom", operation.idTypeOperation);
                // TempData["tf"] = Convert.ToInt32(collections["IDTF"]);
                //var idtf= Convert.ToInt32(collections["IDTF"]);
                var id = operation.id;
                return RedirectToAction("EditCreate", new { id = id });
            }


            return View(operation);
      

        }


        // GET: Operations/Edit/5
        [FiltresAuthorisation(Roles = "Modifier Hypotheque,Modifier Mutation,Special")]
        public ActionResult Edit(int id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** EDIT ID OPERATION :" + id);

                    if (id == 0)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    Operations operation =  db.Operations.Find(id);
                    if (operation == null)
                    {
                        return HttpNotFound();
                    }
            ViewBag.idNatureOperation = new SelectList(db.NatureOperation, "id", "nom", operation.idNatureOperation);
            ViewBag.idTF = new SelectList(db.tf, "id", "numerotf", operation.idTF);
            var typeoperations = db.TypesOperation.Where(a => a.idNatureOperation == operation.idNatureOperation);
            ViewBag.idTypeOperation = new SelectList(typeoperations.ToList(), "id", "nom", operation.idTypeOperation);
            var Actes = db.Actes.Where(i => i.idOperation == operation.id).ToList();
            TempData["ActesCount"] = Actes.Count();

            if (User.IsInRole("Modification operation après validation"))
            {
                ViewBag.Role = 1;
            }
            else
            {
                ViewBag.Role = 0;
            }

            if (operation.idNatureOperation == 2)
            {
                var acteurs = db.Acteurs.Where(a => a.TypeActeur.type == "CRE").ToList();
                ViewBag.idActeur = new SelectList(acteurs, "id", "nom", operation.idActeur);
                if(operation.idVendor != null)
                {
                    ViewBag.Vendor = "Debiteur";
                    var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                    var ideb = operation.Vendors.Acteurs.Where(i => i.TypeActeur.type == "DEB").Select(j => j.id).FirstOrDefault();
                    if(ideb != 0)
                    {
                        ViewBag.EstDEB = ideb;
                    }
                    else ViewBag.EstDEB = null;

                    ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom",ideb);

                }
                

            }
            if (operation.idNatureOperation == 1)
            {

                if (operation.idTypeOperation == 3)
                {
                    Vendors Vo = db.Vendors.Find(operation.idVendor);
                    operation.Vendors = Vo;
                    var Typeentreprise = db.TypeEntreprise.ToList();
                    ViewBag.Typeentreprise = new SelectList(Typeentreprise, "ID", "Libelle");
                }
            }
            if (operation.idNatureOperation == 3)
            {
                /* ----------------- Recuperer la liste des tf fusionné ----------------------------- */
                var listTFfusionner = operation.TFFusions_Principals.tf.ToList();

                ViewBag.FusionSuperficie = operation.TFFusions_Principals.Superficie;
                ViewBag.FusionValeur = operation.TFFusions_Principals.ValeurTerrain;
                ViewBag.NumeroTF = operation.tf.numerotf;
            }


            return View(operation);
        }
        [FiltresAuthorisation(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        public ActionResult EditCreate(int? id)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** EDITCREATE ID OPERATION :" + id);
            if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            
                Operations operation =  db.Operations.Find(id);
               // operation.idTF = operation.valideOperation;
               // operation.activeOperation = 1;
                if (operation == null)
                {
                    return HttpNotFound();
                }

            var typeoperations = db.TypesOperation.Where(a => a.idNatureOperation == operation.idNatureOperation);
            ViewBag.idTypeOperation = new SelectList(typeoperations.ToList(), "id", "nom", operation.idTypeOperation);


            if (operation.idNatureOperation == 1)
            {
                if (operation.idTypeOperation == 3)
                {
                    ViewBag.Denomination = operation.Vendors.NomDenom;
                    ViewBag.Typeentreprise = new SelectList(db.TypeEntreprise, "ID", "Libelle", operation.Vendors.ID_Type_entreprise);
                }

            }
           if(operation.idNatureOperation == 2)
            {
                if (operation.idVendor != null)
                {
                    var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                    var Act = db.Vendors.Where(i => i.id == operation.idVendor).Select(i => i.Acteurs.Where(o => o.idTypeActeur == 5).Select(p => p.id).FirstOrDefault()).FirstOrDefault();
                    ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom", Act);
                }

                var acteurs = db.Acteurs.ToList();
                if (operation.Acteurs.IDTypeProprietaire == 2)
                {
                    acteurs = acteurs.Where(a => a.TypeActeur.type == "CRE" && a.IDTypeProprietaire == 2).ToList();
                }
                else
                {
                    acteurs = acteurs.Where(a => a.TypeActeur.type == "CRE" && a.IDTypeProprietaire == 1).ToList();
                }
                ViewBag.idActeur = new SelectList(acteurs, "id", "nom", operation.idActeur);

            }
           if(operation.idNatureOperation == 3)
            {
                /* ----------------- Recuperer la liste des tf fusionné ----------------------------- */
                var listTFfusionner = operation.TFFusions_Principals.tf.ToList();

                ViewBag.FusionSuperficie = operation.TFFusions_Principals.Superficie;
                ViewBag.FusionValeur = operation.TFFusions_Principals.ValeurTerrain;
                ViewBag.NumeroTF = operation.tf.numerotf;
            }
           // var Actes = db.Actes.Where(i => i.idOperation == operation.id).ToList();
         
            return View(operation);
            

        }
        // POST: Operations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Creer Hypotheque,Creer Mutation,Special")]
        [HttpPost]
        public ActionResult EditCreate( Operations operat,FormCollection collection)
        {

          

                var ActeExist = db.Actes.Any(i => i.idOperation == operat.id);
                
                var ProprietaireExist = db.proprietaire.Where(i => i.Operations.Select(j => j.id).FirstOrDefault() == operat.id).FirstOrDefault();

      /*----------------------- Verification de l'existance d'au moins un acte --------------------- */

                if (ActeExist == false)
                {
                    TempData["message"] = "Veuillez ajouter au moins un acte !";
                    TempData["status"] = "Erreur";
                    var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == operat.idNatureOperation);
                    ViewBag.idTypeOperation = new SelectList(typeoperation.ToList(), "id", "nom", operat.idTypeOperation);

                    if (operat.idNatureOperation == 2)
                    {
                        if (operat.idVendor != null || operat.idVendor != 0)
                        {

                        var IDDebiteur = db.Acteurs.Where(i => i.TypeActeur.type == "DEB").ToList();
                        var Act = db.Vendors.Where(i => i.id == operat.idVendor).Select(i => i.Acteurs.Where(o => o.idTypeActeur == 5).Select(p => p.id).FirstOrDefault()).FirstOrDefault();
                        //var Act = db.Vendors.Where(i => i.id == operat.idVendor).Select(i => i.Acteurs.Where(o => o.idTypeActeur == 8 && o.idTypeActeur != 1 && o.idTypeActeur != 2 && o.idTypeActeur != 3 && o.idTypeActeur != 4 && o.idTypeActeur != 5 && o.idTypeActeur != 6 && o.idTypeActeur != 7).Select(p => p.id).FirstOrDefault()).FirstOrDefault();
                        ViewBag.IDDebiteur = new SelectList(IDDebiteur, "id", "nom", Act);
                        }
                        var acteur = db.Acteurs.Where(a => a.TypeActeur.type == "CRE").ToList();
                        ViewBag.idActeur = new SelectList(acteur, "id", "nom");
                        
                    }
                    if(operat.idNatureOperation == 1)
                    {
                        if(operat.idTypeOperation == 3)
                        {
                            ViewBag.Denomination = operat.Vendors.NomDenom;
                            ViewBag.Typeentreprise = new SelectList(db.TypeEntreprise, "ID", "Libelle", operat.Vendors.ID_Type_entreprise);
                        }

                    }
                    if(operat.idNatureOperation == 3)
                {
                    var listTFfusionner = operat.TFFusions_Principals.tf.ToList();
                    operat.TFFusions_Principals.tf = listTFfusionner;
                }
                    return View(operat);
                }
           
      /* ---------------------- Verification de l'existance d'un proprietaire -----------------------*/
                if (operat.idNatureOperation == 1 && operat.idTypeOperation != 3 && operat.idTypeOperation != 16)
                {

                    if (ProprietaireExist == null)
                    {
                        TempData["message"] = "Veuillez ajouter au moins un beneficiaire !";
                        TempData["status"] = "Erreur";
                        var typeoperation = db.TypesOperation.Where(a => a.idNatureOperation == operat.idNatureOperation);
                        ViewBag.idTypeOperation = new SelectList(typeoperation.ToList(), "id", "nom", operat.idTypeOperation);
                        return View(operat);
                    }
                }
                if(operat.idNatureOperation == 3)
            {
                var TFfusions = db.TFFusions_Principals.Find(operat.IDFusion);
                TFfusions.Superficie = collection["FusionSuperficie"];
                TFfusions.ValeurTerrain = collection["FusionValeur"];
                db.Entry(TFfusions).State = EntityState.Modified;
            }
            //    if(operat.idTypeOperation==16)
            //{
            //    operat.NatureOperation = Convert.ToDecimal(collection["Super"])
            //}

                    operat.tempOperation = 0;
                    operat.activeOperation = 1;
                    operat.valideOperation = 0;
                    operat.IDStatus = 2;
                    db.Entry(operat).State = EntityState.Modified;
                    db.Entry(operat).Property(X => X.idTF).IsModified = false;
            db.Entry(operat).Property(X => X.IDFusion).IsModified = false;
            db.SaveChanges();
            var id = operat.id;
          

            return RedirectToAction("Details", new { id = id });
        }

        [FiltresAuthorisation(Roles = "Modifier Hypotheque,Modifier Mutation,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,datecreation,datemodification,idNatureOperation,idTypeOperation,idTF,dateOperation,montantOperation,tempOperation,activeOperation,valideOperation,rang,idActeur,datefin,Status,idVendor,IDFusion")] Operations operation,FormCollection Collections)
        {

        
                var nature = db.NatureOperation.Find(operation.idNatureOperation);
                var type = db.TypesOperation.Find(operation.idTypeOperation);
            //var oper = db.Operations.Find(operation.id);
            //var status = oper.Status;

            if (operation.idVendor != null && operation.idNatureOperation == 2)
            {

                var IDDebiteur = Convert.ToInt32(Collections["IDDebiteur"]);
                if (IDDebiteur != 0)
                {
                    var Deb = db.Acteurs.Find(IDDebiteur);
                    var Debavant = db.Vendors.Find(operation.idVendor);
                    operation.Vendors = Debavant;
                    var v = operation.Vendors.Acteurs.Where(i => i.TypeActeur.type == "DEB").Select(j => j.id).FirstOrDefault();
                    var Debv = operation.Vendors.Acteurs.Where(i => i.TypeActeur.type == "DEB").FirstOrDefault();
                    if (v != IDDebiteur)
                    {
                        operation.Vendors.Acteurs.Remove(Debv);
                        operation.Vendors.Acteurs.Add(Deb);
                    }
                }
            }
            if(operation.idNatureOperation == 3)
            {
                var ValeurNouveau = Collections["FusionValeur"];
                var SuperficieNouveau = Collections["FusionSuperficie"];
                var FusionTFS = db.TFFusions_Principals.Find(operation.IDFusion);

                FusionTFS.Superficie = SuperficieNouveau;
                FusionTFS.ValeurTerrain = ValeurNouveau;
                db.Entry(FusionTFS).State = EntityState.Modified;
            }

            System.Diagnostics.Debug.WriteLine(" ***************** EDIT 2 ID OPERATION :");
                if (ModelState.IsValid)
                {
                    //if (status == "Radier")
                    //{
                    //    operation.Status = status;
                    //}


                   
                    operation.datemodification = DateTime.Now;
                    db.Entry(operation).State = EntityState.Modified;
                    db.Entry(operation).Property(x => x.IDStatus).IsModified = false;
                    db.Entry(operation).Property(x => x.idTF).IsModified = false;
                    db.Entry(operation).Property(x => x.idNatureOperation).IsModified = false;
                    db.Entry(operation).Property(x => x.idTypeOperation).IsModified = false;
                    db.Entry(operation).Property(x => x.IDFusion).IsModified = false;

                await db.SaveChangesAsync();
                    historique.Tracers("Modification", nature.nom, type.nom, "", CurrentUsers(User.Identity.Name), operation,db.tf.Find(operation.idTF), null);
                var id = operation.id;
                return RedirectToAction("Details", new { id = id });
                }
                var typeoperations = db.TypesOperation.Where(a => a.idNatureOperation == operation.idNatureOperation);
                ViewBag.idTypeOperation = new SelectList(typeoperations.ToList(), "id", "nom", operation.idTypeOperation);

                var acteurs = db.Acteurs.Where(a => a.TypeActeur.type == "CRE").ToList();
                ViewBag.idActeur = new SelectList(acteurs, "id", "nom");
                return View(operation);
            
        }

        // GET: Operations/Delete/5
        [FiltresAuthorisation(Roles = "Supprimer Operation,Special")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operations operation = await db.Operations.FindAsync(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            return View(operation);
        }

        // POST: Operations/Delete/5
        [FiltresAuthorisation(Roles = "Supprimer Operation,Special")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Operations operation = await db.Operations.FindAsync(id);
            db.Operations.Remove(operation);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [FiltresAuthorisation(Roles = "Valider Operation,Special")]
        public ActionResult valider(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operations operation = db.Operations.Find(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Valider", operation);

        }
        [HttpPost]
        [FiltresAuthorisation(Roles = "Valider Operation,Special")]
        public ActionResult ValidationConfirmer(int id)
        {
        


                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                System.Diagnostics.Debug.WriteLine(" ***************** VALIDER ID OPERATION :" + id);
                Operations operation = db.Operations.Find(id);
                var nature = db.NatureOperation.Find(operation.idNatureOperation);
                var type = db.TypesOperation.Find(operation.idTypeOperation);
                if (operation == null)
                {
                    return HttpNotFound();
                }
                operation.datemodification = DateTime.Now;
                operation.valideOperation = 1;
                operation.IDStatus = 3;

                var Lacte = operation.Actes.ToList();
                List<Actes> ListAct = new List<Actes>();
                foreach (var item in Lacte)
                {
                    item.Actif = true;
                    ListAct.Add(item);
                }



                if (operation.NatureOperation.id == 1 && (operation.idTypeOperation != 3 && operation.idTypeOperation != 15 && operation.idTypeOperation != 16))
                {
                    Vendors v = saveVendors(operation.tf);
                    operation.Vendors = v;
                    mettreajourTF(operation);
                }
                if (operation.idNatureOperation == 1 && operation.idTypeOperation == 3)
                {
                    tf TF = db.tf.Find(operation.idTF);
                    Vendors Vo = db.Vendors.Find(operation.idVendor);
                    var Proprios = TF.proprietaire.Select(i => i.nomestablishment).FirstOrDefault();

                    var idProprios = TF.proprietaire.Select(i => i.id).FirstOrDefault();
                    var Denom = operation.Vendors.NomDenom;
                    TypeEntreprise typeEnt = db.TypeEntreprise.Find(Vo.TypeEntreprise.ID);

                    var typeEntreprise = typeEnt.Libelle;


                    proprietaire Pro = db.proprietaire.Find(idProprios);
                    Pro.nomestablishment = Denom;
                    Pro.typestablishment = typeEntreprise;
                    //db.Entry(Pro).State = EntityState.Modified;
                    //db.SaveChanges();


                    Vo.NomDenom = Proprios;
                    db.Entry(Vo).Property(X => X.NomDenom).IsModified = true;
                    //db.Entry(Vo).State = EntityState.Modified;
                    //db.SaveChanges();


                    //var Proprios = db.tfs.Where(i=>i.id == Convert.ToInt32(operation.idTF))
                    //  operation.Vendor.NomDenom;   
                }

                if (operation.idNatureOperation == 3)
                {
                    var ListTF = operation.TFFusions_Principals.tf.ToList();
                    List<tf> ListTFS = new List<tf>();
                    for (int i = 0; i < ListTF.Count; i++)
                    {
                        var tf = db.tf.Find(ListTF[i].id);
                        tf.Actif = false;
                        db.Entry(tf).State = EntityState.Modified;
                        db.Entry(tf).Property(o => o.Actif).IsModified = true;
                        ListTFS.Add(tf);
                    }

                    var TfPrincipal = operation.tf;
                    var NewValeur = operation.TFFusions_Principals.ValeurTerrain;
                    var NewSuperficie = operation.TFFusions_Principals.Superficie;
                    var OldValeur = operation.tf.valeurterrain;
                    var OldSuperficie = operation.tf.superficie;

                    TfPrincipal.valeurterrain = NewValeur;
                    TfPrincipal.superficie = NewSuperficie;
                    operation.TFFusions_Principals.ValeurTerrain = OldValeur;
                    operation.TFFusions_Principals.Superficie = OldSuperficie;
                    // db.Entry(ListTFS).State = EntityState.Modified;
                    // db.Entry(ListTFS).Property(o=>o)
                    db.Entry(TfPrincipal).State = EntityState.Modified;
                }
                if (operation.idNatureOperation == 4)
                {
                    double compteur = 0;
                    foreach (var it in operation.Tfs_Distraction)
                    {
                        Tfs_Distraction TFD = it;
                        tf TFADI = db.tf.Find(TFD.ID_TF);
                        DistractionTF(TFADI, it);
                        compteur += Convert.ToDouble(it.Superficie);
                    }

                    tf tf = db.tf.Find(operation.idTF);
                    var superfici = Convert.ToDouble(tf.superficie);
                    var totalsup = superfici - compteur;
                    tf.superficie = Convert.ToString(totalsup);
                    db.Entry(tf).State = EntityState.Modified;
                    db.Entry(tf).Property(o => o.superficie).IsModified = true;
                }
                if (operation.idNatureOperation == 5)
                {
                    TFDefinitives(operation.idTF);
                }
                if (operation.idTypeOperation == 16)
                {
                    tf tf = db.tf.Find(operation.idTF);
                    decimal superficie;
                    if (tf.superficie.Contains(","))
                    {
                        var SuperficieString = tf.superficie.Replace(',', '.');
                        var t = Decimal.Parse(SuperficieString, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowCurrencySymbol);
                        superficie = t;
                    }
                    else { superficie = Convert.ToDecimal(tf.superficie); }
                    var SuperficieARajouter = operation.montantOperation;
                    var NouvelleSuperficie = superficie + SuperficieARajouter;
                    var users = CurrentUsers(User.Identity.Name);
                    tf.superficie = Convert.ToString(NouvelleSuperficie);
                    tf.dateModified = DateTime.Now;
                    tf.usermodified = users.id;
                    db.Entry(tf).State = EntityState.Modified;
                    db.Entry(tf).Property(o => o.superficie).IsModified = true;
                    db.Entry(tf).Property(o => o.dateModified).IsModified = true;
                    db.Entry(tf).Property(o => o.usermodified).IsModified = true;
                }
                if (operation.idNatureOperation == 6)
                {
                    if (operation.idTypeOperation == 18)
                    {
                        var Valeur = Convert.ToDecimal(operation.montantOperation);
                        var IDTF = Convert.ToInt32(operation.idTF);
                        var AncienneValeur = ReevaluationValeur(Valeur, IDTF);
                        operation.montantOperation = AncienneValeur;
                    }
                    if (operation.idTypeOperation == 19)
                    {
                        var Superficie = Convert.ToDecimal(operation.montantOperation);
                        var IDTF = Convert.ToInt32(operation.idTF);
                        var AncienneSuperficie = RectificationSuperficie(Superficie, IDTF);
                        operation.montantOperation = AncienneSuperficie;
                    }
                }

                db.SaveChanges();


                historique.Tracers("Validation", nature.nom, type.nom, "", CurrentUsers(User.Identity.Name), operation, operation.tf, null);
                var idd = operation.id;
                return RedirectToAction("Details", new { id = idd });

            
        }



        [FiltresAuthorisation(Roles = "Annuler Operation,Special")]
        public ActionResult Annuler(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Operations operation =  db.Operations.Find(id);
            if (operation == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Annuler", operation);

        }
        [FiltresAuthorisation(Roles = "Annuler Operation,Special")]
        [HttpPost]
        public ActionResult AnnulationConfirmer(int id)
        {
           
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                /*-------------------------- Modification du status   -------------------------*/

                Operations op = db.Operations.Find(id);
                op.valideOperation = 0;
                op.activeOperation = 0;
                op.IDStatus = 4;
                db.Entry(op).State = EntityState.Modified;
                db.SaveChanges();

                /*------------------------ L'historique de l'operations ------------------------*/

                var nature = db.NatureOperation.Find(op.idNatureOperation);
                var type = db.TypesOperation.Find(op.idTypeOperation);
                historique.Tracers("Annulation", nature.nom, type.nom, "", CurrentUsers(User.Identity.Name), op, null, null);

                return RedirectToAction("Details", op);
            
        }
        public Vendors saveVendors(tf tf)
        {
            Vendors v = new Vendors();
            v.daterequisition = tf.datetf;
            if (tf.numacp != null)
                v.numacp = tf.numacp;
            if (tf.DateOperation != null)
                v.datesave = tf.DateOperation.Value.Date;
            v.tf = tf;

            v.proprietaire = tf.proprietaire;

            db.Vendors.Add(v);
            db.SaveChanges();
            return v;
        }

        public void DistractionTF(tf TFAD, Tfs_Distraction it)
        {
            try
            {


                Tfs_Distraction TFD = it;
                //tf TFAD = db.tfs.Find(TFD.ID_TF);
                List<Tfs_Distraction_Proprios> ListTFDP = it.Tfs_Distraction_Proprios.ToList();
                tf TF = new tf();
                //TF = TFAD;
                TF.proprietaire = null;
                List<proprietaire> ListPro = new List<proprietaire>();


                for (int i = 0; i < ListTFDP.Count; i++)
                {
                    if (ListTFDP[i].NomComplet == "Etat de Djibouti")
                    {
                        var etat = db.proprietaire.Find(15245);
                        ListPro.Add(etat);
                    }
                    else
                    {


                        proprietaire P = new proprietaire();
                        if (ListTFDP[i].IDTypeProprios == 1)
                        {
                            P.prenom = ListTFDP[i].NomComplet;
                            if (ListTFDP[i].DateNaiss != " " || ListTFDP[i].DateNaiss != null)
                            {
                                P.dateNaissace = Convert.ToDateTime(ListTFDP[i].DateNaiss);
                            }
                            P.typeproprietaire = 1;
                            ListPro.Add(P);
                        }
                        else
                        {
                            P.nomestablishment = ListTFDP[i].NomEntreprise;
                            P.nomeresponsable1 = ListTFDP[i].NomComplet;
                            if (ListTFDP[i].DateNaiss != " " || ListTFDP[i].DateNaiss != null)
                            {
                                P.dateNaissace = Convert.ToDateTime(ListTFDP[i].DateNaiss);
                            }
                            P.typeproprietaire = 2;
                            P.typestablishment = ListTFDP[i].TypeEntreprise.Libelle;
                            ListPro.Add(P);
                        }
                    }

                }
                //  TF.id = 0;
                TF.datetf = it.Operations.dateOperation;
                TF.EstPublic = true;
                TF.Actif = true;
                TF.numerotf = Convert.ToInt32(TFD.NumeroTF);
                TF.proprietaire = ListPro;
                TF.valeurterrain = TFD.ValeurTerrain;
                TF.superficie = TFD.Superficie;
                TF.idquartier = TFAD.idquartier;
                TF.nomillot = TFAD.nomillot;
                TF.estbati = TFAD.estbati;
                TF.estillot = TFAD.estillot;
                TF.DateOperation = DateTime.Today;
                TF.ID_Unites = 1;
                TF.nomrepertoire = TFAD.nomrepertoire;
                db.tf.Add(TF);
                TFD.Actif = true;
                db.Entry(TFD).State = EntityState.Modified;
                db.SaveChanges();

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                            ve.PropertyName,
                            eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                            ve.ErrorMessage);
                    }
                }
            }
        }
        public void fusionsTF(TFFusions_Principals TFPrincipale)
        {

        }

        public void TFDefinitives(int? id)
        {
            tf tf = db.tf.Find(id);
            tf.typetf = 1;
            db.Entry(tf).State = EntityState.Modified;
            db.Entry(tf).Property(X => X.typetf).IsModified = true;
            db.SaveChanges();
        }
        public void mettreajourTF(Operations operation)
        {
            tf TitreFoncier = operation.tf;
            TitreFoncier.proprietaire = operation.proprietaire;
            db.SaveChanges();
            //TitreFoncier.DateOperation = ope
        }

        public decimal ReevaluationValeur(decimal NouvelleValeur,int IDTF)
        {
            var users = CurrentUsers(User.Identity.Name);
            tf tf = db.tf.Find(IDTF);
            var AncienneValeur = tf.valeurterrain;
            tf.valeurterrain = Convert.ToString(NouvelleValeur);
            tf.dateModified = DateTime.Now;
            tf.usermodified = users.id;
            db.Entry(tf).State = EntityState.Modified;
            db.Entry(tf).Property(o => o.valeurterrain).IsModified = true;
            db.Entry(tf).Property(o => o.dateModified).IsModified = true;
            db.Entry(tf).Property(o => o.usermodified).IsModified = true;
            db.SaveChanges();
            decimal Valeur;
            if (AncienneValeur.Contains(","))
            {
                var ValeurString = AncienneValeur.Replace(',', '.');
                var t = Decimal.Parse(ValeurString, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowCurrencySymbol);
                Valeur = t;
            }
            else { Valeur = Convert.ToDecimal(AncienneValeur); }

            return Valeur;
        }

        public decimal RectificationSuperficie(decimal NouvelleSuperficie, int IDTF)
        {
            var users = CurrentUsers(User.Identity.Name);
            tf tf = db.tf.Find(IDTF);
            var AncienneSuperficie = tf.superficie;
            tf.superficie = Convert.ToString(NouvelleSuperficie);
            tf.dateModified = DateTime.Now;
            tf.usermodified = users.id;
            db.Entry(tf).State = EntityState.Modified;
            db.Entry(tf).Property(o => o.valeurterrain).IsModified = true;
            db.Entry(tf).Property(o => o.dateModified).IsModified = true;
            db.Entry(tf).Property(o => o.usermodified).IsModified = true;
            db.SaveChanges();
            decimal Superficie;
            if (AncienneSuperficie.Contains(","))
            {
                var SuperficieString = AncienneSuperficie.Replace(',', '.');
                var t = Decimal.Parse(SuperficieString, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowCurrencySymbol);
                Superficie = t;
            }
            else { Superficie = Convert.ToDecimal(AncienneSuperficie); }

            return Superficie;
        }
        public int VerifieRangTF(int id) // Verifie le Rang du Titre foncier et renvoie le rang exacte du TF
        {
            var Resultat = "teste";
            int nombre = 0;
            var Check = db.Operations.Where(i => i.idTF == id  && i.idNatureOperation == 2 && (i.IDStatus == 2 || i.IDStatus == 3)).ToList();
            if (Check.Count == 0)
            {
                nombre = 1;
            }
            else
            {
                nombre = (Check.Count) + 1;
            }

            return nombre;
        }

        //public bool Verifie1erRangExist(int id)
        //{
        //    var Exist=db.Operations.Any(i=>i.)

        //    return true;
        //}
        public decimal VerifieMontantRestant(int id) // Verifie si le Montant restant pour cette titre foncier 
        {
            decimal MontantTotal = 0;
            decimal Montanthypo = 0;
            tf titrefoncier = db.tf.Find(id);
            if (titrefoncier.valeurterrain != null || Convert.ToInt32(titrefoncier.valeurterrain) != 0)
            {
                List<Operations> hypos = new List<Operations>();
                hypos = db.Operations.Where(i => i.idTF == id && i.idNatureOperation == 2 &&  i.IDStatus == 3).ToList();
                if (hypos.Count > 0)
                {
                    for (int i = 0; i < hypos.Count; i++)
                    {
                        if ((hypos[i].montantOperation != null) /*|| (hypos[i].montantOperation.Value != 0)*/)
                        {
                            Montanthypo += hypos[i].montantOperation.Value;
                        }
                        else Montanthypo += 0;
                    }
                    MontantTotal = Convert.ToDecimal(titrefoncier.valeurterrain) - Montanthypo;
                }
                else MontantTotal = Convert.ToDecimal(titrefoncier.valeurterrain);
            }
            else MontantTotal = 0;
            return MontantTotal;
        }
        
        public ActionResult ListCreancier(int? id)
        {
            int typeop= Convert.ToInt32(id);
            int typeacteur = 0;
            List<Acteurs> Creancier = new List<Acteurs>();
            if (typeop == 8)
            {
                typeacteur = 2;
                Creancier = db.Acteurs.Where(x => x.IDTypeProprietaire == typeacteur && x.idTypeActeur==4).ToList();
            }
            else if( typeop == 9)
            {
                typeacteur = 1;
                Creancier = db.Acteurs.Where(x => x.IDTypeProprietaire == typeacteur && x.idTypeActeur == 4).ToList();
            }
            else Creancier = db.Acteurs.Where(i=>i.idTypeActeur==4).ToList();

            List<Acteurs> lst = new List<Acteurs>();
            Acteurs cr = null;
           
            for (int j = 0; j < Creancier.Count(); j++)
            {
                cr = new Acteurs();
                cr.id = Creancier[j].id;
                cr.nom = Creancier[j].nom;
                lst.Add(cr);
            }
            SelectList selectsubDirectories = new SelectList(lst, "id", "nom", 0);
            return Json(selectsubDirectories);
        }
        
        public ActionResult ListActeurs(int? id)
        {
            int typeacteur = Convert.ToInt32(id);

            var ListActeur = db.Acteurs.Where(i => i.idTypeActeur == typeacteur && i.IDTypeProprietaire == 1).ToList();
            List<Acteurs> lst = new List<Acteurs>();
            Acteurs cr = null;

            for (int j = 0; j < ListActeur.Count(); j++)
            {
                cr = new Acteurs();
                cr.id = ListActeur[j].id;
                cr.nom = ListActeur[j].nom;
                lst.Add(cr);
            }
            SelectList selectsubDirectories = new SelectList(lst, "id", "nom", 0);
            return Json(selectsubDirectories);


        }

        public bool DateNotInFutur(DateTime Date)
        {
            if(Date > DateTime.Now)
            {
                return true;
            }
            return false;
        }

        public ActionResult VerifieDate(string DateString)
        {
            var Date = Convert.ToDateTime(DateString);
            var DateBool = DateNotInFutur(Date);

            if(DateBool == true)
            {
                return Json(new { success = true });
            }
            else return Json(new { success = false });

            return Json(new { success = false});
        }
        public ActionResult ListTypeOperation(int? id)
        {
            if (id != null)
            {
                int operation = Convert.ToInt32(id);
                List<TypesOperation> Type = new List<TypesOperation>();
                Type = db.TypesOperation.Where(i => i.idNatureOperation == operation).ToList();

                List<TypesOperation> Type1 = new List<TypesOperation>();
                TypesOperation cr = null;

                for (int j = 0; j < Type.Count(); j++)
                {
                    cr = new TypesOperation();
                    cr.id = Type[j].id;
                    cr.nom = Type[j].nom;
                    Type1.Add(cr);
                }
                SelectList selectsubDirectories = new SelectList(Type1, "id", "nom", 0);
                return Json(selectsubDirectories);
            }
            SelectList selectListAll = new SelectList(db.TypesOperation, "id", "nom");
            return Json(selectListAll);
        }



        public ActionResult CreateCreancierPartiel(int? id)
        {
            if(id == null) { id = 0; }

            return PartialView(id);
        }

        public ActionResult CreateCreancier(int? id,string NomComplet,string NomMere,string Email, string CIN,string Fixe, string Telephone, int ID_Type_Creancier, string Adresse)
        {

            Acteurs Creancier = new Acteurs();
            Creancier.nom = NomComplet;
            //bool Existcreancier = false;
            //if (ID_Type_Creancier == 2)
            //{
            //     Existcreancier = db.Acteurs.Any(i => i.nom == NomComplet && i.idTypeActeur == 4 && i.IDTypeProprietaire == 2);
            //    if (Existcreancier == true)
            //    {
            //        TempData["message"] = "Ce creancier existe déjà dans la base !";
            //        TempData["status"] = "Erreur";
            //        return Json(new { success = false });
            //    }
            //}
            //else
            //{
            //   Existcreancier = db.Acteurs.Any(i => i.nom == NomComplet && i.idTypeActeur == 4 && i.IDTypeProprietaire == 1);
            //    if (Existcreancier == true)
            //    {
            //        TempData["message"] = "Ce creancier existe déjà dans la base !";
            //        TempData["status"] = "Erreur";
            //            return Json(new { success = false });
            //        }

            //}
            Creancier.idTypeActeur = 4;
            Creancier.telephonePortable = Telephone;
            Creancier.telephoneFixe = Fixe;
            Creancier.IDTypeProprietaire = ID_Type_Creancier;
            Creancier.adresse = Adresse;
            Creancier.email = Email;
            Creancier.numerocin = CIN;
            Creancier.nommere = NomMere;
            
            db.Acteurs.Add(Creancier);
            db.SaveChanges();
            var list = db.Acteurs.ToList();
            
            if (id== 8)
            {
                list = list.Where(i => i.idTypeActeur == 4 && i.IDTypeProprietaire == 2).ToList();
            }
            else if (id== 9)
            {
                list = list.Where(i => i.idTypeActeur == 4 && i.IDTypeProprietaire == 1).ToList();
            }
            else
            {
                list = list.Where(i => i.idTypeActeur == 4).ToList();
            }
            SelectList Creanciers = new SelectList(list.Where(ij=>ij.id==Creancier.id).OrderBy(i => i.nom).ToList(), "id", "nom", 0);
            return Json(new { success = true , infos= Creanciers });
            }
        public ActionResult GetListCreancier()
        {
            var list = db.Acteurs.Where(i=>i.idTypeActeur == 4).ToList();
            SelectList Creanciers = new SelectList(list, "id", "nom", 0);
            return Json(Creanciers);
        }

        public ActionResult CreateDebiteurPartiel()
        {
            return PartialView("CreateDebiteurPartiel");
        }
        public ActionResult CreateDebiteur(string NomComplet, string NomMere, string Email, string CIN, string Fixe, string Telephone, int ID_Type_Creancier, string Adresse)
        {

            Acteurs Debiteur = new Acteurs();
            if(NomComplet != "")
            {
               Debiteur.nom = NomComplet.ToUpper();
            }
          
            //bool Existcreancier = false;
            //if (ID_Type_Creancier == 2)
            //{
            //     Existcreancier = db.Acteurs.Any(i => i.nom == NomComplet && i.idTypeActeur == 4 && i.IDTypeProprietaire == 2);
            //    if (Existcreancier == true)
            //    {
            //        TempData["message"] = "Ce creancier existe déjà dans la base !";
            //        TempData["status"] = "Erreur";
            //        return Json(new { success = false });
            //    }
            //}
            //else
            //{
            //   Existcreancier = db.Acteurs.Any(i => i.nom == NomComplet && i.idTypeActeur == 4 && i.IDTypeProprietaire == 1);
            //    if (Existcreancier == true)
            //    {
            //        TempData["message"] = "Ce creancier existe déjà dans la base !";
            //        TempData["status"] = "Erreur";
            //            return Json(new { success = false });
            //        }

            //}
            Debiteur.idTypeActeur = 5;
            Debiteur.telephonePortable = Telephone;
            Debiteur.telephoneFixe = Fixe;
            Debiteur.IDTypeProprietaire = ID_Type_Creancier;
            Debiteur.adresse = Adresse;
            Debiteur.email = Email;
            Debiteur.numerocin = CIN;
            Debiteur.nommere = NomMere;

            db.Acteurs.Add(Debiteur);
            db.SaveChanges();
            var list = db.Acteurs.Where(i => i.idTypeActeur == 5).OrderBy(i=>i.nom).ToList();
            SelectList Debiteurs = new SelectList(list.Where(i=>i.id == Debiteur.id), "id", "nom", 0);

            return Json(new { success = true , debit=Debiteurs});
        }
        public ActionResult GetListDebiteur()
        {
            var list = db.Acteurs.Where(i => i.idTypeActeur == 5).ToList();
            SelectList Creanciers = new SelectList(list, "id", "nom", 0);
            return Json(Creanciers);
        }

        public utilisateur CurrentUsers(string LoginUser)
        {
            var UserCurrent = db.utilisateur.Where(i => i.login == LoginUser).FirstOrDefault();

            return UserCurrent;
        }
    }
}
