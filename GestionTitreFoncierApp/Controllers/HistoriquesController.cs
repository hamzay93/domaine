﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]

    public class HistoriquesController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        public void Tracers(string ActionName, string Operations, string TypeOperations, string Description, utilisateur user, Operations op, tf tf, Actes ACT)
        {
            Historiques H = new Historiques();
            H.ActionName = ActionName;
            H.Operations = Operations;
            H.TypesOperations = TypeOperations;
            H.Descriptions = Description;
            H.DateActions = DateTime.Now;
            H.UserActions = user.id;
            //H.IDTF = tf.id; 
            if (op != null)
            {
                HistoriquesOperations HO = new HistoriquesOperations();
                if (ACT == null)
                {
                    HO.IDActes = null;
                }
                else HO.IDActes = ACT.id;
                HO.Historiques = H;
                HO.IDOperations = op.id;
                
                db.Historiques.Add(H);
                db.HistoriquesOperations.Add(HO);
            }
            else
            {
                db.Historiques.Add(H);
            }
            db.SaveChanges();
        }
        public ActionResult ListeHistoriques(int id)
        {
            Operations Op = db.Operations.Find(id);
            var historiquesOperations = Op.HistoriquesOperations.Select(i=>i.Historiques).ToList();
            return PartialView("_Historiques", historiquesOperations);
        }
        public ActionResult HistoriquesTF(int id)
        {
           tf tf = db.tf.Find(id);
            var historiquestf = db.Historiques.Where(i => i.IDTF == tf.id && i.ActionName != "Consultation").ToList().OrderByDescending(i=>i.DateActions);
                //tf.Historiques.ToList();
            return PartialView("_HistoriquesTF", historiquestf);
        }

        //public void TracersTF(string ActionName, string Operations, string TypeOperations, string Description, utilisateur user, tf tf)
        //{
        //    Historique H = new Historique();
        //    H.ActionName = ActionName;
        //    H.Operations = Operations;
        //    H.TypesOperations = TypeOperations;
        //    H.Descriptions = Description;
        //    H.DateActions = DateTime.Now;
        //    H.UserActions = user.id;
        //    H.IDTF = tf.id;
        //    db.Historiques.Add(H);
        //    db.SaveChanges();
        //}
        public ActionResult Index(string DayParametre)
        {
            var DateFinHistorique = DateTime.Today.AddHours(23).AddMinutes(59).AddSeconds(59);
            //var ListHistorique = db.Historiques.Where(i=>i.
            var Today = DateTime.Today;
            var Hier = Today;
            if (DayParametre == "-2" || DayParametre == "-1")
            {
                int Nombre = Convert.ToInt32(DayParametre);
                Hier = Today.AddDays(Nombre);
            }
            var HierFin = Hier.AddHours(23).AddMinutes(59).AddSeconds(59);
            var users = db.utilisateur.Where(i=>i.etat == true).ToList();
            List<historiquesConsultations> LHC = new List<historiquesConsultations>();
            historiquesConsultations HC = null;
            for(int j = 0; j<users.Count;j++)
            {
                var iduser = users[j].id;
                var userCons = db.Historiques.Where(i => i.ActionName == "Consultation" && i.UserActions == iduser && i.DateActions > Hier && i.DateActions<HierFin).ToList();
                var usercrea = db.Historiques.Where(i => i.ActionName == "Creation" && i.UserActions == iduser && i.DateActions > Hier && i.DateActions < HierFin).ToList();
                var usermodif = db.Historiques.Where(i => i.ActionName == "Modification" && i.UserActions == iduser && i.DateActions > Hier && i.DateActions < HierFin).ToList();
                HC = new historiquesConsultations();
                HC.ID = users[j].id;
                HC.Nom = users[j].nom;
                HC.CountConsul = userCons.Count;
                HC.CountCreation = usercrea.Count;
                HC.CountModification = usermodif.Count;
                LHC.Add(HC);
            }
            return View(LHC);
        }
        

    }
}