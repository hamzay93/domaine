﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;
using System.Web.Security;
using GestionTitreFoncierApp.Security;
using PagedList;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class utilisateursController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();

        // GET: utilisateurs
        public ActionResult _UtilisateurMiseaJour(int? page, string sortOrder)
        {
           
                    try
                    {
                ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle");
                System.Globalization.CultureInfo culInfo = new System.Globalization.CultureInfo("en-US");
                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "utilisateurs_desc" : "utilisateurs";

                //  var utilisateurs = db.utilisateurs.Include(u => u.typeutilisatur1);
                var utilisateurs = db.utilisateur.Include(o=>o.typeutilisatur1);

                switch (sortOrder)
                {
                    case "utilisateurs_desc":
                        utilisateurs = utilisateurs.OrderBy(s => s.id);
                        break;
                    default:
                        utilisateurs = utilisateurs.OrderByDescending(s => s.id);
                        break;
                }


                //int pageSize = 10;
                //int pageNumber = (page ?? 1);
                //return View(utilisateurs.ToPagedList(pageNumber, pageSize));
                return View(utilisateurs.ToList());
            }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
        
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            //So that the user can be referred back to where they were when they click logon
            if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null )
                returnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
            if (Url.IsLocalUrl(returnUrl) && !string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.returnUrl = returnUrl;
            }
            return View();
        }
        [HttpPost]
        [AllowAnonymous]

        public ActionResult Login(utilisateur users, string returnUrl)
        {
             var DEhashPass = FormsAuthentication.HashPasswordForStoringInConfigFile(users.motdepasse.Trim(), "md5");
             //var v1 = db.utilisateur.Where(a => a.login.Equals(users.login) && a.motdepasse.Equals(users.motdepasse)).FirstOrDefault();
             var v1 = db.utilisateur.Where(a => a.login.Equals(users.login) && a.motdepasse.Equals(DEhashPass)).FirstOrDefault();

            if (v1 != null)
            {
                if (v1.typeutilisatur != 10)
                {
                    FormsAuthentication.SetAuthCookie(users.login, false);

                    //if(Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    //    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    //{
                    //    return Redirect(returnUrl);
                    //}
                    //else
                    //{

                    if (Session["User"] == null)
                    {
                        Session["User"] = v1;
                        Session["LogedIduser"] = v1.id.ToString();
                        Session["LogedUserPrenom"] = v1.login.ToString();

                        string decodedUrl = "";
                        if (!string.IsNullOrEmpty(returnUrl))
                            decodedUrl = Server.UrlDecode(returnUrl);

                        //Login logic...

                        if (Url.IsLocalUrl(decodedUrl) && decodedUrl != "/utilisateurs/logout")
                        {
                            return Redirect(decodedUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "tfs");
                        }
                    }
                    else
                    {
                        string decodedUrl = "";
                        if (!string.IsNullOrEmpty(returnUrl))
                            decodedUrl = Server.UrlDecode(returnUrl);

                        //Login logic...

                        if (Url.IsLocalUrl(decodedUrl) && decodedUrl != "/utilisateurs/logout")
                        {
                            return Redirect(decodedUrl);
                        }
                        else
                        {
                            return RedirectToAction("Index", "tfs");
                        }
                    }
                }
                else
                {
                    ViewBag.Msg = "Vous n'etes autorisé !";
                    ViewBag.Color = "#e70000";
                    return View();
                }
                //}
            }
            else
            {
                ViewBag.Msg = "Login/Password incorrect";
                ViewBag.Color = "#ff4b4b";
                return View();

            }
            
            
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            // System.Web.HttpContext.Current.Session["userlognID"] = "";
            return RedirectToAction("Login");
        }
        public bool IsValid(string _userLogin, string _userPWD)
        {
            bool isvaliduser = false;
            utilisateur ut = new utilisateur();
            if (db.utilisateur.Where(x => x.login == _userLogin && x.motdepasse == _userPWD && x.etat == true).Count() != 0)
            {
                isvaliduser = true;
                System.Web.HttpContext.Current.Session["userlognID"] = db.utilisateur.Where(x => x.login == _userLogin && x.motdepasse == _userPWD).First().id;
            }




            return isvaliduser;
            //using (var cn = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename" +
            //  @"='C:\Tutorials\1 - Creating a custom user login form\Creating " +
            //  @"a custom user login form\App_Data\Database1.mdf';Integrated Security=True"))
            //{
            //    string _sql = @"SELECT [Username] FROM [dbo].[System_Users] " +
            //           @"WHERE [Username] = @u AND [Password] = @p";
            //    var cmd = new SqlCommand(_sql, cn);
            //    cmd.Parameters
            //        .Add(new SqlParameter("@u", SqlDbType.NVarChar))
            //        .Value = _username;
            //    cmd.Parameters
            //        .Add(new SqlParameter("@p", SqlDbType.NVarChar))
            //        .Value = Helpers.SHA1.Encode(_password);
            //    cn.Open();
            //    var reader = cmd.ExecuteReader();
            //    if (reader.HasRows)
            //    {
            //        reader.Dispose();
            //        cmd.Dispose();
            //        return true;
            //    }
            //    else
            //    {
            //        reader.Dispose();
            //        cmd.Dispose();
            //        return false;
            //    }
            //}
        }

        public bool ValidUserLogin(string userlogin)
        {
            bool isExist = true;
            if (!db.utilisateur.Any(x => x.login == userlogin))
            { isExist = false; }
            return isExist;
        }
       

        // GET: utilisateurs/Details/5
        public ActionResult Details(int? id)
        {

          
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                utilisateur utilisateur = db.utilisateur.Find(id);
                if (utilisateur == null)
                {
                    return HttpNotFound();
                }
                return View(utilisateur);
           

        }
        [FiltresAuthorisation(Roles = "Creer Utilisateur,Special")]
        // GET: utilisateurs/Create
        public ActionResult Create()
        {
           
            try
            {
                if (User.IsInRole("Special"))
                {
                    ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle");
                }
                else
                {
                    var typ = db.typeutilisatur.Where(i => i.id != 1 && i.id != 4).ToList();
                    ViewBag.typeutilisatur = new SelectList(typ, "id", "libelle");
                }
                return View();
          
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: utilisateurs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,nom,login,motdepasse,etat,datecreate,typeutilisatur")] utilisateur utilisateur)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.utilisateurs.Add(utilisateur);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.typeutilisatur = new SelectList(db.typeutilisaturs, "id", "libelle", utilisateur.typeutilisatur);
        //    return View(utilisateur);
        //}

        [FiltresAuthorisation(Roles = "Creer Utilisateur,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(utilisateur utilisateur)
        {
            try
            {
                var userid = db.utilisateur.Where(i => i.login == User.Identity.Name).Select(i => i.id).FirstOrDefault();
                    utilisateur userToACreate = new utilisateur();
                typeutilisatur userttype = new typeutilisatur();
                userttype = db.typeutilisatur.Where(x => x.id == utilisateur.typeutilisatur).First();
                userToACreate.nom = utilisateur.nom;
                var LoginExist = db.utilisateur.Any(i => i.login == utilisateur.login);
                if(LoginExist == true)
                {
                    TempData["message"] = "Ce Login Existe déjà !";
                    TempData["status"] = "Erreur";
                    if (User.IsInRole("Special"))
                    {
                        ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle",utilisateur.typeutilisatur);
                    }
                    else
                    {
                        var typ = db.typeutilisatur.Where(i => i.id != 1 && i.id != 4).ToList();
                        ViewBag.typeutilisatur = new SelectList(typ, "id", "libelle",utilisateur.typeutilisatur);
                    }
                    return View();
                }

                userToACreate.login = utilisateur.login;
                userToACreate.motdepasse = utilisateur.motdepasse;
                userToACreate.typeutilisatur1 = userttype;
                userToACreate.etat = true;
                userToACreate.datecreate = DateTime.Today;
                userToACreate.usercreate = userid;
                    //(int)System.Web.HttpContext.Current.Session["userlognID"];
                userToACreate.motdepasse = FormsAuthentication.HashPasswordForStoringInConfigFile(utilisateur.motdepasse.Trim(), "md5");

                if (ModelState.IsValid)
                {
                    db.utilisateur.Add(userToACreate);
                    db.SaveChanges();
                    return RedirectToAction("_UtilisateurMiseaJour");
                }

                ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle", utilisateur.typeutilisatur);
                return View(utilisateur);
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }

        }
        public ActionResult verfyuserLogin(string loginvalue)
        {
            bool b = db.utilisateur.Any(x => x.login == loginvalue);

            return Json(b);
        }

        // GET: utilisateurs/Edit/5
        [FiltresAuthorisation(Roles = "Modifier Utilisateur,Special")]
        public ActionResult Edit(int? id)
        {
            try
            {
               


                    if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                utilisateur utilisateur = db.utilisateur.Find(id);
                if (utilisateur == null)
                {
                    return HttpNotFound();
                }
                ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle", utilisateur.typeutilisatur);
                return View(utilisateur);
           
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // POST: utilisateurs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "id,nom,login,motdepasse,etat,datecreate,typeutilisatur")] utilisateur utilisateur)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(utilisateur).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.typeutilisatur = new SelectList(db.typeutilisaturs, "id", "libelle", utilisateur.typeutilisatur);
        //    return View(utilisateur);
        //}
        [FiltresAuthorisation(Roles = "Modifier Utilisateur,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(utilisateur utilisateur)
        {
            try
            {
               
                 utilisateur utilisateurToEdit = new utilisateur();
                utilisateurToEdit = db.utilisateur.Where(x => x.id == utilisateur.id).First();
                utilisateurToEdit.id = utilisateur.id;
                utilisateurToEdit.nom = utilisateur.nom;
                utilisateurToEdit.motdepasse = utilisateur.motdepasse;
                utilisateurToEdit.typeutilisatur = utilisateur.typeutilisatur;
                utilisateurToEdit.login = utilisateur.login;
                utilisateurToEdit.etat = utilisateur.etat;
                //if (!ValidUserLogin(utilisateur.login))
                //{

                if (ModelState.IsValid)
                {
                    db.Entry(utilisateurToEdit).State = EntityState.Modified;
                    db.Entry(utilisateurToEdit).Property(X => X.motdepasse).IsModified = false;
                    db.SaveChanges();
                    return RedirectToAction("_UtilisateurMiseaJour");
                }
                ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle", utilisateur.typeutilisatur);

                //}
                //else
                //{ ModelState.AddModelError("", "Login data is incorrect!"); }

                return View(utilisateurToEdit);
         
}
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: utilisateurs/Delete/5
        [FiltresAuthorisation(Roles = "Directeurs,Administrateur,Responsables")]
        public ActionResult Delete(int? id)
        {

           
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                utilisateur utilisateur = db.utilisateur.Find(id);
                if (utilisateur == null)
                {
                    return HttpNotFound();
                }
                return View(utilisateur);
           
        }

        // POST: utilisateurs/Delete/5
        [FiltresAuthorisation(Roles = "Directeurs,Administrateur,Responsables")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           
                utilisateur utilisateur = db.utilisateur.Find(id);
                db.utilisateur.Remove(utilisateur);
                db.SaveChanges();
                return RedirectToAction("Index");
           
        }

        [HttpGet]
        public ActionResult ChangementMotPasse()
        {
            utilisateur Users = db.utilisateur.Where(i => i.login == User.Identity.Name).FirstOrDefault();
            return View(Users);
        }

        [HttpPost]
        public ActionResult ChangementMotPasse(utilisateur Users,FormCollection collections)
        {
            
            var UsersOld = db.utilisateur.Find(Users.id);
            var VerifieOldPassword = UsersOld.motdepasse;
            var Oldpassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Users.motdepasse.Trim(), "md5"); 
            var VerifiePassword = collections["NewPassword"];
            var VerifieConfPass = collections["ConfNewPassword"];
            if (VerifieOldPassword != Oldpassword)
            {
                TempData["message"] = "Verifier Votre ancien mot de passe !";
                TempData["status"] = "Erreur";
                return View(Users);
            }
            else if(VerifieConfPass != VerifiePassword)
            {
                TempData["message"] = "Verifier Votre confirmation de mot de passe !";
                TempData["status"] = "Erreur";
                return View(Users);
            }
            else
            {
                Users = UsersOld;
                Users.motdepasse = FormsAuthentication.HashPasswordForStoringInConfigFile(VerifiePassword.Trim(), "md5");
                //VerifiePassword;
                db.utilisateur.Attach(Users);
                db.Entry(Users).Property(x => x.motdepasse).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Logout");
            }
            return View(Users);
        }

        public ActionResult VerifiePass(int? id, string pass)
        {
            if (id != null && pass != "")
            {
                var users = db.utilisateur.Where(i => i.id == id).FirstOrDefault();
                string Password = FormsAuthentication.HashPasswordForStoringInConfigFile(pass.Trim(), "md5");
                if (Password == users.motdepasse)
                {
                    return Json(new { success = true });
                }
                else
                {
                    return Json(new { success = false });
                }
            }
            return Json(new { success = false });
        }
        [FiltresAuthorisation(Roles = "Creer Utilisateur,Special")]

        public ActionResult IndexRoles()
        {
            List<typeutilisatur> roles = new List<typeutilisatur>();
            if (User.IsInRole("Special"))
            {
                roles = db.typeutilisatur.ToList();
            }
            else
            {
                roles = db.typeutilisatur.Where(i => i.id != 1 && i.id != 4).ToList();
            }
                return View(roles);
        }

        [FiltresAuthorisation(Roles = "Creer Utilisateur,Special")]

        public ActionResult CreateDroitsToRole(int id)
        {
            if (id == 0)
            {

            }

            var typeutilisateur = db.typeutilisatur.Find(id);
            var DroitAces = db.DroitAcces.ToList();

            if (User.IsInRole("Special"))
            {
                ViewBag.typeutilisatur = new SelectList(db.typeutilisatur, "id", "libelle", typeutilisateur.id);
            }
            else
            {
                var typ = db.typeutilisatur.Where(i => i.id != 1 && i.id != 4).ToList();
                ViewBag.typeutilisatur = new SelectList(typ, "id", "libelle", typeutilisateur.id);
                DroitAces = DroitAces.Where(i => i.ID != 21).ToList();
            }
            ViewBag.typeutilisatur = typeutilisateur.id;
            RolesDroits RD = new RolesDroits();
           
            List<DroitsAcces> LD = new List<DroitsAcces>();
            List<DroitAcces> List = typeutilisateur.DroitAcces.ToList();
            int j = 0;
            for (int i = 0; i < (DroitAces.Count)*2; i++)
            {
                DroitsAcces D = new DroitsAcces();
                if (i < List.Count )
                {
                        var leDroit = db.DroitAcces.Find(List[i].ID);
                        D.ID = leDroit.ID;
                        D.Libelle = leDroit.Libelle;
                        D.Checked = true;
                }
                else
                {
                    if (i >= DroitAces.Count)
                    {
                        var IDD = DroitAces[j].ID;
                        var Exist = LD.Any(o => o.ID == IDD);
                        if (Exist != true)
                        {
                            D.ID = DroitAces[j].ID;
                            D.Libelle = DroitAces[j].Libelle;
                            D.Checked = false;
                        }
                        j++;
                    }
                    else
                    {
                        var IDDD = DroitAces[i].ID;
                        var Exist = LD.Any(o => o.ID == IDDD);
                        if (Exist != true)
                        {
                            D.ID = DroitAces[i].ID;
                            D.Libelle = DroitAces[i].Libelle;
                            D.Checked = false;
                        }
                    }
                }
                if (D.ID != 0)
                {
                    LD.Add(D);
                }
            }

            RD.Droits = LD.OrderBy(h=>h.ID).ToList();
            //RD.typeutilisateur = typeutilisateur;
            return View(RD);
        }
        [FiltresAuthorisation(Roles = "Creer Utilisateur,Special")]

        [HttpPost]
        public ActionResult CreateDroitsToRole(RolesDroits RD, FormCollection collection)
        {
            List<DroitAcces> Droits = new List<DroitAcces>();
           // string[] select = collection["Selected"].Replace("true,false", "true").Split(',');
            var IDType = Convert.ToInt32(collection["typeutilisatur"]);
            var type = db.typeutilisatur.Find(IDType);
            for (int j = 0; j < RD.Droits.Count; j++)
            {
                if (RD.Droits[j].Checked == true)
                {
                    var ID = RD.Droits[j].ID;
                    var Exist = type.DroitAcces.Any(d => d.ID == ID);
                    if (Exist == false)
                    {
                        var Acces = db.DroitAcces.Find(RD.Droits[j].ID);
                        Droits.Add(Acces);
                        type.DroitAcces.Add(Acces);
                    }
                }
                else
                {
                    var ID = RD.Droits[j].ID;
                    var Exist = type.DroitAcces.Any(d => d.ID == ID);
                    if (Exist == true)
                    {
                        var Acces = db.DroitAcces.Find(RD.Droits[j].ID);
                        Droits.Remove(Acces);
                        type.DroitAcces.Remove(Acces);
                    }

                }
            }
            db.Entry(type).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("IndexRoles", "utilisateurs");
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
