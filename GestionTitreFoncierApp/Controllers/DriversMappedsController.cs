                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;

namespace GestionTitreFoncierApp.Controllers
{
    public class DriversMappedsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();

        // GET: DriversMappeds
        public ActionResult Index()
        {
            try
            {

                    try
                    {
                        return View(db.DriversMapped.ToList());
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }
        public bool testServer(int ?serverID)
        {
            bool isAvaialble = false;
            try
            {
                DriversMapped dm = db.DriversMapped.Find(serverID);
                using (new NetworkConnection(dm.SharedPath, new NetworkCredential(dm.username, dm.Password)))
                {
                    isAvaialble = true;

                }
                return isAvaialble;
            }
            catch (Exception ex)
            {
                return isAvaialble;

            }
        }
        // GET: DriversMappeds/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriversMapped driversMapped = db.DriversMapped.Find(id);
            if (driversMapped == null)
            {
                return HttpNotFound();
            }
            return View(driversMapped);
        }

        // GET: DriversMappeds/Create
        public ActionResult Create()
        {
            try
            {

                    try
                    {
                        //string[] strDrives = Environment.GetLogicalDrives();
                        //ViewBag.Drive = strDrives;
                        ViewBag.Drive = new SelectList(db.Drives, "id", "name");

                        return View();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
              
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }

        }

        // POST: DriversMappeds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,Domian,username,Password,DateCreate,SharedPath")] DriversMapped driversMapped)
        public ActionResult Create(DriversMapped driversMapped)
        {
          
                    try
                    {
                        DriversMapped DriversMappedToCreate = new DriversMapped();
                        if (ModelState.IsValid)
                        {
                            DriversMappedToCreate.Domian = driversMapped.Domian;
                            DriversMappedToCreate.username = driversMapped.username;
                            DriversMappedToCreate.Password = driversMapped.Password;
                          //  DriversMappedToCreate.IsValid = driversMapped.IsValid;
                            DriversMappedToCreate.IsActive = driversMapped.IsActive;
                            DriversMappedToCreate.Drive = driversMapped.Drive;
                            DriversMappedToCreate.SharedPath = driversMapped.SharedPath;

                             DriversMappedToCreate.DateCreate = DateTime.Now;
                            db.DriversMapped.Add(DriversMappedToCreate);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        return View(DriversMappedToCreate);

                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
                  
        }

        // GET: DriversMappeds/Edit/5
        public ActionResult Edit(int? id)
        {
           
               
                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriversMapped driversMapped = db.DriversMapped.Find(id);

            if (driversMapped == null)
            {
                return HttpNotFound();
            }
                        //string[] strDrives = Environment.GetLogicalDrives();
                        //ViewBag.Drive = strDrives;
                        ViewBag.Drive = new SelectList(db.Drives, "id", "name");
                        return View(driversMapped);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
             
        }

        // POST: DriversMappeds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DriversMapped driversMapped)
        {
          
                    try
                    {
                        DriversMapped DriversMappedToedit = new DriversMapped();

                        if (ModelState.IsValid)
                        {
                            DriversMappedToedit = db.DriversMapped.Find(driversMapped.id);
                            DriversMappedToedit.Domian = driversMapped.Domian;
                            DriversMappedToedit.username = driversMapped.username;
                            DriversMappedToedit.Password = driversMapped.Password;
                            DriversMappedToedit.IsValid = driversMapped.IsValid;
                            DriversMappedToedit.IsActive = driversMapped.IsActive;
                            DriversMappedToedit.Drive = driversMapped.Drive;
                            DriversMappedToedit.SharedPath = driversMapped.SharedPath;

                            DriversMappedToedit.DateCreate = DateTime.Now;
                            db.Entry(DriversMappedToedit).State = EntityState.Modified;
                               db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        return View(DriversMappedToedit);

                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
            
            //try
            //{

            //    if (!System.Web.HttpContext.Current.Session["userlognID"].Equals(""))

            //    {
            //        try
            //        {
            //            if (ModelState.IsValid)
            //            {
            //                if (ModelState.IsValid)
            //                {
            //                    db.Entry(driversMapped).State = EntityState.Modified;
            //                    db.SaveChanges();
            //                    return RedirectToAction("Index");
            //                }
            //                return View(driversMapped);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            ViewBag.message = ex.Message;
            //            return View("Error");
            //        }
            //    }
            //    else
            //    {
            //        return RedirectToAction("logout", "utilisateurs");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return RedirectToAction("logout", "utilisateurs");

            //}
        }

        // GET: DriversMappeds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DriversMapped driversMapped = db.DriversMapped.Find(id);
            if (driversMapped == null)
            {
                return HttpNotFound();
            }
            return View(driversMapped);
        }

        // POST: DriversMappeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DriversMapped driversMapped = db.DriversMapped.Find(id);
            db.DriversMapped.Remove(driversMapped);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
                                                                                                                                                                                                                                                                                                                                 