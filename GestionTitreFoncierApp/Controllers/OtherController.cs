﻿using GestionTitreFoncierApp.Models;
using GestionTitreFoncierApp.Security;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;


namespace GestionTitreFoncierApp.Controllers
{
    
    
    public class OtherController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        public string localphysicpath = @"c:\Domain\";


        [ChildActionOnly]
        [FiltresAuthorisation(Roles = "Consultation Images,Special")]

        public PartialViewResult DisplayImages(string imageindex, int typview, string numerotf)
        {
            string[] imagestf;
            var p = getdestpath(numerotf);
            string ErrorPath = Server.MapPath(Url.Content("~/Images/ERRORS/"));
            string chemincomplet = "C:/Domain/" + p.Replace(@"\", "/");
            var folder = new DirectoryInfo(chemincomplet);
            if (folder.Exists)
            {
                if (Directory.GetFiles(chemincomplet).Length == 0 && Directory.GetDirectories(chemincomplet).Length == 0)
                {
                    imagestf = Directory.GetFiles(ErrorPath);
                }
                else
                { imagestf = Directory.GetFiles(chemincomplet); }
            }
            else
            {
                imagestf = Directory.GetFiles(ErrorPath);
            }

            byte[] imageByteData2 = System.IO.File.ReadAllBytes(imagestf[0].Replace(@"\", "/"));
            string imageBase64Data2 = Convert.ToBase64String(imageByteData2);
            string imageDataURL2 = string.Format("data:image/jpg;base64,{0}", imageBase64Data2);

            ViewBag.ImageData = imageDataURL2;
            ViewBag.indeximg = 0;
            ViewBag.nbrimages = imagestf.Length - 1;
            TempData["chemins"] = imagestf;

            return PartialView("DisplayImages");
           // return PartialView("DisplayImages", new ViewDataDictionary { { "pathofimg1", "0" } });
        }

        public string getdestpath(string numerotf)
        {
            //14120
            string fullpath = "";
            int numeortflength = numerotf.Length;
            if (numeortflength < 3)
            {
                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "000 000 à 000 999";//014 000 à 014 999
                string fourththpath = "000 000 à 000 099";//014 100 à 014 199
                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;

            }
            if (numeortflength == 3)
            {
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "000 000 à 000 999";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199


                ////Fourth
                var aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, firstleterofnumerof);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, firstleterofnumerof);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;
            }
            else if (numeortflength == 4)
            {
                //000 000 à 099 999\000 000 à 009 999
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);
                string fourthletterofnumerotf = numerotf.Substring(3, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "000 000 à 009 999";//  010 000 à 019 999
                string thirdpathpath = "";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199
                                         ////third name
                var aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(2, 1);
                aStringBuilder.Insert(2, firstleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(12, 1);
                aStringBuilder.Insert(12, firstleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                ////Fourth
                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, Secondleterofnumerof);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, Secondleterofnumerof);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;

            }
            else if (numeortflength == 5)
            {
                string firstleterofnumerof = numerotf.Substring(0, 1);
                string Secondleterofnumerof = numerotf.Substring(1, 1);
                string thirdletterofnumerotf = numerotf.Substring(2, 1);
                string fourthletterofnumerotf = numerotf.Substring(3, 1);

                string firstpublicpath = @"000 000 à 099 999";
                string Secondpublicpath = "";//  010 000 à 019 999
                string thirdpathpath = "";//014 000 à 014 999
                string fourththpath = "";//014 100 à 014 199



                //Second name
                var aStringBuilder = new StringBuilder(firstpublicpath);
                aStringBuilder.Remove(1, 1);
                aStringBuilder.Insert(1, firstleterofnumerof);
                Secondpublicpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(11, 1);
                aStringBuilder.Insert(11, firstleterofnumerof);
                Secondpublicpath = aStringBuilder.ToString();

                ////third name
                aStringBuilder = new StringBuilder(Secondpublicpath);
                aStringBuilder.Remove(2, 1);
                aStringBuilder.Insert(2, Secondleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(12, 1);
                aStringBuilder.Insert(12, Secondleterofnumerof);
                thirdpathpath = aStringBuilder.ToString();

                ////Fourth
                aStringBuilder = new StringBuilder(thirdpathpath);
                aStringBuilder.Remove(4, 1);
                aStringBuilder.Insert(4, thirdletterofnumerotf);
                fourththpath = aStringBuilder.ToString();

                aStringBuilder = new StringBuilder(fourththpath);
                aStringBuilder.Remove(14, 1);
                aStringBuilder.Insert(14, thirdletterofnumerotf);
                fourththpath = aStringBuilder.ToString();


                fullpath = firstpublicpath + @"\" + Secondpublicpath + @"\" + thirdpathpath + @"\" + fourththpath + @"\DJIB-TF-" + numerotf;
            }

            return fullpath;
            //"010 000 à 019 999"
        }

    }
}
