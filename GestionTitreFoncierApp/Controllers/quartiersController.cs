﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestionTitreFoncierApp.Models;
using GT.Models;
using GestionTitreFoncierApp.Security;
using PagedList;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class quartiersController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        public ActionResult getQuarlst(string typ, int? regid)
        {
         

                try
                {
                    if (typ == "Quart")
                    {
                        List<img> lst = new List<img>();
                        img im = null;
                        var quartiers = db.quartier.Where(x => x.estlotissement == false && x.regionid == regid).ToList();
                        for (int j = 0; j < quartiers.Count(); j++)
                        {
                            im = new img();
                            im.pathofimg = quartiers[j].libelle;
                            im.indexnav = quartiers[j].id;
                            lst.Add(im);
                        }

                        SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
                        return Json(selectsubDirectories);
                    }
                    if (typ == "Lotis")
                    {
                        List<img> lst = new List<img>();
                        img im = null;
                        var quartiers = db.quartier.Where(x => x.estlotissement == true && x.regionid == regid).ToList();
                        for (int j = 0; j < quartiers.Count(); j++)
                        {
                            im = new img();
                            im.pathofimg = quartiers[j].libelle;
                            im.indexnav = quartiers[j].id;
                            lst.Add(im);
                        }
                        SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
                        return Json(selectsubDirectories);
                    }
                    else
                    {
                        List<img> lst = new List<img>();
                        img im = null;
                        var quartiers = db.quartier.Where(x => x.regionid == regid).ToList();
                        for (int j = 0; j < quartiers.Count(); j++)
                        {
                            im = new img();
                            im.pathofimg = quartiers[j].libelle;
                            im.indexnav = quartiers[j].id;
                            lst.Add(im);
                        }
                        SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
                        return Json(selectsubDirectories);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.message = ex.Message;
                    return View("Error");
                }

           
        }

        //public ActionResult getVillelst(int? regid)
        //{


        //        try
        //        {
        //            if (typ == "Quart")
        //            {
        //                List<img> lst = new List<img>();
        //                img im = null;
        //                var quartiers = db.quartiers.Where(x => x.estlotissement == false && x.regionid == regid).ToList();
        //                for (int j = 0; j < quartiers.Count(); j++)
        //                {
        //                    im = new img();
        //                    im.pathofimg = quartiers[j].libelle;
        //                    im.indexnav = quartiers[j].id;
        //                    lst.Add(im);
        //                }

        //                SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
        //                return Json(selectsubDirectories);
        //            }
        //            if (typ == "Lotis")
        //            {
        //                List<img> lst = new List<img>();
        //                img im = null;
        //                var quartiers = db.quartiers.Where(x => x.estlotissement == true && x.regionid == regid).ToList();
        //                for (int j = 0; j < quartiers.Count(); j++)
        //                {
        //                    im = new img();
        //                    im.pathofimg = quartiers[j].libelle;
        //                    im.indexnav = quartiers[j].id;
        //                    lst.Add(im);
        //                }
        //                SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
        //                return Json(selectsubDirectories);
        //            }
        //            else
        //            {
        //                List<img> lst = new List<img>();
        //                img im = null;
        //                var quartiers = db.quartiers.Where(x => x.regionid == regid).ToList();
        //                for (int j = 0; j < quartiers.Count(); j++)
        //                {
        //                    im = new img();
        //                    im.pathofimg = quartiers[j].libelle;
        //                    im.indexnav = quartiers[j].id;
        //                    lst.Add(im);
        //                }
        //                SelectList selectsubDirectories = new SelectList(lst, "indexnav", "pathofimg", 0);
        //                return Json(selectsubDirectories);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ViewBag.message = ex.Message;
        //            return View("Error");
        //        }

          
        //}


        //GET: quartiers

        public ActionResult getville(int? regid)
        {

            if (regid != 0 /*|| regid != null*/)
            {
                //List<quartier> listquart = new List<quartier>();
                //quartier quart = null;
                //var quartiers = db.quartiers.Where(i => i.Ville.region_ID == regid).ToList(); 
                List<Ville> lst = new List<Ville>();
                Ville vill = null;
                var Villes = db.Ville.Where(x => x.region_ID == regid).ToList();
                for (int j = 0; j < Villes.Count(); j++)
                {
                    vill = new Ville();
                    vill.id = Villes[j].id;
                    vill.libelle = Villes[j].libelle;
                    lst.Add(vill);
                }
                //for (int i = 0; i < quartiers.Count(); i++)
                //{
                //    quart = new quartier();
                //    quart.id = quartiers[i].id;
                //    quart.libelle = quartiers[i].libelle;
                //    listquart.Add(quart);
                //}
                //SelectList selectQuarts = new SelectList(listquart.OrderBy(i => i.libelle), "id", "libelle", 0);
                SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                return Json(selectsubDirectories);


            }
            SelectList selectsubDirectoriess = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle");
            return Json(selectsubDirectoriess);

        }
        public ActionResult getquartier(int? ville,int? Arrondi, string typ)
        {
            if(typ== "Quart")
            {
                if (Arrondi == 0 && ville != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.regionid == ville && x.estlotissement == false).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i=>i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);


                }
                else if (ville == 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi && x.estlotissement == false).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
                else if (ville != 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi && x.regionid == ville && x.estlotissement == false).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
            }
            else if(typ == "Lotis")
            {
                if (Arrondi == 0 && ville != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.regionid == ville && x.estlotissement == true).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);


                }
                else if (ville == 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi && x.estlotissement == true).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
                else if (ville != 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi && x.regionid == ville && x.estlotissement == true).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
            }
            else
            {
                if (Arrondi == 0 && ville != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.regionid == ville).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);


                }
                else if (ville == 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
                else if (ville != 0 && Arrondi != 0)
                {
                    List<quartier> lst = new List<quartier>();
                    quartier Quartier = null;
                    var Quartiers = db.quartier.Where(x => x.idArr == Arrondi && x.regionid == ville).ToList();
                    for (int j = 0; j < Quartiers.Count(); j++)
                    {
                        Quartier = new quartier();
                        Quartier.id = Quartiers[j].id;
                        Quartier.libelle = Quartiers[j].libelle;
                        lst.Add(Quartier);
                    }

                    SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                    return Json(selectsubDirectories);
                }
            }

            SelectList selectsubDirectoriess = new SelectList(db.quartier.OrderBy(i => i.libelle), "id", "libelle");
            return Json(selectsubDirectoriess);

        }
        public ActionResult getArrond(int? Commune)
        {

            if (Commune != null)
            {
                List<Arrondissement> lst = new List<Arrondissement>();
                Arrondissement arrondissement = null;
                var Arrondissements = db.Arrondissement.Where(x => x.idCommune == Commune).ToList();
                for (int j = 0; j < Arrondissements.Count(); j++)
                {
                    arrondissement = new Arrondissement();
                    arrondissement.id = Arrondissements[j].id;
                    arrondissement.libelle = Arrondissements[j].libelle;
                    lst.Add(arrondissement);
                }

                SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                return Json(selectsubDirectories);


            }
            SelectList selectsubDirectoriess = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle");
            return Json(selectsubDirectoriess);

        }
        public ActionResult getCommune(int? ville)
        {

            if (ville != 0)
            {
                List<Commune> lst = new List<Commune>();
                Commune commune = null;
                var Communes = db.Commune.Where(x => x.idVille == ville).ToList();
                for (int j = 0; j < Communes.Count(); j++)
                {
                    commune = new Commune();
                    commune.id = Communes[j].id;
                    commune.libelle = Communes[j].libelle;
                    lst.Add(commune);
                }

                SelectList selectsubDirectories = new SelectList(lst.OrderBy(i => i.libelle), "id", "libelle", 0);
                return Json(selectsubDirectories);


            }
            SelectList selectsubDirectoriess = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle");
            return Json(selectsubDirectoriess);

        }


        public ActionResult gettyp(int? regid, int? ville, int? Arrondi,int? Comm, string typ)
        {
            var quartiers = db.quartier.Include(f=>f.Ville);
            if(regid != 0 && regid != null)
            {
                quartiers = quartiers.Where(i => i.Ville.region_ID == regid);
            }
            if(ville != 0 && ville != null)
            {
                quartiers = quartiers.Where(i => i.regionid == ville);
            }
            if(Arrondi != 0 && Arrondi != null)
            {
                quartiers = quartiers.Where(i => i.idArr == Arrondi);
            }
            if(Comm !=0 && Comm != null)
            {
                quartiers = quartiers.Where(i => i.Arrondissement.idCommune == Comm);
            }
            if(typ == "Quart")
            {
                quartiers = quartiers.Where(i => i.estlotissement == false);
            }
            else if (typ == "Lotis")
            {
                quartiers = quartiers.Where(i => i.estlotissement == true);
            }
            SelectList ListQuartiers = new SelectList(quartiers.OrderBy(i => i.libelle).ToList(), "id", "libelle", 0);
            return Json(ListQuartiers);
        }
        public ActionResult Index(string RegId,string VilleId,string ArrondId,string CommuneId,string typLieu)
        {
           
                System.Globalization.CultureInfo culInfo = new System.Globalization.CultureInfo("en-US");
                
                ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle");

                var quartiers = db.quartier.Include(q => q.Ville);
                quartiers = quartiers.OrderByDescending(s => s.id);

            int typeliu;
            string liutp = typLieu;
            if (liutp == "Quart")
            {
                typeliu = 1;
            }
            else if (liutp == "Lotis")
            {
                typeliu = 2;
            }
            else
            {
                typeliu = 3;
            }

            int regID = RegId != "" ? Convert.ToInt32(RegId) : 0;
            int villeID = VilleId != "" ? Convert.ToInt32(VilleId) : 0;
            int communeID = CommuneId != "" ? Convert.ToInt32(CommuneId) : 0;
            int arrondID = ArrondId != "" ? Convert.ToInt32(ArrondId) : 0;

            if (regID != 0)
            {
                quartiers = quartiers.Where(i => i.Ville.region_ID == regID);
                var Villes = db.Ville.Where(i => i.region_ID == regID).ToList();
                ViewBag.VilleId = new SelectList(Villes, "id", "libelle", villeID);
            }
            if (villeID != 0)
            {
                quartiers = quartiers.Where(i => i.regionid == villeID);
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle).Where(i=>i.idVille == villeID), "id", "libelle");
            }
            if (communeID != 0)
            {
                quartiers = quartiers.Where(i => i.Arrondissement.idCommune == communeID);
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle).Where(i=>i.idCommune == communeID), "id", "libelle");
            }
            if (arrondID != 0)
            {
                quartiers = quartiers.Where(i => i.idArr == arrondID);
            }
            if (typeliu == 1)
            {
                quartiers = quartiers.Where(i => i.estlotissement == false );
            }
            if (typeliu == 2)
            {
                quartiers = quartiers.Where(i => i.estlotissement == true);
            }

            //var Ville = 0;
            //    if (VilleId != "" && VilleId != null)
            //    {

            //        Ville = Convert.ToInt32(VilleId);
            //        quartiers = quartiers.Where(i => i.regionid == Ville);

            //    }
            // var Villes = db.Villes.ToList();
            //if (RegId != "" && RegId != null)
            //{
            //    int Reg = Convert.ToInt32(RegId);

            //        //quartiers = quartiers.Where(i => i. == Reg);
            //    if (VilleId != "" && VilleId != null)
            //    {
            //        Villes = Villes.Where(i => i.region_ID == Reg).ToList();
            //        ViewBag.VilleId = new SelectList(Villes, "id", "libelle", Ville);

            //    }
            //    //else
            //    //{
            //    //    var villes = db.Villes.Where(i => i.region_ID == Reg).ToList();
            //    //    for (int w = 0; w < villes.Count(); w++)
            //    //    {
            //    //        quartiers = quartiers.Where(i => i.regionid == villes[w].id);
            //    //    }
            //    //}


            //}
            //if (ArrondId != "" && ArrondId != null)
            //    {
            //        int Arrond = Convert.ToInt32(ArrondId);
            //        quartiers = quartiers.Where(i => i.idArr == Arrond);
            //    }
            //    if (CommuneId != "" && CommuneId != null)
            //    {
            //        int Commune = Convert.ToInt32(CommuneId);
            //        quartiers = quartiers.Where(i => i.Arrondissement.idCommune == Commune);
            //    }



            //int pageSize = 10;
            //int pageNumber = (page ?? 1);
            return View(quartiers.ToList());
            }

        // GET: quartiers/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                
                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quartier quartier = db.quartier.Find(id);
            if (quartier == null)
            {
                return HttpNotFound();
            }
            return View(quartier);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        // GET: quartiers/Create
        [FiltresAuthorisation(Roles = "Creer Localisation,Special")]
        public ActionResult Create()
        {
            
                ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle");
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle");
                return View();
        }
        [FiltresAuthorisation(Roles = "Creer Localisation,Special")]
        // POST: quartiers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "libelle,estlotissement,regionid,user_create,id,RegId,VilleId,CommuneId,ArrondId,typLieu")] quartier quartier,FormCollection form)
        {
            var Region = Convert.ToInt32(form["RegId"]);
            var Ville = Convert.ToInt32(form["VilleId"]);
            var Arrond = Convert.ToInt32(form["ArrondId"]);
            var Commune = Convert.ToInt32(form["CommuneId"]);
            var typLieu = form["typLieu"];

            if(typLieu == "Quart")
            {
                quartier.estlotissement = false;
            }
            else if(typLieu == "Lotis")
            {
                quartier.estlotissement = true;
            }
            else
            {
                TempData["message"] = "Veuillez Indiqué le type de lieu !";
                TempData["status"] = "Erreur";
                ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle", Region);
                ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle", Ville);
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle", Arrond);
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle", Commune);
                return View(quartier);
            }


            if (Ville != 0)
            {
                quartier.regionid = Ville;
                if(Arrond != 0)
                { quartier.idArr = Arrond; }
                db.quartier.Add(quartier);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                TempData["message"] = "Veuillez selectionné une ville !";
                TempData["status"] = "Erreur";
                ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle", Region);
                ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle", Ville);
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle", Arrond);
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle", Commune);
                return View(quartier);

            }

                ViewBag.RegId = new SelectList(db.region.OrderBy(i => i.libelle), "id", "libelle",Region);
                ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle",Ville);
                ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle",Arrond);
                ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle",Commune);
                return View(quartier);
        }

        // GET: quartiers/Edit/5
        [FiltresAuthorisation(Roles = "Modifier Localisation,Special")]

        public ActionResult Edit(int? id)
        {
             


                    if (id == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                    }
                    quartier quartier = db.quartier.Find(id);
                    if (quartier == null)
                    {
                        return HttpNotFound();
                    }
            var Regi = 0;
            if (quartier.Ville != null)
            {
                Regi = Convert.ToInt32(quartier.Ville.region_ID);
            }


            ViewBag.RegId = new SelectList(db.region, "id", "libelle",Regi);
                    ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle).Where(i => i.region_ID== Regi), "id", "libelle", quartier.regionid);
                    ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle",quartier.idArr);
            var comm = 0;
                if(quartier.Arrondissement != null)
            {
                comm =Convert.ToInt32(quartier.Arrondissement.idCommune);
            }

                    ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle",comm);
                // ViewBag.regionid = new SelectList(db.regions, "id", "libelle", quartier.regionid);
                return View(quartier);
                
            }
          
        
        [FiltresAuthorisation(Roles = "Modifier Localisation,Special")]
        // POST: quartiers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "libelle,estlotissement,RegId,user_create,id,VilleId,ArrondId,CommuneId")] quartier quartier,FormCollection form)
        {
            var ville = form["VilleId"];
            var Arrond = form["ArrondId"];
               
                    
                if(Arrond != "")
                {
                    var ArrondID = Convert.ToInt32(Arrond);
                    quartier.idArr = ArrondID; 
                }
                if (ville != "")
                {
                    var VilleID = Convert.ToInt32(ville);
                    quartier.regionid = VilleID;

                    db.Entry(quartier).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                    
                  
            ViewBag.RegId = new SelectList(db.region, "id", "libelle", quartier.Ville.region_ID);
            ViewBag.VilleId = new SelectList(db.Ville.OrderBy(i => i.libelle), "id", "libelle", quartier.regionid);
            ViewBag.ArrondId = new SelectList(db.Arrondissement.OrderBy(i => i.libelle), "id", "libelle", quartier.idArr);
            ViewBag.CommuneId = new SelectList(db.Commune.OrderBy(i => i.libelle), "id", "libelle", quartier.Arrondissement.idCommune);
            return View(quartier);
               
         
        }

        // GET: quartiers/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {

               
                    try
                    {
                        if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            quartier quartier = db.quartier.Find(id);
            if (quartier == null)
            {
                return HttpNotFound();
            }
            return View(quartier);
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }
        [FiltresAuthorisation(Roles = "Supprimer Localisation,Special")]
        // POST: quartiers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {

                
                    try
                    {
                        quartier quartier = db.quartier.Find(id);
            db.quartier.Remove(quartier);
            db.SaveChanges();
            return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        ViewBag.message = ex.Message;
                        return View("Error");
                    }
               
            }
            catch (Exception ex)
            {
                return RedirectToAction("logout", "utilisateurs");

            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
