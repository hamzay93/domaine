﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Net;
using System.Threading.Tasks;
using System.Data.Entity;
using GestionTitreFoncierApp.Security;

namespace GestionTitreFoncierApp.Controllers
{
    [Authorize]
    public class ParametragesController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        // GET: Parametrages
        public ActionResult Index(/*int? page,*/ string IDTypeActeurs, string IDTypeProprietaire, string Nom/*, string currentFilter, string CurrentFilterTA, string CurrentFilterTP, string sortOrder*/)
        {
            var TypeActeurs = db.TypeActeur.Where(i=>i.type != "CRE" && i.type != "DEB").ToList();
            ViewBag.IDTypeActeurs = new SelectList(TypeActeurs, "id", "libelle");
            var TypeProprietaire = db.typeproprietaire.ToList();
            ViewBag.IDTypeProprietaire= new SelectList(TypeProprietaire, "id", "libelle");

            System.Globalization.CultureInfo culInfo = new System.Globalization.CultureInfo("en-US");
            //ViewBag.CurrentSort = sortOrder;
            //ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "typeActeur_desc" : "typeActeur";

            //if (IDTypeActeurs != null && IDTypeProprietaire != null && Nom != null)
            //{
            //    page = 1;
            //}
            //else
            //{
            //    IDTypeActeurs = CurrentFilterTA;
            //    IDTypeProprietaire = CurrentFilterTP;
            //    Nom = currentFilter;

            //}
            //ViewBag.CurrentFilterTA = IDTypeActeurs;
            //ViewBag.CurrentFilterTP = IDTypeProprietaire;
            //ViewBag.CurrentFilter = Nom;



            var Acteurs = db.Acteurs.Where(i=>i.idTypeActeur != 4 && i.idTypeActeur != 5);

            int IDTypeActeur = 0;
            if (!String.IsNullOrEmpty(IDTypeActeurs) && IDTypeActeurs != "" && IDTypeActeurs != "0")
            {

                 IDTypeActeur = Convert.ToInt32(IDTypeActeurs);
                Acteurs = Acteurs.Where(i => i.idTypeActeur == IDTypeActeur);
                ViewBag.IDTypeActeurs = new SelectList(TypeActeurs, "id", "libelle",IDTypeActeur);

            }
            int IDTypeProprietaires = 0;
            if (!String.IsNullOrEmpty(IDTypeProprietaire) && IDTypeProprietaire != "" && IDTypeProprietaire != "0")
            {
                 IDTypeProprietaires = Convert.ToInt32(IDTypeProprietaire);
                Acteurs = Acteurs.Where(i => i.IDTypeProprietaire == IDTypeProprietaires);
                ViewBag.IDTypeProprietaire = new SelectList(TypeProprietaire, "id", "libelle",IDTypeProprietaire);

            }
            if (!String.IsNullOrEmpty(Nom))
            {
                Acteurs = Acteurs.Where(i => i.nom.StartsWith(Nom));
            }

            //switch (sortOrder)
            //{
            //    case "operation_desc":
            //        Acteurs = Acteurs.OrderBy(s => s.idTypeActeur);
            //        break;
            //    default:
            //        Acteurs = Acteurs.OrderByDescending(s => s.id);
            //        break;
            //}

            Acteurs = Acteurs.OrderByDescending(s => s.id);
            //int pageSize = 15;
            //int pageNumber = (page ?? 1);
            return View(Acteurs.ToList());
        }

        [FiltresAuthorisation(Roles = "Creer Acteur,Special")]
        [HttpGet]
        public ActionResult Create()
        {
            var TypeActeurs = db.TypeActeur.Where(i => i.type != "CRE" && i.type != "DEB").ToList();
            var TypeProprietaire = db.typeproprietaire.ToList();
            ViewBag.idTypeActeur= new SelectList(TypeActeurs, "id", "libelle");
            ViewBag.IDTypeProprietaire= new SelectList(TypeProprietaire, "id", "libelle");
            return PartialView("Create");
        }
        [FiltresAuthorisation(Roles = "Creer Acteur,Special")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,nom,numerocin,nommere,telephonePortable,telephoneFixe,email,adresse,idTypeActeur,IDTypeProprietaire")] Acteurs acteur)
        {
            if (ModelState.IsValid)
            {
                db.Acteurs.Add(acteur);
                await db.SaveChangesAsync();
                ViewBag.idTypeActeur = new SelectList(db.TypeActeur, "id", "libelle");
                ViewBag.IDTypeProprietaire = new SelectList(db.typeproprietaire, "id", "libelle");
                //return RedirectToAction("Index");
                //string url = Url.Action("Index", "Parametrages");
                return Json(new { success = true });
            }
            ViewBag.idTypeActeur = new SelectList(db.TypeActeur, "id", "libelle", acteur.idTypeActeur);
            ViewBag.IDTypeProprietaire = new SelectList(db.typeproprietaire, "id", "libelle", acteur.IDTypeProprietaire);
            return PartialView(acteur);
        }
        public ActionResult Details(int id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acteurs Acteurs = db.Acteurs.Find(id);
            if(Acteurs == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return PartialView("Details",Acteurs);
        }

        [FiltresAuthorisation(Roles = "Modifier Acteur,Special")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Acteurs acteur = await db.Acteurs.FindAsync(id);
            if (acteur == null)
            {
                return HttpNotFound();
            }

            ViewBag.idTypeActeur = new SelectList(db.TypeActeur, "id", "libelle", acteur.idTypeActeur);
            ViewBag.IDTypeProprietaire = new SelectList(db.typeproprietaire, "id", "libelle", acteur.IDTypeProprietaire);
            return PartialView(acteur);
        }
        [FiltresAuthorisation(Roles = "Modifier Acteur,Special")]
        // POST: Acteurdds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,nom,numerocin,nommere,telephonePortable,telephoneFixe,email,adresse,idTypeActeur,IDTypeProprietaire")] Acteurs acteur)
        {
            if (ModelState.IsValid)
            {
                db.Entry(acteur).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Json(new { success = true });
            }
            ViewBag.idTypeActeur = new SelectList(db.TypeActeur, "id", "libelle", acteur.idTypeActeur);
            ViewBag.IDTypeProprietaire = new SelectList(db.typeproprietaire, "id", "libelle", acteur.IDTypeProprietaire);
            return PartialView(acteur);
        }


       
    }
}