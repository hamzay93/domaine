﻿using GestionTitreFoncierApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GestionTitreFoncierApp.Controllers
{
    public class FusionsController : Controller
    {
        private GTFDB_PROD_PRESEntities db = new GTFDB_PROD_PRESEntities();
        HistoriquesController historique = new HistoriquesController();
        OperationsController OperationController = new OperationsController();

        // GET: Fusions
        public ActionResult List(int id)
        {
            ViewBag.ID_Fusion = id;
            var ListFusions = db.TFFusions_Principals.Where(a => a.ID == id);
            ViewBag.ActesCount = ListFusions.Count();
            return PartialView("_List", ListFusions.ToList());
        }
        public ActionResult Index(int id)
        {
            ViewBag.ID_Fusion = id;
            var ListFusions = db.TFFusions_Principals.Where(a => a.ID == id);
            ViewBag.ActesCount = ListFusions.Count();
            return PartialView("_Index", ListFusions.ToList());
        }
        public ActionResult Create(int? id /*,int? idop*/)
        {
            System.Diagnostics.Debug.WriteLine(" ***************** ID TF :" + id);

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tf tf = db.tf.Find(id);
            Operations Op = new Operations();
            Op.tf = tf;
            Op.idTF = tf.id;
            Op.idTypeOperation = 12;
            Op.idNatureOperation = 3;
            var typeoperations = db.TypesOperation.Where(i => i.idNatureOperation == 3).ToList();
            ViewBag.idTypeOperation = 12;
            ViewBag.IDTF = tf.id;
            var su = Convert.ToDouble(tf.superficie);
            var val = Convert.ToDouble(tf.valeurterrain);
            ViewBag.superficie = (int)su;
            ViewBag.valeur = (int)val;
            ViewBag.supertxt = (int)su;
            ViewBag.valeurtxt = (int)val;
            return View(Op);
        }

        [HttpPost]
        public ActionResult Create(FormCollection form,  Operations operations)
        {
            var natureid = Convert.ToInt32(form["idNatureOperation"]);
            var typeid = Convert.ToInt32(form["idTypeOperation"]);
            var DateOperation = Convert.ToDateTime(form["dateOperation"]);
            var ListNbreTFS = form.AllKeys.Where(k => k.StartsWith("NumTFS")).ToList();
            if (ListNbreTFS.Count == 0)
            {
                TempData["message"] = "Veuillez indiqué les Tfs a Fusionnées !";
                TempData["status"] = "Erreur";
                tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                operations.tf = tf;
                operations.idTypeOperation = 12;
                operations.idNatureOperation = 3;
                ViewBag.IDTF = operations.idTF;
                ViewBag.superficie = Convert.ToInt32(form["superficie"]);
                ViewBag.valeur = Convert.ToInt32(form["valeur"]);
                ViewBag.supertxt = Convert.ToInt32(form["supertxt"]);
                ViewBag.valeurtxt = Convert.ToInt32(form["valeurtxt"]);
                return View(operations);
            }
            var Chek =OperationController.DateNotInFutur(Convert.ToDateTime(DateOperation));
            if(Chek == true)
            {
                TempData["message"] = "La date Operation doit etre anterieur a la date d'aujourd'hui !";
                TempData["status"] = "Erreur";
                tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                operations.tf = tf;
                operations.idTypeOperation = 12;
                operations.idNatureOperation = 3;
                ViewBag.IDTF = operations.idTF;
                ViewBag.superficie = Convert.ToInt32(form["superficie"]);
                ViewBag.valeur = Convert.ToInt32(form["valeur"]);
                ViewBag.supertxt = Convert.ToInt32(form["supertxt"]);
                ViewBag.valeurtxt = Convert.ToInt32(form["valeurtxt"]);
                for(int i=0;i<ListNbreTFS.Count();i++)
                {
                    string Num = ListNbreTFS[i];
                    ViewBag.Num = form[Num];
                }
                return View(operations);
            }

            /* ---------------------- Listé les TF a fusionniés ------------------------------- */
           
            var listTFfusions = new List<tf>();
            for (int i = 0; i < ListNbreTFS.Count(); i++)
            {
                string k = ListNbreTFS[i];
                //string id = "NumTF" + i;
                string NumTF = form[k];
                var NTF = Convert.ToInt32(NumTF);
                var TFfusions = db.tf.FirstOrDefault(j => j.numerotf == NTF && j.Actif == true);
                var TFAccp = TFValide(TFfusions.id, null);
                if (TFAccp == true)
                {
                    listTFfusions.Add(TFfusions);
                }
                else
                {
                    TempData["message"] = "Desolé ! le numero tf "+ NTF + " n'existe pas";
                    TempData["status"] = "Erreur";

                    tf tf = db.tf.Find(Convert.ToInt32(form["IDTF"]));
                    operations.tf = tf;
                    ViewBag.IDTF = Convert.ToInt32(form["IDTF"]);
                   // ViewBag.NbreTFS = NbreTFS;
                    ViewBag.superficie = Convert.ToInt32(form["superficie"]);
                    ViewBag.valeur = Convert.ToInt32(form["valeur"]);
                    ViewBag.supertxt = Convert.ToInt32(form["supertxt"]);
                    ViewBag.valeurtxt = Convert.ToInt32(form["valeurtxt"]);
                    return View(operations);
                }
            }

            /* --------------------- Remplire la table Fusions  ------------------------------- */

            //var listTableFusions = new List<TFs_Fusions>();
            var Fusions = new TFFusions_Principals();
            Fusions.IDTF_Principal = Convert.ToInt32(form["IDTF"]);
            Fusions.Superficie = Convert.ToString(form["supertxt"]);
            Fusions.ValeurTerrain = Convert.ToString(form["valeurtxt"]);
            for (int h = 0; h < listTFfusions.Count; h++)
            {
                Fusions.tf.Add(listTFfusions[h]);
            }
            db.TFFusions_Principals.Add(Fusions);

            /*--------------------- Creation de l'operation -------------------------------------------*/
            
            operations.TFFusions_Principals = Fusions;
            operations.idTF = Convert.ToInt32(form["IDTF"]);
            operations.datecreation = DateTime.Now;
            operations.datemodification = DateTime.Now;
            operations.tempOperation = 1;
            operations.activeOperation = 0;
            operations.valideOperation = 0;
            operations.IDStatus = 1;
            operations.dateOperation = DateOperation;
            db.Operations.Add(operations);
            db.SaveChanges();
            var nature = db.NatureOperation.Find(operations.idNatureOperation);
            var type = db.TypesOperation.Find(operations.idTypeOperation);
            var TF = db.tf.Find(operations.idTF);
            historique.Tracers("Creation", nature.nom, type.nom, "",OperationController.CurrentUsers(User.Identity.Name), operations, TF, null);

            return RedirectToAction("EditCreate","Operations", new { id = operations.id });
        }

        public ActionResult CreateFusions(int? id)
        {
            ViewBag.IDFusion = id;
            var TFFusion = db.TFFusions_Principals.Find(id);
            ViewBag.IDTF_Principal = TFFusion.IDTF_Principal;
            return PartialView("_Create",TFFusion);
        }
        [HttpPost]
        public ActionResult CreateFusions(TFFusions_Principals tfFusions,FormCollection Form)
        {
            string NumTFS = Form["NumTFS"];
            int Num = Convert.ToInt32(NumTFS);
            var TfConf = TFValide(null, NumTFS);
            if(TfConf == true)
            {
                var tf = db.tf.FirstOrDefault(i => i.numerotf == Num && i.Actif == true);
                var superficieold = Convert.ToInt32(tfFusions.Superficie);
                int superficieNew;
                if (tf.superficie.Contains(","))
                {
                    var SuperficieString = tf.superficie.Replace(',', '.');
                    var t = Decimal.Parse(SuperficieString, NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint | NumberStyles.AllowCurrencySymbol);
                    superficieNew = Convert.ToInt32(t);
                }
                else superficieNew = Convert.ToInt32(tf.superficie);

                var TotalSuperficie = superficieold + superficieNew;
                tfFusions.Superficie = Convert.ToString(TotalSuperficie);
                var ValeurOld = Convert.ToInt32(tfFusions.ValeurTerrain);
                var ValeurNew = Convert.ToInt32(tf.valeurterrain);
                var TotalValeur = ValeurOld + ValeurNew;
                tfFusions.ValeurTerrain = Convert.ToString(TotalValeur);
                db.Entry(tfFusions).State = EntityState.Modified;
                tfFusions.tf.Add(tf);
                db.SaveChanges();
            }
            else
            {
                return Json(new { success = false});
            }
            string url = Url.Action("List", "Fusions", new { id = tfFusions.ID });
            return Json(new { success = true, url = url });
        }


        public ActionResult VerifieExistTF(int? id ,int? pass)
        {
            if (id != null)
            {
                string Num = Convert.ToString(id);
                var TFAccp = TFValide(null, Num);
                if (TFAccp == true)
                {
                    var NTF = Convert.ToInt32(Num);
                    tf tf = db.tf.FirstOrDefault(i => i.numerotf == NTF);
                    var superficie = 0;
                    var valeur = 0;
                    if (tf.superficie != null)
                    {
                        var Sup = Convert.ToDecimal(tf.superficie);
                        superficie += decimal.ToInt32(Sup);
                    }
                    if (tf.valeurterrain != null)
                    {
                        valeur = Convert.ToInt32(tf.valeurterrain);
                    }
                    return Json(new { success = true, sup = superficie, val = valeur });
                }
                else
                {
                    return Json(new { success = false });
                }
            }
            else
            {
                return Json(new { success = false });
            }

        }

        public bool TFValide(int? id,string numeros)
        {
            var tf = db.tf.FirstOrDefault();
            if (id != null)
            {
                tf = db.tf.Find(id);
            }
            if(numeros != null)
            {
                var NTF = Convert.ToInt32(numeros);
                tf = db.tf.FirstOrDefault(i => i.numerotf == NTF);
            }
            if (tf != null)
            {
                /* --------------- Verificaion de toutes les conditions d'une fusion Valide d'un tf -----------------*/
                var ExistHypoPasRadier = db.Operations.Any(i => i.idTF == tf.id && i.idNatureOperation == 2 && (i.IDStatus == 3 || i.IDStatus == 5));
                if (tf.Actif != true || ExistHypoPasRadier == true)
                {
                    return false;
                }
                else return true;
            }
            else return false;
        }

        }
}